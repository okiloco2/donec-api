module.exports = function(sandbox) {
  const app = sandbox.app;
  const db = sandbox.db;

  return {
    init: function() {
      sandbox.get("/users/:_id", this.test); //Toma el nombre del router como path
      sandbox.post("/crear_archivos", (req, res) => {
        return res.json({
          msg: "Archivos cargados",
          success: true
        });
      });
    },
    test: function(req, res) {
      const { _id } = req.params;
      db.user.findById({ _id }, { password: 0 }, (err, data) => {
        if (err) {
          return res.json({
            success: false,
            msg: err.message
          });
          return;
        }
        return res.json({
          success: true,
          data
        });
      });
    }
  };
};
