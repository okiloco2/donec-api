const Donec = require("../../Donec");
const GroupSchema = {
    "name": {"type":"String","unique":true,"required": true},
    "modules": [
        {
            "module": {
                "type": "ObjectId",
                "ref": "module"
            }
        }
    ]
}
const GroupModel = new Donec.Model("group",GroupSchema,"en");
module.exports = GroupModel;