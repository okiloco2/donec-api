var jwt = require("jsonwebtoken");
class JwtValidator{

    validate(token){
        return new Promise((resolve,reject) => {

            jwt.verify(token, process.env.SECRET_KEY, function(err,decode){
               	
                let msg = "";
                if(err){
                       switch(err.name){	
                           case "TokenExpiredError":
                               msg ="La sesión ha expirado.";
                           break;
                           default:
                               msg = err.message;
                           break;
                       }	
                      reject(msg);
                }else{
                    //console.log("usergroup: ",decode.group);	
                    resolve();
                }
            });
        });
    }
}
module.exports = JwtValidator;