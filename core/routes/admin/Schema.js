var Helper = require("../../helpers/helper");
var md5 = require("md5");
var path = require("path");
var fs = require("fs");
var beautifyJS = require("js-beautify").js;
const replace = require("replace-js-file");
var pluralize = require("../../helpers/helper").pluralize;
const config_database = path.join(
  global.SERVER_ROOT,
  "database",
  "config.json"
);

module.exports = function(sandbox) {
  var app = sandbox.app;
  var db = sandbox.db;
  var router = sandbox.router;

  return {
    basepath: "",
    init: function(schema) {
      const self = this;
      db.on("schema", function(schema) {});
      schema.virtual("alias").get(function() {
        let lang = this.lang || "es";
        try {
          return pluralize(lang, this.name);
        } catch (err) {
          return this.name;
        }
      });
      schema.virtual("slug").get(function() {
        try {
          return "/app/" + pluralize(this.lang || "es", this.name);
        } catch (err) {
          return this.name;
        }
      });

      schema.virtual("type").get(function() {
        const CORE_MODELS = db.getCoreModels();
        return CORE_MODELS.includes(this.name) ? "core" : "app";
      });

      schema.statics.listar = function(params, callback) {
        db.schema.find(params, function(err, docs) {
          if (callback != undefined) {
            callback(err, docs);
          }
        });
      };
      schema.statics.add = function(params) {
        var self = this;
        return new Promise((resolve, reject) => {
          let { name, config, lang } = params;

          config = typeof config === "object" ? JSON.stringify(config) : config;

          let msg =
            typeof id !== "undefined"
              ? "Esquema actualizado."
              : "Esquema creado con éxito.";

          let modelTpl = `
						const Donec = require("../../Donec");
						const Schema = ${config}
						const Model = new Donec.Model("${name}",Schema,"${lang}");
						module.exports = Model;`;

          modelTpl = beautifyJS(modelTpl, {
            indent_size: 2,
            space_in_empty_paren: true
          });
          const filefullpath = path.join(
            global.APP_PATH,
            "models",
            `${name}.js`
          );

          //CREAR EL ARCHIVO!!
          fs.writeFile(filefullpath, modelTpl, function(err) {
            if (err) {
              //console.log(err);
              return reject(err);
            }
            self.create(params, function(err, doc) {
              if (err) {
                switch (err.code) {
                  case 11000:
                    msg = "Ya existe un esquema con el nombre " + name + ".";
                    break;
                  default:
                    msg = err.errmsg;
                    break;
                }
                reject({ success: false, msg: msg });
                return;
              }
              if (!doc) {
                reject({ success: false, msg: err.errmsg });
              } else {
                db.refresh(function(err, schema) {
                  resolve({ success: true, msg: msg, _id: doc._id });
                });
                /*var poolDb = db.getActiveGroup();
									poolDb.collections.push({
										"name":name,
										"config":config,
										"lang":lang
									});
									//Controlar escape de comillas
									poolDb.collections.forEach(function(item,index){
										if(typeof(item.config)=='string'){
											item.config = JSON.parse(item.config);
										}
									});

									db.updateActiveGroup(poolDb)
									.then(function(){
										console.log("Archivo de configuración actualizado.",poolDb);
										db.refresh(function(err,schema){
											resolve({"success":true,"msg":msg,"_id":doc._id});
										});
									},function(err){
										//console.log(err);
										reject({"success":false,"msg":"No se pudo modificar el archivo de configuración."});
									});*/
              }
            });
          });
        });
      };
      schema.statics.updateModel = function(params) {
        // delete params["_id"];

        //params.config = params.config.replace(/#/g,'');

        return new Promise((resolve, reject) => {
          if (typeof params.config === "string") {
            params.config = params.config.replace(/\s/g, "");
            try {
              params.config = JSON.parse(params.config);
            } catch (err) {
              return reject({ success: false, msg: err.message });
            }
          }

          console.log("::::: PUT: update schemas", params);
          if (typeof params.state !== "undefined") {
            params["state"] = "1";
          }

          db.schema.findById(params._id, function(err, doc) {
            if (err) {
              return reject({ success: false, msg: err });
            }
            if (!doc) {
              reject({ success: false, msg: "No se encontró el registro." });
            } else {
              var find = JSON.parse(doc.config);
              var input = params.config;

              const filefullpath = path.join(
                global.APP_PATH,
                "models",
                `${doc.name}.js`
              );

              db.schema.create(params, function(err, doc) {
                if (err) {
                  reject({ success: false, msg: err.message });
                  return;
                }
                if (!doc) {
                  reject({ success: false, msg: err });
                } else {
                  var msg = "Registro actualizado con éxito.";

                  replace(filefullpath, find, input, function(err, file_path) {
                    if (err) {
                      //console.log(err.message,find);
                      return reject({ success: false, msg: err.message });
                    }

                    /*
                     * AL ACTUALIZAR NO ESTÁ INSTANCEANDO UN NUEVO MODELO
                     */
                    var schema = db.redefineSchema(params).then(
                      function(model) {
                        //REDEFINE MODELO
                        db[doc.name] = model;

                        resolve({ success: true, msg: msg, _id: doc._id });
                        /* db.schema.create(params,function(err,doc){
													if(err){
														reject({"success":false,"msg":err.message});
														return;
													}
													if(!doc){
														reject({"success":false,"msg":err});
													}else{
		
														var msg = "Registro actualizado con éxito.";
														resolve({"success":true,"msg":msg,"_id":doc._id});
													}
												}); */
                      },
                      function(err) {
                        reject({ success: false, msg: err.message });
                      }
                    );
                  });
                }
              });
            }
          });
        });
      };
      /*
       * @Routes
       *
       */
      app.get("/admin/schemas", this.listar);
      app.post("/admin/schemas", this.create);
      app.put("/admin/schemas/:_id", this.update);
      app.delete("/schemas/:_id", this.remove);
      app.get("/refresh", this.refresh);
      app.get("/get_fields", this.getFieldsMap);

      //console.log("\t::: ADMIN SCHEMA INIT! :::",this);
    },
    update: function(req, res) {
      var { name, config, lang } = req.body;
      var self = this;
      var _id = req.params._id;

      db.schema
        .updateModel({
          _id,
          name,
          config,
          lang
        })
        .then(response => {
          return res.json(response);
        })
        .catch(error => {
          return res.json(error);
        });
    },
    remove: function(req, res) {
      var params = req.body;
      params["_id"] = req.params._id;

      const CORE_MODELS = db.getCoreModels();
      db.schema.findById(params._id, function(err, doc) {
        /*if(doc){
					res.send(JSON.stringify({"success":false,"msg":doc.alias}));
					return;
				}*/
        if (err) {
          return res.send(JSON.stringify({ success: false, msg: err }));
          return;
        }
        if (!doc) {
          return res.send(
            JSON.stringify({
              success: false,
              msg: "No se encontró el registro."
            })
          );
        } else {
          let name = doc.name;
          /*Helper.readFile(config_database).
					then(function(config){*/

          let filefullpath = path.join(global.APP_PATH, "models", `${name}.js`);
          if (CORE_MODELS.includes(name)) {
            return res.json({
              success: false,
              msg: "No se puede eliminar un modelo del Core"
            });
          }
          db.schema.findByIdAndRemove(params._id, function(err) {
            //Eliminar de Schema
            //console.log("DELETE: remove "+name+'('+((err)?"Fail":"succefully")+')');

            try {
              if (!Helper.existsFile(filefullpath)) {
                //Eliminar collection
                db.dropCollection(name, function(err, result) {
                  if (err) {
                    return res.send(
                      JSON.stringify({ success: true, msg: err.message })
                    );
                  }
                  return res.send(
                    JSON.stringify({
                      success: true,
                      msg: "Registro eliminado con éxito."
                    })
                  );
                });

                /* .then(function(){
											res.send(JSON.stringify({"success":true,"msg":"Registro eliminado con éxito."}));																	
										},function(err){
											res.send(JSON.stringify({"success":true,"msg":"La collección ya fue eliminada."}));
										}); */
              } else {
                Helper.unlink(filefullpath)
                  .then(() => {
                    //Eliminar collection
                    db.dropCollection(name, function(err, result) {
                      if (err) {
                        return res.send(
                          JSON.stringify({ success: true, msg: err.message })
                        );
                      }
                      return res.send(
                        JSON.stringify({
                          success: true,
                          msg: "Registro eliminado con éxito."
                        })
                      );
                    });
                  })
                  .catch(err => {
                    return res.json({
                      success: false,
                      msg: err.message
                    });
                  });
              }
            } catch (err) {
              return res.send(
                JSON.stringify({
                  success: true,
                  msg: "La collección ya fue eliminada."
                })
              );
              return;
            }
          });

          /*Helper.writeFile(config_database,config)
						.then(function(){
							res.send(JSON.stringify({"success":true,"msg":"Registro eliminado con éxito."}));
						},function(err){
							console.log("No se pudo modificar el archivo de configuración.");
							res.send(JSON.stringify({"success":false,"msg":"No se pudo modificar el archivo de configuración."}));
						});
						return;*/
          /*},function(err) {
						res.send(JSON.stringify({"success":false,"msg":err}));
					});*/
        }
      });
    },
    create: function(req, res) {
      var { name, config, lang } = req.body;
      db.schema
        .add({
          name,
          config,
          lang
        })
        .then(response => {
          return res.json(response);
        })
        .catch(error => {
          return res.json(error);
        });
    },
    refresh: function(req, res) {
      db.refresh(function(err, schema) {
        if (err) {
          return res.json({
            success: false,
            msg: err.message
          });
        } else {
          return res.send("Aplicaión actualizada.");
        }
      });
    },
    getFieldsMap: function(req, res) {
      let { name } = req.query;
      return res.send(JSON.stringify(db[name].getFieldsMap()));
    },
    listar: function(req, res) {
      var params = req.query || {};

      //console.log("LISTAR SCHEMAS!");

      db.schema.listar(params, function(err, data) {
        if (err) {
          return res.json({
            msg: err.message,
            data: [],
            success: false
          });
        }
        return res.json({
          data,
          success: true
        });
      });
    }
  };
};
