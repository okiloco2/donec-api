var md5 = require("md5");
//Users
module.exports = function(sandbox) {
  const { db } = sandbox;
  return {
    init: function(schema) {
      var self = this;
      //Ejemplo de Virtual
      schema.virtual("alias").get(function() {
        return this.name + "-lord";
      });
      schema.statics.listar = function(params, callback) {
        var result = [];
        db.group.query(params, function(err, query) {
          var cursor = query
            .populate("modules.module")
            //.select('')
            .cursor()
            .eachAsync(function(group) {
              result.push(group);
            })
            .then(res => {
              callback(result);
            });
        });
      };
      sandbox.get("/groups", function(req, res) {
        self.listar(req.query).then(function(docs) {
          return res.send(
            JSON.stringify({
              data: docs
            })
          );
        });
      });
      /**
       * Restringir eliminación y creación de grupos por defecto.
       */
      sandbox.delete("/groups/:_id", function(req, res) {
        return res.json({
          success: false,
          msg: "No se puede eliminar grupos"
        });
      });
      /* sandbox.post("/groups",function(req,res){
				res.json({
					success:false,
					msg:"No se puede crear grupos"
				});
			}); */
    },
    listar: function(params) {
		console.log("Params Filter:",params);
      return new Promise(function(resolve, reject) {
        db.group.listar(params, function(docs) {
          resolve(docs);
        });
      });
    }
  };
};
