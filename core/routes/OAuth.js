var md5 = require("md5");
var Helper = require("../../helpers/helper");
var Constants = require("../../helpers/Constants");
const path = require("path");
var jwt = require("jsonwebtoken");

var app_config = path.join(global.SERVER_ROOT, "app.json");

module.exports = function(sandbox) {
  var app = sandbox.app;
  var db = sandbox.db;
  var io = sandbox.io;

  return {
    init: function() {
      const self = this;
      //Evento Constructor User - Se dispara cuando el Schema user ha sido instanciado.
      db.on("user", function(schema) {
        //Extendemos la funcionalidad del Schema para usar en el Modelo User.
        schema.statics.login = function(params) {
          var self = this;
          return new Promise(function(resolve, reject) {
            var result = {};
            self
              .findOne(params)
              .populate("usergroup")
              .select("username email usergroup modules")
              .cursor()
              .eachAsync(function(user) {
                //Si hay datos entra a recorrer
                user.usergroup.modules.forEach(function(docs, index, arr) {
                  db.module.findOne(docs.module, (err, doc) => {
                    user.usergroup.modules[index] = doc;
                  });
                });
                //user.usergroup.modules=modules;
                resolve(user);
              })
              .then(function(data) {
                reject("Usuario y/o contraseña invalidos.");
              });
          });
        };
        schema.statics.authenticate = function(params) {
          var self = this;
          return new Promise(function(resolve, reject) {
            var result = {};
            self
              .findOne(params)
              .populate("usergroup")
              //.select('username email')
              .cursor()
              .eachAsync(function(user) {
                const payloads = {
                  email: params.email,
                  password: params.password,
                  group: user.usergroup.name
                };
                const Jwt = sandbox.loadPlugin("Jwtoken256");
                let token = Jwt.sign(payloads);
                /* var token = jwt.sign(params,process.env.SECRET_KEY,{
								expiresIn:process.env.SESSION_TIMEOUT
							}); */
                resolve({ user: user, token: token });
              })
              .then(function(data) {
                reject("Usuario y/o contraseña invalidos.");
              });
          });
        };
        schema.on("define", function(model) {
          app.get("/logout", function(req, res) {
            if (req.session.user_id) {
              req.session.destroy(function(err) {
                if (err) {
                  return res.send(err);
                }
                return res.send(
                  JSON.stringify({
                    success: true,
                    msg: "Session finalizada."
                  })
                );
              });
            } else {
              return res.send(
                JSON.stringify({
                  success: false,
                  msg: "No existe sisión de usaurio."
                })
              );
            }
          });
        });
      });
      app.post("/admin/login", this.login);
      app.post("/install", function(req, res) {
        var params = req.body;
        //Instalar Donec
        self.install(params).then(
          function(obj) {
            //Actualizar variables Globales
            global.config = obj;
            return res.send(
              JSON.stringify({
                success: true,
                msg:
                  "Donec Instalado con éxito, por favor utilice su cuenta de usuario: " +
                  params.email +
                  ", no olvide su contraseña para acceder al panel de administración.",
                user: obj
              })
            );
          },
          function(err) {
            // global.Msg("Atención","No se pudo crear el archivo de configuración");
            return res.send(
              JSON.stringify({
                success: false,
                msg: err.message || err
              })
            );
          }
        );
      });
      app.post("/config", function(req, res) {
        var params = req.body;

        Helper.readFile(app_config).then(function(config) {
          for (var key in config) {
            if (typeof config[key] == "object") {
              for (var s in config[key]) {
                if (params[s] != null) {
                  config[key][s] = params[s];
                }
              }
            } else {
              if (params[s] != null) {
                config[key] = params[key];
              }
            }
          }
          for (var key in params) {
            if (params[s] != null) {
              config[key] = params[key];
            }
          }
          //console.log(config);
          Helper.writeFile(app_config, config).then(
            function() {
              return res.send({
                success: true,
                msg: "Configuración Actualizada con éxito.",
                config: config
              });
            },
            function(err) {
              return res.send({
                success: false
              });
            }
          );
        });
      });
      app.get("/config", this.get_config);

      app.post("/change_password", this.change_password);
      app.post("/get_key_access", this.get_key_access);
    },
    install: function install(params) {
      return new Promise(function(resolve, reject) {
        try {
          if (db.getStatus() === 0) {
            reject({
              message: Constants.TEXT_STATUS_DISCONNECTED
            });
          }
          if (!params.password || !params.email) {
            reject(error);
          }
          db.group.findOne({ name: "superadmin" }, function(err, doc) {
            if (!doc) {
              //console.log("No existe el grupo.");
              // global.Msg("Atención","No existe el grupo.");

              db.module.create(
                {
                  // config:"{\"title\": \"Usuarios\",\"config\": {\"className\":\"Admin.view.users.Users\",\"alias\":\"users\",\"iconCls\":\"fa fa-folder\"}}",
                  config:
                    '{ "path": "/", "exact": "true", "name": "Home", "component": "Home", "menu":"false" }',
                  name: "Home"
                },
                function(err, module) {
                  if (err) {
                    reject(err);
                  }
                  db.group.create(
                    {
                      name: "superadmin",
                      modules: module.id
                    },
                    function(err, group) {
                      if (err) {
                        reject(err);
                      }

                      //Crear Usuario
                      db.user.create(
                        {
                          password: md5(params.password),
                          usergroup: group.id,
                          email: params.email || ""
                        },
                        function(err, user) {
                          if (err) {
                            reject(err);
                          }

                          var obj = {};
                          obj["user"] = {
                            email: params.email || "",
                            instaled_at: new Date()
                          };
                          delete params.password;
                          delete params.email;

                          for (var key in params) {
                            obj[key] = params[key];
                          }
                          Helper.writeFile(app_config, obj).then(
                            function() {
                              //console.log("Archivo de configuración creado.",obj);
                              resolve(obj);
                            },
                            function(err) {
                              reject(err);
                              //global.Msg("Error",err);
                            }
                          );
                        }
                      );
                    }
                  );
                }
              );
            } else {
              try {
                db.user.count({}, (err, count) => {
                  if (err) {
                    reject(err);
                  }
                  if (count === 0) {
                    //Crear Usuario
                    db.user.create(
                      {
                        password: md5(params.password),
                        usergroup: doc.id, //Grupo Super Administrador
                        email: params.email
                      },
                      function(err, user) {
                        if (err) {
                          reject(err);
                        }

                        var obj = {};
                        obj["user"] = {
                          email: params.email || "",
                          instaled_at: new Date()
                        };
                        delete params.password;
                        delete params.email;

                        for (var key in params) {
                          obj[key] = params[key];
                        }
                        Helper.writeFile(app_config, obj).then(
                          function() {
                            //console.log("Archivo de configuración creado.",obj);
                            resolve(obj);
                          },
                          function(err) {
                            reject(err);
                            //global.Msg("Error",err);
                          }
                        );
                      }
                    );
                  } else {
                    reject(
                      "El proyecto ya está instalado. Por favor inice sesión como Super Administrador."
                    );
                  }
                });
              } catch (error) {
                reject(error);
              }
            }
          });
        } catch (error) {
          console.log("ERROR: ", error.message);
          reject(error);
        }
      });
    },
    change_password: function(req, res) {
      var params = req.body;

      //console.log(params);
      db.user.findById(req.session.user_id, function(err, doc) {
        if (err) {
          return res.send({
            success: false,
            msg: "No exite usuario en la sesión."
          });
        }
        if (doc.password != md5(params.current_password)) {
          return res.send({
            success: false,
            msg: "Contraseña actual invalida."
          });
        }
        doc.password = md5(params.password);
        doc.save(function(err, doc) {
          req.session.destroy(function(err) {
            if (err) {
              return res.send(err);
            }
            return res.send({
              success: !err,
              msg: "Contraseña Actualizada."
            });
          });
        });
      });
    },
    get_key_access: function(req, res) {
      var params = req.body;
      if (params.password != undefined) {
        params.password = md5(params.password);
      }
      db.user
        .findOne({ email: params.email, password: params.password })
        .then(function(user) {
          if (user) {
            var token = jwt.sign(params, process.env.SECRET_KEY, {
              expiresIn: process.env.SESSION_TIMEOUT
            });
            return res.json({
              success: true,
              msg: "Usuario autorizado.",
              token: token
            });
          } else {
            return res.json({
              success: false,
              msg: "Usuario no autorizado."
            });
          }
        });
    },
    get_config: function(req, res) {
      Helper.readFile(app_config).then(
        function(config) {
          return res.send({
            success: !Helper.isEmpty(config),
            config
          });
        },
        function(err) {
          return res.send({
            success: false
          });
        }
      );
    },
    login: function(req, res, next) {
      var params = req.body;

      console.log("LOGINNN!!!",req.headers);
      if (!db.user) {
        return res.send(
          JSON.stringify({
            success: false,
            msg: Constants.TEXT_STATUS_DISCONNECTED
          })
        );
      } else {
        //Llamar funcion login del Modelo user
        db.user
          .authenticate({ email: params.email, password: md5(params.password) })
          .then(function(auth) {
            let { user } = auth;
            global.session = req.session;
            req.session.user_id = user._id;
            //console.log(">> userId: ", req.session.user_id);
            return res.status(200).json({
              user: auth.user,
              token: auth.token,
              success: true
            });
          })
          .catch(err => {
            return res.send(JSON.stringify({ msg: err, success: false }));
          });
      }
    }
  };
};
