import React, { Component } from 'react';
import AppMenu from './AppMenu.jsx';
class AppSidebar  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {open: true};
  	}
    componentDidMount() {
      //console.log("AppSideBar - routes: ",this.props.routes);
    }
  	render(){
  		return(
  			<div className="navbar">
  				<AppMenu nestedListStyle="item-list" items={this.props.items} routes={this.props.routes}/>
  			</div>
		)
  	}
}
export default AppSidebar;
