/**
* @Helpers: Funciones para reutilizar
*/
var pluralizeES= require('pluralize-es');
var pluralizeEN = require('pluralize');
var capitalize = require('string-capitalize');
var jsonfile = require('jsonfile');
var csv = require("fast-csv");
const fs = require("fs");
const path = require("path");


function getFilesizeInBytes(filename) {
    const stats = fs.statSync(filename)
    const fileSizeInBytes = stats.size
    return fileSizeInBytes
}

module.exports = {
	CONFIG_DB:"./database/ManagerDB",
	APP_CONFIG:'./server/app.json',
	isEmpty:function(obj){
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		var str = null;

	      // null and undefined are "empty"
	      if (obj == null) return true;
	      
	      // If it isn't an object at this point
	      // it is empty, but it can't be anything *but* empty
	      // Is it empty?  Depends on your application.
	      if (typeof obj !== "object") return true;

	      // Assume if it has a length property with a non-zero value
	      // that that property is correct.
	      if (obj.length > 0)    return false;
	      if (obj.length === 0)  return true;

	      // Otherwise, does it have any properties of its own?
	      // Note that this doesn't handle
	      // toString and valueOf enumeration bugs in IE < 9
	      for (var key in obj) {
	          if (hasOwnProperty.call(obj, key)) return false;
	      }
	      return true;
	},
	pluralize:function (lang,val){
		var _val = val;
		switch(lang){
			case "es":
				_val=pluralizeES(_val);
			break;
			case "en":
				_val=pluralizeEN(_val);
			break;
		}
		return _val;
	},
	capitalize:function(str){
		return capitalize(str);
	},
	readFile:function(file){

		return new Promise(function(resolve,reject){
			if (!fs.existsSync(file)) {

			    reject(`El archivo ${file} no existe.`);
			    return;
			}else{
				if(getFilesizeInBytes(file)==0){
					//console.log(getFilesizeInBytes(file)==0);
					reject("El archivo está vacio.");
					return;
				}
			}	
			try{
				jsonfile.readFile(file,function(err,obj){
					if(err){ reject(err); return;}

		    		//global.socket.emit("message","read file "+file);
					resolve(obj);

				});
				//console.log("read file: ",file);
			}catch(err){
				//global.socket.emit("error","Faile to read file "+file)
				console.log("faild read file: ",file);
				reject(err);	
			}
		});
	},
	writeFile:function(file,obj){
		
		return new Promise(function(resolve,reject){
			if (!fs.existsSync(file)) {
			    reject("El archivo no existe.");
			    //global.socket.emit("Error al Escribir archivo.");
			    return;
			}
			try{

				jsonfile.writeFile(file,obj,{spaces: 2, EOL: '\r\n'},function(err){
					if(err){ reject(err); return;}
		    		//global.socket.emit("message","El archivo no existe.");
					//console.log("write file:",file,obj);
					resolve();
				});
			}catch(err){
				//global.socket.emit("error","Faile to write file "+file)	
				//console.log("faild read file: ",file);
				reject(err);	
			}
		});
	},
	readCsv:function(file){
		return new Promise(function(resolve,reject){
			fs.createReadStream(file)
			.pipe(csv())
			.on("data",function(data){
				//console.log("data ",data);	
			})
			.on("end",function(data){
				//console.log("Read finish.",data);
				resolve(data);
			});
		});
	},
	writeCsv:function(file){
		return new Promise(function(resolve,reject){

		});
	},
	uniqArray:function(data,field){
		var result =[];
		var obj_data = [];
		if(data.length==0){return result;}
		if(field){
			if(typeof(data[0]) === 'object'){
				data.forEach(item => {
					if(result.indexOf(item[field])<0){
						result.push(item[field]);
						obj_data.push(item);
				    }
				});
				return obj_data;
			}
		}
		data.forEach(item => {
			if(result.indexOf(item) < 0) {
		        result.push(item);
		    }
		});
		return result;
	},
	fileExists:function(filefullpath){
		let exist = true;
		return new Promise((resolve,reject) => {
			if(!filefullpath){reject("Not file path.");return;}
			fs.access(filefullpath, fs.F_OK, (err) => {
				if(err){
					if(err.code === "ENOENT"){
						resolve(false);
					}
					return;						
				}
				resolve(true);
			});
		});		
	},
	createDirectoryAndFile:function(folder,filename,content){
		
		let filefullpath = path.join(folder,filename);

		return new Promise((resolve,reject) => {
			if(!fs.existsSync(folder)) {//Si no existe crea el directorio
				fs.mkdirSync(folder);
			}
			this.fileExists(filefullpath)
			.then(exists => {
				if(!exists){
					fs.writeFile(filefullpath,content, function(err) {
						if(err){
							reject(err);
							return;
						}
						resolve(exists);
					});
				}else{
					resolve(exists);
				}
			});			
		});
	},
	getFilesize:getFilesizeInBytes 
}