var md5 = require("md5");
module.exports = function({ db, app }) {
  createUser = params => {
    //console.log("POST - User / New|Update");
    return new Promise((resolve, reject) => {
      if (params.password != undefined) {
        params.password = md5(params.password);
      }
      if (!params.usergroup || !params.email) {
        reject({
          success: false,
          message: "Debe especificar un grupo o email."
        });
        return;
      }
      if (params.id != undefined && params.id != "") {
        db.user.findOne({ _id: params.id }).then(function(user) {
          if (!user) {
            reject({
              success: false,
              msg: "El usuario no existe."
            });
          } else {
            db.user.create(params, function(err, doc) {
              resolve({
                success: true,
                msg: !params.id
                  ? "Usuario creado con éxito."
                  : "Usuario actualizado con éxito."
              });
            });
          }
        });
      } else {
        db.user.findOne({ email: params.email }).then(function(user) {
          if (!user) {
            db.group.findOneById(params.usergroup, function(err, doc) {
              if (!doc) {
                reject({
                  success: false,
                  msg: "El grupo de usuario no existe."
                });
              } else {
                db.user.create(params, function(err, doc) {
                  resolve({
                    success: true,
                    _id: doc._id,
                    msg: !params.id
                      ? "Usuario creado con éxito."
                      : "Usuario actualizado con éxito."
                  });
                });
              }
            });
          } else {
            reject({
              success: false,
              msg: "No se pudo crear porque el usuario ya existe."
            });
          }
        });
      }
    });
  };

  return {
    init: function(schema) {
      /*sandbox se utiliza para crear rutas para la aplicación
       *Ejemplo: sandbox.post("/users",self.insert_user);
       *El endpoit sería: /app/users, para crear rutas especializadas se utiliza app. */
      app.post("/admin/users", this.validate_user);
      app.post("/app/users", this.validate_user);

      //this.hola = this.hola.bind(this);
      app.get("/admin/users", this.list_users);
    },
    insert_user: function(params) {
      //console.log("POST - User / New|Update");

      return new Promise((resolve, reject) => {
        let { password, email, usergroup, id } = params;

        if (params.password != undefined) {
          params.password = md5(params.password);
        }

        //console.log(params);


        if (params.id != undefined && params.id != "") {
          db.user.findOne({ _id: params.id }).then(function(user) {
            if (!user) {
              return res.send(
                JSON.stringify({
                  success: false,
                  msg: "El usuario no existe."
                })
              );
            } else {
              db.user.create(params, function(err, doc) {
                return res.send(
                  JSON.stringify({
                    success: true,
                    msg: !params.id
                      ? "Usuario creado con éxito."
                      : "Usuario actualizado con éxito."
                  })
                );
              });
            }
          });
        } else {
          db.user.findOne({ email: params.email }).then(function(user) {
            if (!user) {
              db.group.findOneById(params.usergroup, function(err, doc) {
                if (!doc) {
                  return res.send(
                    JSON.stringify({
                      success: false,
                      msg: "El grupo de usuario no existe."
                    })
                  );
                } else {
                  db.user.create(params, function(err, doc) {
                    return res.send(
                      JSON.stringify({
                        success: true,
                        _id: doc._id,
                        msg: !params.id
                          ? "Usuario creado con éxito."
                          : "Usuario actualizado con éxito."
                      })
                    );
                  });
                }
              });
            } else {
              return res.send(
                JSON.stringify({
                  success: false,
                  msg: "No se pudo crear porque el usuario ya existe."
                })
              );
            }
          });
        }
      });
    },
    validate_user: function(req, res, next) {
      const self = this;
      let params = req.body;

      if (typeof global.default_group !== "undefined") {
        db.group.findOne({ name: global.default_group }, (err, doc) => {
          if (err) {
            return res.json({
              success: false,
              msg: err.message
            });
          }

          if (!doc) {
            //Si el grupo No existe
            db.group.create(
              {
                name: global.default_group
              },
              function(err, doc) {
                if (err) {
                  if (err.code !== 11000) {
                    //11000: DuplicateKey
                    return res.json({ success: false, msg: err.message });
                  }
                }
                this.createUser(params).then(
                  response => {
                    return res.json(response);
                  },
                  err => {
                    return res.json(err);
                  }
                );
              },
              function(err) {
                return res.json({ success: false, msg: err.message });
              }
            );
          } else {
            this.createUser(params).then(
              response => {
                return res.json(response);
              },
              err => {
                return res.json(err);
              }
            );
          }
        });
      } else {
        return res.json({
          success: false,
          msg: "No se ha definido un rol de usuario por defecto."
        });
      }
    },
    list_users: (req, res) => {
      const params = req.query;

      db.user.listar(params).then(data => {
        return res.json({
          success: true,
          data
        });
      });
    }
  };
};
