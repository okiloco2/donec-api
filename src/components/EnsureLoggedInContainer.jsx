import React, { Component } from 'react';
import { withRouter } from 'react-router';

class EnsureLoggedInContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      history:[]
    }
    this.isLoggedIn = this.isLoggedIn.bind(this);
  }
  componentWillUnmount(props){
    alert("??");
  }
  componentDidMount() {
    const { history } = this.props || [];
    console.log(this.isLoggedIn())
    if (!this.isLoggedIn()) {
       history.push("/login");
    }
  }
  isLoggedIn(){
    return (window.localStorage.getItem("user")!=null);
  }
  render() {
    if (this.isLoggedIn()) {
      return this.props.children || <h2>->Hola mundo</h2>
    } else {
      return <h2>Hola mundo</h2>
    }
  }
}
export default withRouter(EnsureLoggedInContainer);