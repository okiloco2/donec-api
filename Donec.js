"use strict";
const express = require("express");
const favicon = require("serve-favicon");
const { createLogger, format, transports } = require("winston");
var http = require("https");
var md5 = require("md5");
var fs = require("fs");
var path = require("path");
var url = require("url");
var pem = require("pem");
var cors = require("cors");
var randtoken = require("rand-token");
var moment = require("moment-timezone");
var bodyParser = require("body-parser");

var session_middleware = require("./middlewares/session");
var session = require("express-session");
//var RedisStore = require("connect-redis")(session);

/* var SessionManager = require("./core/classes/SessionManager"); */

const TEXT_CONSTANTS = require("./helpers/Constants");

//var dbstatus_middleware = require("./core/middlewares/dbstatus");
var api_middleware = require("./middlewares/api");
//var admin_middleware = require("./middlewares/admin");

var online = false;
var Api, Admin;
var events = require("events"); //Events administra eventos
const mongoose = require("mongoose");
const ManagerDB = require("./core/ManagerDB");
mongoose.Promise = global.Promise;

var Helper = require("./helpers/helper.js");
var Constants = require("./core/helpers/Constants.js");
const DonecRouter = require("./core/classes/DonecRouter");
const SocketIo = require("./core/classes/Socketio");
const __basedir = path.dirname(
  require.main.filename || process.mainModule.filename
);

var socket;
var cookieParser = require("cookie-parser");
var app = express();

/* AQUI */
var redisStore = require("connect-redis")(session);
const redis = require("redis");
var client = redis.createClient();

app.use(bodyParser.json());
app.use(cookieParser("secretSign#143_!223"));
app.use(bodyParser.urlencoded({ extended: true }));

/* app.post("/admin/login", (req, res) => {
  let { email, password } = req.body;
  req.session.userId = Math.random(10) * 40;
  res.status(200).json({
    msg: "Success",
    token: "XXX"
  });
}); */

/* AUI END */

var server = require("http").Server(app);
var io = require("socket.io")(server);

process.env.SECRET_KEY = randtoken.uid(256); //Random secret key
process.env.SESSION_TIMEOUT = "1d"; //Tiempo de caducar la Session "1d", "20h", 60
const DEFAULT_PORT = process.env.PORT || 8443;

process.env.ACTIVE_GROUP = "donec";

var Donec = Donec || {};

Donec.Types = {};
var types = ["Date", "Object"];
types = types.concat(Object.keys(mongoose.Types));

types.forEach(item => {
  Donec.Types[item] = item;
});

//Middlewares
/* app.use(bodyParser.json()); //para peticiones aplication/json
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(
  session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: false,
    cookie: {
      sameSite: false, // You probably want to use 'lax' here
      secure: false,
      maxAge: 1000 * 60 * 60 * 3
    }
  })
);

//Use Cors all origin domains
app.use(cors()); */

let plugins = {};
let plugin_instance = {};

/*
 * String utils
 */
String.prototype.capitalize = function() {
  return this.replace(/(?:^|\s)\S/g, function(a) {
    return a.toUpperCase();
  });
};
String.prototype.format = function() {
  var a = this;
  var k;
  for (k in arguments) {
    a = a.replace("{" + k + "}", arguments[k]);
  }
  return a;
};

var EventEmitter = function() {
  var self = this;

  //console.log(msg);
  const logger = createLogger({
    format: format.combine(format.splat(), format.simple()),
    transports: [
      new transports.File({
        filename: "errors.log",
        level: "error"
      })
    ]
  });

  return {
    create: function(Donec, app, db, io, router, moduleId) {
      var module = Donec.modules[moduleId];
      var schema, custom, base_path;
      const me = this;
      me.routes = me.routes || [];

      if (typeof module !== "undefined") {
        custom = module["custom"];
        base_path = module["path"];
        schema = module["schema"];
      }

      db.on("ready", function() {
        if (!schema) {
          //console.log("Custom Router: ",moduleId,base_path);
        }

        /*  var redis = require("redis");
        var client = redis.createClient();
        client.publish("ready", {}.toString); */

        /**
         * Si el router es personalizado.
         * */
        if (!schema) app.use(base_path, router);
      });
      return {
        log: {
          error: function(message, _private) {
            _private = _private || true;
            let level = "error";
            logger.log({
              private: _private,
              level,
              message
            });
          },
          info: function(message, _private) {
            _private = _private || true;
            let level = "info";
            logger.log({
              private: _private,
              level,
              message
            });
          },
          debug: function(message, _private) {
            _private = _private || true;
            let level = "debug";
            logger.log({
              private: _private,
              level,
              message
            });
          },
          warn: function(message, _private) {
            _private = _private || true;
            let level = "warn";
            logger.log({
              private: _private,
              level,
              message
            });
          }
        },
        notify: function(evt) {
          if (typeof evt == "object") {
            //console.log(evt["type"]);
            self.emit(evt.type, evt.options);
          }
        },
        listen: function(evt, callback) {
          //console.log("listen: ",evt);
          self.on(evt, callback);
        },
        use: function(route_path, callback, router) {
          //console.log("use ",route);
          if (typeof router !== "undefined") {
            app.use(route_path, router);
          } else {
            if (typeof callback !== "undefined") {
              app.use(route_path, callback);
            } else {
              app.use(route_path, function(req, res, next) {
                next();
              });
            }
          }
        },
        get: function(path, callback) {
          return this.route({ path, method: "get" }, callback);
        },
        post: function(path, callback) {
          return this.route({ path, method: "post" }, callback);
        },
        put: function(path, callback) {
          return this.route({ path, method: "put" }, callback);
        },
        update: function(path, callback) {
          return this.route({ path, method: "put" }, callback);
        },
        delete: function(path, callback) {
          return this.route({ path, method: "delete" }, callback);
        },
        route: function(config, callback) {
          let { path, method } = config;
          //console.log(path, callback);
          if (typeof path != "string") {
            return;
          }

          let namespace = `${method.toLowerCase()} ${base_path}`;
          if (!schema) {
            /* console.log(`*path ${method.toUpperCase()}: `, base_path + path); */
            namespace = `${path}`;
          } else {
            /* console.log(
              `-overwrite path: ${method.toUpperCase()}: `,
              base_path
            ); */
          }
          me.routes[path] = {
            [method]: callback
          };
          //>_Agregar Rutas para Socket Io
          let prev_routes = app.get("routes") || {};
          let item_route = {};
          let key = [namespace];
          let method_key = method;
          item_route[`${method_key} ${key}`] = callback;
          app.set("routes", { ...prev_routes, ...item_route });
          return router.route(path)[method](callback);
        },
        restrict_access(req, res, next) {
          return res.json({
            success: false,
            msg: TEXT_CONSTANTS.TEXT_RESTRICT_ACCESS
          });
          return;
        },
        restrict(original_url, method) {
          let restrict_access = global.restrict_access;

          const _regex =
            "(" + method.toUpperCase() + ")\\s(/){0,1}(" + original_url + ")";
          const str_regex =
            "(" +
            method.toUpperCase() +
            ")\\s(/){0,1}([(\\w+(/){0,1})]+)[\\w+]";

          let restrinct = {};
          let _match = restrict_access.match(_regex);
          if (_match) {
            _match.forEach(item => {
              if (item) {
                if (item.match(str_regex)) {
                  let _method = item.substring(0, item.indexOf(" "));
                  //console.log("Match: ",original_url, item," Method: "+_method);
                  restrinct[original_url] = _method.toLowerCase();
                }
              }
            });
          }
          return restrinct;
        },
        getApp: function() {
          return app;
        },
        getCurrentActiveGroup() {
          return new Promise((resolve, reject) => {
            Helper.readFile(config_database).then(function(config) {
              let active_group = config[config.active_group];
              resolve(active_group);
            });
          });
        },
        getRouter: function(name) {
          switch (name) {
            case "admin":
              return this.AdminRouter;
              break;
            default:
              return this.router;
              break;
          }
        },
        loadPlugin: function(pluginId) {
          return plugin_instance[pluginId.toLowerCase()];
        },
        app,
        db,
        io,
        router
      };
    }
  };
};

/*Permite emitir Eventos personalizados*/
EventEmitter.prototype = new events.EventEmitter();

var Sandbox = new EventEmitter();

Donec.restart = function(callback) {
  let router = require("./Router");
  //console.log("##########################");
  if (this.db) {
    delete this.db;
    //Crear Instancia de Objeto Administrador de base de datos
    var db = ManagerDB.createManagerDB();
    router = require("./Router");
    Api = require("./Api");
    db.disconnect();
    //app.use("/app",router(app,db,io));
    Donec.Core(app, db, io, router);
    if (callback != undefined) {
      callback();
    }
  }
};

let initialized = false;
Donec.initializeApp = function(config) {
  /**
   * Set configuration
   */
  //Load default configuration
  const default_config = require("./core/donec.config.js");
  if (typeof config === "undefined") {
    config = default_config;
  }
  const port = config.port || DEFAULT_PORT;
  const __index = config.index || "/";
  const __basepath = config.basepath || "";
  const server_root = config.root || "server";
  const session_express =
    config.session_express || default_config.session_express;

  global.session_express = session_express;

  let db, router;
  let middlewares = {};
  /**
   ** Set Global params
   */
  global.config = config;
  global.default_config = default_config;
  global.APP_PATH = path.join(__basedir, __basepath); //Directorio de la Aplicación

  /**
   * Set type protocol
   */
  const protocol = config.protocol || default_config.protocol;
  global.protocol = protocol;

  const allowOrigins = config.allowOrigins || default_config.allowOrigins;

  global.session = config.session || default_config.session;

  /**
   * Configurar token
   */
  if ("token" in config) {
    let { key_secret, time_out } = config.token;
    process.env.SECRET_KEY = key_secret || process.env.SECRET_KEY;
    process.env.SESSION_TIMEOUT = time_out || process.env.SESSION_TIMEOUT;
    global.SESSION_TIMEOUT = time_out || process.env.SESSION_TIMEOUT;
  }
  /**
   * Define Restriction Access Paths
   */
  global.restrict_access = "";
  if ("restrict_access" in config) {
    if (
      typeof config.restrict_access === "object" &&
      Array.isArray(config.restrict_access)
    ) {
      global.restrict_access = config.restrict_access.join(" ");
    } else if (typeof config.restrict_access === "string") {
      global.restrict_access +=
        typeof config.restrict_access !== "undefined"
          ? " " + config.restrict_access
          : " ";
    }
  }
  if ("restrict_access" in default_config) {
    if (
      typeof default_config.restrict_access == "object" &&
      Array.isArray(default_config.restrict_access)
    ) {
      default_config.restrict_access = default_config.restrict_access.join(" ");
    } else if (typeof default_config.restrict_access == "string") {
      default_config.restrict_access =
        typeof default_config.restrict_access !== "undefined"
          ? " " + default_config.restrict_access
          : "";
    }
    global.restrict_access +=
      global.restrict_access + " " + default_config.restrict_access;

    var _paths = global.restrict_access;
    if (typeof _paths === "string") {
      _paths = _paths.match(
        /((GET)|(POST)|(DELETE)|(PUT))\s(\/)([(\w+(\/)?)]+)(\:[\w]+)?/g
      );
      let new_paths = [];
      new_paths = _paths.reduce((new_paths, str, index) => {
        const exclude_regex = `\-${str}`;
        const regx_exlude = new RegExp(exclude_regex, "g");
        var exclude_matchs = restrict_access.match(regx_exlude);

        if (!exclude_matchs) {
          //Revocar restricción de paths con Negación (-)
          if (new_paths.indexOf(str) === -1) {
            new_paths.push(str);
          }
        }
        return new_paths;
      }, new_paths);
      global.restrict_access = new_paths.join(" ");
    }
    //console.log("__paths: ",global.restrict_access);
  }

  /**
   * Allow user registration.
   */
  global.ALLOW_USER_REGISTRATION = default_config.allow_user_registration;
  if ("allow_user_registration" in config) {
    global.ALLOW_USER_REGISTRATION = config.allow_user_registration;
  }
  /**
   * Default group create
   **/
  global.default_group = default_config.default_group;
  if ("default_group" in config) {
    global.default_group = config.default_group;
  }
  /**
   * Define Global PATHS
   */
  global.SERVER_ROOT = path.join(__basedir, server_root); //Raiz del proyecto
  global.CORE_PATH = path.join(__dirname, "core"); //Directorio del Core
  global.SERVER_PATH = __dirname; //Directorio del servidor Donec

  global.active_group = default_config.database.active_group;
  if (typeof config.database !== "undefined") {
    global.active_group = config.database.active_group;
  }
  global.pool_database = config.database || default_config.database;

  process.env.ACTIVE_GROUP = global.active_group;
  //Definir Carpeta pública
  const public_dir = config.public_directory || path.join(__basedir, "public");
  app.use("/public", express.static(public_dir));

  const PATH_FAVICON = config.favicon || path.join(public_dir, "favicon.ico");
  //"public/images/favicon.ico"
  try {
    app.use(favicon(PATH_FAVICON));
  } catch (err) {
    console.log("Favicon ", err.message);
  }

  /* HAY QUE MEJORAR EL ALMACENAMIENTO DE LA SESIÓN Y CORS ORIGINS */
  /* app.use(
    session({
      secret: "lalala",
      resave: false,
      saveUninitialized: true,
      store: new redisStore({
        host: "localhost",
        port: 6379,
        client: client,
        ttl: 260
      })
    })
  ); */
  app.use(
    session({
      secret: process.env.SECRET_KEY,
      resave: false,
      saveUninitialized: false,
      cookie: {
        sameSite: false, // You probably want to use 'lax' here
        secure: false,
        maxAge: 1000 * 60 * 60 * 3
      }
    })
  );

  /* Para que funcione el almacenamiento de cookies desde otro origin */
  app.use(
    cors({
      origin: allowOrigins,
      methods: ["GET", "POST", "PUT", "DELETE"],
      credentials: true // enable set cookie
    })
  );

  //Puerto del servidor
  app.set("port", port);

  /**
   * Inicializar aplicación Donec.
   */
  function init() {
    if (!initialized) {
      //Crear directorio de administracion
      Helper.createDirectoryAndFile(
        public_dir,
        Constants.FILENAME_README,
        Constants.TEXT_INDEX
      ).then(
        () => {
          Helper.createDirectoryAndFile(
            public_dir,
            "index.html",
            Constants.HTML_CONTENT
          ).then(
            () => {
              let path_favicon = path.join(public_dir, "favicon.ico");
              let path_logo = path.join(public_dir, "DonecLogo.svg");

              Helper.fileExists(path_favicon).then(exists => {
                if (!exists) {
                  //Crear favicon
                  fs.createReadStream(
                    path.join("resources", "images", "favicon.ico")
                  ).pipe(fs.createWriteStream(path_favicon));
                }
              });
              //Crear imagen Logo
              Helper.fileExists(path_logo).then(exists => {
                if (!exists) {
                  fs.createReadStream(
                    path.join("resources", "images", "DonecLogo.svg")
                  ).pipe(fs.createWriteStream(path_logo));
                }
              });

              app.get(__index, function(req, res) {
                return res.sendFile(
                  path.join(global.SERVER_PATH, "public", "index.html")
                );
              });
              initialized = true;
            },
            err => console.log("ERROR: ", err.message)
          );
        },
        err => console.log("ERROR: ", err.message)
      );
    }
  }

  /**
   * Builder
   * @param {express} app
   * @param {ManagerDB} db
   * @param {SocketIO} io
   * @param {Router} router
   */
  function Builder(app, db, io, router) {
    var self = this;
    this.modules = {};
    this.services = {};
    this.plugins = {};
    this.debug = false;
    this.router = {};
    this.AdminRouter = {};
    this.app = app;
    this.db = db;
    this.io = io;

    //Create Instance
    function createInstance(moduleId) {
      let reserved_keywords = [
        "restart",
        "initializeApp",
        "Plugin",
        "loadPlugin",
        "Model"
      ];
      var moduleName = moduleId.capitalize();

      //console.log(">_moduleName: ", moduleName);
      try {
        if (reserved_keywords.indexOf(moduleId) !== -1) {
          throw new Error(`El moduleId ${moduleId} es una clave reservada.`);
        }
        var module = self.modules[moduleId];
        var schema = module["schema"];
        var router = self.router;
        if (!schema) {
          //Si no tiene schema es personalizado.
          router = express.Router();
        }
        var sandbox = Sandbox.create(self, app, db, io, router, moduleId);
        var service = self.services[moduleId];

        module["db"] = db;
        module["app"] = app;
        module["io"] = io;
        module["router"] = router;

        //module["router"] = new DonecRouter();
        var instance = module.creator(sandbox),
          name,
          method,
          output;
        /**
         * Los Servicios deben llevar el mismo nombre del Router
         * * Se quitó inicio automático enlazado con el router
         * * Los Servicios se pueden importar desde el router y ser usados directamente.
         */
        if (service != undefined) {
          //module["service"] = Donec.startService(moduleId,instance);
        }
        if (!self.debug) {
          for (var name in instance) {
            method = instance[name];
            if (typeof method == "function") {
              instance[name] = (function(name, method) {
                var evname = moduleId.capitalize() + name.capitalize();
                return function() {
                  try {
                    sandbox.notify({
                      type: "before" + evname,
                      options: arguments
                    });
                    output = method.apply(this, arguments);
                    sandbox.notify({ type: "after" + evname, options: output });
                    return output;
                  } catch (err) {
                    console.log(
                      "[Error] Module: ",
                      moduleId,
                      "Method:",
                      name,
                      err
                    );
                  }
                };
              })(name, method);
            }
          }
        }
        if (module.path != undefined) {
          sandbox.use(module.path);
        }
        module["getInstance"] = () => instance;

        instance["path"] = module.path;
        instance["parent"] = module.parent;

        return instance;
      } catch (err) {
        console.log("ERROR: ", err.message);
        return {};
      }
    }

    function createService(serviceId) {
      var instance = createInstance(serviceId);
      return instance;
    }
    //Incializa un modulo
    Donec.start = function(moduleId) {
      //console.log("Se va a iniciar: ",moduleId);
      // var sandbox = Sandbox.create(self,app,db,io,self.router);
      var module = self.modules[moduleId];

      // module.instance = module.creator(sandbox);
      //Definir el SCOPE de cada módulo
      module.instance = createInstance.call(module, moduleId);
      //console.log(">_instance", moduleId);
      let prev_instance = {};
      if (Donec[moduleId]) {
        prev_instance = Donec[moduleId];
      }
      Donec[moduleId] = { ...prev_instance, ...module.instance };

      if (typeof module.instance.init !== "undefined") {
        if (typeof module.instance.init == "function") {
          if (!module.schema) {
            module.instance.init(db, io);
          } else {
            //db.on(moduleId,function(schema){
            module.instance.init(module.schema);
            //});
            //console.log(">Init: ",moduleId);
          }
        }
      }
    };
    //Destruye un modulo
    Donec.stop = function(moduleId) {
      var data = self.modules[moduleId];
      if (data.instance) {
        data.instance.destroy();
        data.instance = null;
      }
    };
    //Inicializa todos los modulos
    Donec.startAll = function(moduleId) {
      var moduleData = self.modules;
      //console.log("-- Modules --",moduleData);
      for (var moduleId in moduleData) {
        //console.log("moduleId: ",moduleId);
        Donec.start(moduleId);
        if ("moduleId" in moduleData) {
          Donec.start(moduleId);
        }
      }
    };
    //Detener todos los modulos
    Donec.stopAll = function(moduleId) {
      var moduleData = self.modules;
      for (var moduleId in moduleData) {
        if ("moduleId" in moduleData) {
          Donec.stop(moduleId);
        }
      }
    };
    Donec.register = function(module, creator, custom) {
      custom = custom || false;
      var moduleId = module.name || module;
      var schema = module.schema;
      var modulePath = module.path;

      //console.log(">_* Register ",moduleId,modulePath);
      self.modules[moduleId] = {
        creator: creator,
        custom: custom,
        path: modulePath,
        parent: module.parent,
        schema: schema,
        instance: null
      };
    };
    Donec.define = function(moduleId, config, custom) {
      if (typeof config == "object") {
        var fn = function() {};
        var module = {};
        for (var k in config) {
          if (typeof config[k] == "function") {
            if (k == "init") {
              fn = config[k];
            }
          } else {
            module[k] = config[k];
          }
        }
        module["name"] = moduleId;
        Donec.register(module, fn);
      } else if (typeof config == "function") {
        Donec.register(moduleId, config, custom);
      }
      Donec.start(moduleId);
    };
    Donec.createService = function(serviceId, config) {
      if (typeof serviceId === "function" && typeof config === "undefined") {
        config = serviceId;
        serviceId = "service-" + moment().format("YmdHms"); //ServiceId
        console.log("serviceId: ", serviceId);
      }
      serviceId = serviceId.toLowerCase();
      self.services[serviceId] = {
        creator: config,
        instance: null
      };
      return Donec.startService(serviceId);
    };

    const debounce = fn => {
      return function() {
        const context = this;
        const args = arguments;
        fn.apply(context, args);
      };
    };

    Donec.startPlugin = function(pluginId) {
      var plugin = plugins[pluginId.toLowerCase()];
      if (!plugin) return;
      if (!plugin.instance) {
        plugin.instance = plugin.creator();
        if (
          plugin.instance.init != undefined &&
          typeof plugin.instance.init == "function"
        ) {
          plugin.instance.init(); //Iniciar Plugin
        }
      }
      plugin_instance[pluginId.toLowerCase()] = plugin.instance;
      return plugin.instance;
    };
    Donec.startService = function(serviceId, module) {
      var service = services[serviceId];
      if (!service.instance) {
        service.instance = service.creator(db);
        if (
          service.instance.init != undefined &&
          typeof service.instance.init == "function"
        ) {
          service.instance.init(db); //Iniciar Servicio
        }
      }
      return service.instance;
    };
    //Inicializa todos los Servicios
    Donec.startAllServices = function() {
      var serviceData = self.services;
      console.log("-- Services --");
      for (var serviceId in serviceData) {
        console.log(".. ", serviceId, " ..");
        Donec.startService(serviceId);
      }
    };
    this.router = router("/app", app, db, io);
    app.use("/app", this.router);
    //ApiRest - API Donec
    var ApiRouter = Api(self, app, db, io);
    //this.AdminRouter = Admin(self,app,db,io);
    //app.use("/admin",AdminRouter);

    /* var sessionManager = new SessionManager();
    var sessionMiddleware = sessionManager.start();

    if (typeof sessionMiddleware != "undefined") {
      app.use(sessionMiddleware);
    } */

    app.set("server", server);
    //app.set("session_middleware", sessionMiddleware);

    //Usar Sesión de Express
    if (session_express) {
      app.use("/admin", session_middleware);
      app.use("/app", session_middleware);
    }
    /**
     * #protocol
     * En esta sección se define el tipo de protocolo a utilizar
     * dependiendo de la propiedad "protocol", esta puede
     * tomar los valores: ("http", "socket" ó "http/socket")
     * "http": Inidica que el manejador de la comunicación solo se realiza mediante http,
     * "socket": Define la comunicación mediante socket,
     * "http/socket": permite la comunicación mixta, es decir tanto por: http como socket,
     * en caso de no estar definida se tomaran ambos protocolos.
     */

    var realtime, adminRouter, appRouter;

    switch (protocol) {
      case "http":
        adminRouter = new DonecRouter("/admin", app, db, io);
        appRouter = new DonecRouter("/app", app, db, io);
        break;
      case "socket":
        realtime = new SocketIo("/app", app, db, io);
        break;
      case "http/socket":
        adminRouter = new DonecRouter("/admin", app, db, io);
        appRouter = new DonecRouter("/app", app, db, io);
        realtime = new SocketIo("/app", app, db, io);
        break;
      default:
        adminRouter = new DonecRouter("/admin", app, db, io);
        appRouter = new DonecRouter("/app", app, db, io);
        realtime = new SocketIo("/app", app, db, io);

        break;
    }

    app.use("/api", ApiRouter);

    return {
      loadPlugins: function() {
        const default_config = global.default_config;
        const core_plugins = default_config.plugins;
        const app_plugins = global.config.plugins;
        //load core plugins
        let __plugins = {};
        let core_plugin_index = path.join("./", "core", "plugins", "index.js");

        if (core_plugins) {
          Helper.fileExists(core_plugin_index).then(exist => {
            if (exist) {
              __plugins["index-core"] = require("./core/plugins/index");
            }
            core_plugins.forEach(pluginId => {
              let plugin_path = "./core/plugins/" + pluginId;
              try {
                __plugins[pluginId] = require(plugin_path);
                //Donec.startPlugin(pluginId);
              } catch (err) {
                console.log(
                  "[Warning]: No se pudo cargar el Plugin: ",
                  pluginId,
                  err
                );
              }
            });
          });
        }
        var app_path_name = path.basename(global.APP_PATH);
        let app_plugins_path = "./" + app_path_name + "/plugins";
        let app_plugin_index = path.join(
          global.APP_PATH,
          "plugins",
          "index.js"
        );

        Helper.createDirectoryAndFile(
          app_plugins_path,
          Constants.FILENAME_README,
          Constants.TEXT_README_PLUGINS
        ).then(() => {
          if (app_plugins) {
            Helper.fileExists(app_plugin_index).then(exist => {
              if (exist) {
                let index_app = app_plugins_path + "/index";
                __plugins["index-app"] = require.main.require(index_app);
              }
              app_plugins.forEach(pluginId => {
                let plugin_path = app_plugins_path + "/" + pluginId;
                try {
                  if (!exist) {
                    __plugins[pluginId] = require.main.require(plugin_path);
                    console.log("EXT: ", __plugins[pluginId]);
                  }
                  Donec.startPlugin(pluginId);
                } catch (err) {
                  console.log(
                    "[Warning]: No se pudo cargar el Plugin: ",
                    pluginId
                  );
                }
              });
            });
          }
        });

        //Donec.startAllPlugins();var Test = require("./core/plugins/Test");
      },
      getModule: function(moduleId) {
        return self.modules[moduleId];
      },
      getDatabase: function() {
        return db;
      },
      io
    };
    // app.use("/admin",router("/admin",app,db,io));
  }
  /**
   * Carga los middleware
   * @param {string} _middlewar_path
   * @param {object} scope
   * @param {boolean} autouse
   */
  function loadMiddleware(_middlewar_path, scope, autouse) {
    let _middleware;
    autouse = autouse || true;

    let basename = path.basename(_middlewar_path);
    let prev_middlewares = app.get("middlewares") || {};

    if (scope) {
      try {
        if (scope == "core") {
          _middleware = require(_middlewar_path);
          if (autouse) {
            app.use("/app", _middleware);
            app.use("/admin", _middleware);
          }
        } else if (scope == "app") {
          _middleware = require.main.require(_middlewar_path);
          if (autouse) {
            app.use("/app", _middleware);
          }
        }
        let current_middleware = {
          [basename]: _middleware
        };
        app.set("middlewares", { ...prev_middlewares, ...current_middleware });
        return _middleware;
      } catch (error) {
        console.log("Error al cargar middleware: ", error);
      }
    } else {
      console.log("NO scope!");
    }
  }

  /**
   * Cargas los middlewares del Core y la aplicación.
   * @param {object} config
   * @param {string} base_path
   */
  function loadMiddlewares(config, base_path) {
    /**
     ** Cargar Middleweres del core
     */
    let scope = config.scope || "app";

    if (scope == "app") {
      var app_path_name = path.basename(global.APP_PATH);
      base_path = "./" + app_path_name + "/middlewares";
    }

    if ("middlewares" in config) {
      let core_middlewares = config["middlewares"];
      if (typeof core_middlewares === "string") {
        core_middlewares = core_middlewares.split(",");
      }
      if (Array.isArray(core_middlewares)) {
        core_middlewares.forEach((key_path, index) => {
          let _middlewares = core_middlewares[index];
          let _middlewar_path = `${base_path}/${key_path}`;

          if (typeof _middlewares === "object") {
            middlewares[key_path] = loadMiddleware(_middlewar_path, scope);
          } else {
            middlewares[key_path] = loadMiddleware(_middlewar_path, scope);
          }
        });
      } else if (typeof core_middlewares === "object") {
        Object.keys(core_middlewares).forEach(key_path => {
          console.log("\t > path: ", key_path);
          let _middlewares = core_middlewares[key_path];
          if (Array.isArray(_middlewares)) {
            _middlewares.forEach(item => {
              middlewares[key_path] = middlewares[key_path] || {};
              let _middlewar_path = `./core/middlewares/${item}`;
              try {
                middlewares[key_path][item] = loadMiddleware(
                  _middlewar_path,
                  scope,
                  false
                );

                app.use("/" + key_path, middlewares[key_path][item]);
              } catch (error) {
                console.log("Error al cargar middleware: ", item, error);
              }
            });
          }
        });
      }
    }
  }

  //Load Core middlewares
  loadMiddlewares(default_config, "./core/middlewares");

  //Create app middlewares directory.
  var app_path_middlewares = path.join(global.APP_PATH, "middlewares");
  Helper.createDirectoryAndFile(
    app_path_middlewares,
    Constants.FILENAME_README,
    Constants.TEXT_MIDDLEWARES
  ).then(() => {
    //Load Default app middlewares
    loadMiddlewares(config);
  });

  //console.log("Iniciando Aplicación");
  return new Promise(function(resolve, reject) {
    server.listen(app.get("port"), function() {
      /**
       * Restringir rutas de creación de usuarios, grupos y modulos
       * para el ámbito de aplicación.
       */
      if (!global.ALLOW_USER_REGISTRATION) {
        global.restrict_access += " POST /app/users POST /app/groups";
      }
      global.restrict_access += " POST /app/modules";

      /* app.all("*",(re,res) => {
				res.status(404)
				.send({
					msg:'not found',
					success:false
				});
			}); */

      //Cargar middleware que restringe las urls
      var restrict_access_middleware = require("./core/middlewares/restrict_access");
      app.use("*", restrict_access_middleware);

      //cargar middleware para validar parametros de entrada
      var jsonrequestmiddleware = require("./core/middlewares/jsonrequestmiddleware");
      app.use("*", jsonrequestmiddleware);

      /**
       * Cargar Router
       */
      router = require("./core/Router");
      Api = require("./core/Api");

      //Crear Instancia de Objeto Administrador de base de datos
      db = ManagerDB.createManagerDB();
      /**
       * Crear Instancia de Builder para iniciar Donec.
       */

      let builder = new Builder(app, db, io, router);
      builder.loadPlugins();

      app.set("builder", builder);

      app.use("*", function(req, res, next) {
        if (req.session) {
          let { user_id } = req.session;
          var token = req.headers["authorization"] || req.headers["token"];

          //#console.log("Validar Token: ", token, user_id);
          var allowAccess = req.originalUrl.includes("logout");
          if (allowAccess) return next();
          return next();
          //console.log("TPKEN::: ",token,(token==="undefined"));
          if (typeof token !== "undefined") {
            db.access.findOne(
              { /* user_id, */ token, active: true },
              (err, doc) => {
                if (!doc)
                  return res.status(401).send(
                    JSON.stringify({
                      success: false,
                      msg: "El token está vencido."
                    })
                  );
                return next();
              }
            );
          } else {
            return next();
          }
        }
      });

      /**
       * Conectar al servidor
       */
      db.connect().then(
        function(msg) {
          //iniciar utilidades de aplicación
          init();
          console.log(
            "Donec is running at http://localhost:" + app.get("port")
          );
          resolve(app, io);
        },
        function(err) {
          init();
          reject(err);
        }
      );
    });
  });
};

Donec.Plugin = function(pluginId, handler) {
  if (typeof pluginId === "function" && typeof handler === "undefined") {
    handler = pluginId;
    pluginId = "plugin-" + moment().format("YmdHms"); //pluginId
    console.log("pluginId: ", pluginId);
  }
  pluginId = pluginId.toLowerCase();
  plugins[pluginId] = {
    creator: handler,
    instance: null
  };
  return Donec.startPlugin(pluginId);
};

Donec.loadPlugin = pluginId => {
  return plugin_instance[pluginId.toLowerCase()];
};
Donec.Model = function(name, config, lang) {
  const self = this;
  this.name = name;
  this.config = config;
  this.lang = lang;
  let instance = {};
  let virtuals = {};

  const Builder = app.get("builder");
  const db = Builder.getDatabase();
  const schema_module = Builder.getModule("schema");

  //Define statics functions
  this.define = function(method_name, method) {
    /*  db.on(self.name,schema => {
      schema.statics[method_name] = method;
    }); */
    instance[method_name] = method;
    return self;
  };

  this.virtual = function(name) {
    return {
      get: function(callback) {
        db.on(self.name, schema => {
          schema.virtual(name).get(callback);
        });
        return self.virtual(name);
      },
      set: function(callback) {
        db.on(self.name, schema => {
          schema.virtual(name).set(callback);
        });
        return self.virtual(name);
      }
    };
  };
  db.on(`prev-${self.name}`, schema => {
    Object.keys(instance).forEach(name => {
      let method = instance[name];
      if (typeof method === "function") {
        schema.statics[name] = method;
        console.log("Hey Define este metodo!", name, instance);
      }
    });
  });
};
module.exports = Donec;
