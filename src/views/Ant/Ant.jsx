import React , { Component } from 'react';
import { withRouter } from 'react-router'
import Constants from '../../helper/Constants.jsx';
import 'antd/dist/antd.css'; 
import { Form, Icon, Input, Button,Calendar, Badge } from 'antd';
import { ard, CardBody, CardGroup, Col, Container, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

const FormItem = Form.Item;


function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
function onPanelChange(value, mode) {
  console.log(value, mode);
}
function getListData(value) {
  let listData;
  switch (value.date()) {
    case 8:
      listData = [
        { type: 'warning', content: 'This is warning event.' },
        { type: 'success', content: 'This is usual event.' },
      ]; break;
    case 10:
      listData = [
        { type: 'warning', content: 'This is warning event.' },
        { type: 'success', content: 'This is usual event.' },
        { type: 'error', content: 'This is error event.' },
      ]; break;
    case 15:
      listData = [
        { type: 'warning', content: 'This is warning event' },
        { type: 'success', content: 'This is very long usual event。。....' },
        { type: 'error', content: 'This is error event 1.' },
        { type: 'error', content: 'This is error event 2.' },
        { type: 'error', content: 'This is error event 3.' },
        { type: 'error', content: 'This is error event 4.' },
      ]; break;
    default:
  }
  return listData || [];
}

function dateCellRender(value) {
  const listData = getListData(value);
  return (
    <ul className="events">
      {
        listData.map(item => (
          <li key={item.content}>
            <Badge status={item.type} text={item.content} />
          </li>
        ))
      }
    </ul>
  );
}

function getMonthData(value) {
  if (value.month() === 8) {
    return 1394;
  }
}

function monthCellRender(value) {
  const num = getMonthData(value);
  return num ? (
    <div className="notes-month">
      <section>{num}</section>
      <span>Backlog number</span>
    </div>
  ) : null;
}
class Ant extends Component{
	constructor(props){
		super(props);
		this.fields = {
			name:""
		}
		this.state = {
			loading:false,
			errors:{},
			...this.fields
		}
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(e,{name, value}){
    	this.setState({
    		[name]:value
    	});
    }
    handleCancel(e){
    	e.preventDefault();
    	if(this.props.onCancel){
    		this.props.onCancel();
    	}
    }
    handleSubmit(e){
    	e.preventDefault();

    	
    	if(this.props.onSubmit){
    		this.props.onSubmit();
    	}
    }
	render(){
		const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
		// Only show error after a field is touched.
	    const userNameError = isFieldTouched('userName') && getFieldError('userName');
	    const passwordError = isFieldTouched('password') && getFieldError('password');

	    return (
	    	<div className="app flex-row align-items-center">
	    	  <Container>
	    	    <Row className="justify-content-center">
	    	      <Col md="12" sx="2">
	            	<div style={{background:"white"}}>
	        	      	<Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} />
	                </div>
	    	      </Col>
	    	    </Row>
	    	  </Container>
	    	</div>
	    	
	    );
	}
}
const WrappedHorizontalLoginForm = Form.create()(Ant);
export default withRouter(WrappedHorizontalLoginForm);