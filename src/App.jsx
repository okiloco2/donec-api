import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
	Login,
	Projects,
	Page404,
	Ant
} from './views/index.jsx';

import AppPanel from './components/AppPanel.jsx';
import EnsureLoggedInContainer from './components/EnsureLoggedInContainer.jsx';

import store from './store.jsx';
import axios from 'axios';
import { BrowserRouter as Router, HashRouter, Route, Switch } from 'react-router-dom';

class App  extends Component{
	constructor(props) {
	    super(props);
	    this.state = {
	    	
	    };
	    store.subscribe(() => {
	    	this.saveState(store.getState());
	    });

  	}
	saveState(state){
		try {
          let serializedState = JSON.stringify(state);
          localStorage.setItem("state", serializedState);
      }
      catch (err){
      }
	}
	render(){
	 	return(
	 		<HashRouter>
 		  		<Switch>
		 		    <Route path="/404" component={Page404} />
		 		    <Route exact path="/login" name="Login" component={Login} />
		 		    <Route exact path="/projects" name="Login" component={Projects} />
		 		    <Route path="/ant" name="AntDesign" component={Ant} />
		 		    <Route path="/" name="AppPanel" component={AppPanel} />
 		  		</Switch>
	 		</HashRouter>
		)
	}
}
export default App;