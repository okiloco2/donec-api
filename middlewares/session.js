module.exports = function(req, res, next) {
  var allowAccess = req.originalUrl.includes("login");

  if (!allowAccess) {
    var lvl_app = "";
    var admin_path = req.originalUrl.includes("/admin");

    if (admin_path) {
      if (req.session) {
        if (req.session.group)
          if (req.session.group !== "superadmin") {
            res.status(401).send(
              JSON.stringify({
                success: false,
                msg: "El usuario no tiene acceso como Super Admin."
              })
            );
            return;
          }
      } else {
        return res.status(401).send(
          JSON.stringify({
            success: false,
            msg: "No se pudo obtener datos de la sessión."
          })
        );
      }
    }
    console.log(
      "------------------------------ USER ID: ",
      global.session.user_id + " ------------------------------", req.session.user_id
    );
    if (!global.session.user_id) {
      res.status(401).send(
        JSON.stringify({
          success: false,
          msg: "No existe usuario en la sessión."
        })
      );
    } else {
      next();
    }
  } else {
    next();
  }
};
