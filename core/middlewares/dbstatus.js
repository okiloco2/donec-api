const mongoose = require("mongoose");
const Constants = require("../../helpers/Constants");
module.exports = function(req,res,next){
	var state = mongoose.connection.readyState;
	if(state === 0){
		res.status(200)
		.send(JSON.stringify({
			success:false,
			msg:Constants.TEXT_STATUS_DISCONNECTED
		}));
	}else{
		next();
	}
}