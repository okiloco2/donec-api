module.exports = function(sandbox) {
  const app = sandbox.app;
  const db = sandbox.db;

  return {
    init: function(schema) {
      schema.statics.listar = function(params) {
        return this.find(params).limit(10);
      };
      sandbox.get("/users/:_id", this.test);
      sandbox.post("/users", this.crear);
      sandbox.get("/users/:_id", this.test);
    },
    crear: function(req, res) {
      var params = req.body;
      db.user.listar(params).then((err, docs) => {
        if (err)
          return res.json({
            msg: err.message,
            success: false
          });
        return res.json({
          data: docs,
          success: true
        });
      });
    },
    test: function(req, res) {
      const { _id } = req.params;
      db.user.findById({ _id }, { password: 0 }, (err, data) => {
        if (err) {
          return res.json({
            success: false,
            msg: err.message
          });
        }
        return res.json({
          success: true,
          data
        });
      });
    },
    diHola: function(msg = "") {
      console.log("Hola", msg);
    }
  };
};
