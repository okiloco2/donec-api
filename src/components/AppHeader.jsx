import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import Avatar from 'material-ui/Avatar';
import FontIcon from 'material-ui/FontIcon';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import Badge from 'material-ui/Badge';
import ContentSend from 'material-ui/svg-icons/content/send';
import { withRouter } from 'react-router';
import Popover from 'material-ui/Popover/Popover';
import {Menu, MenuItem} from 'material-ui/Menu';
import {List, ListItem} from 'material-ui/List';

import {
   get_current_active_group
} from "../services/index.jsx";

import store from "../store.jsx";
import {
  onChangeActiveGroup
} from "../actionCreators.jsx";

import axios from 'axios';

const style = {color: "#000"};

class AppHeader  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	open: true,
        active_group:"",
        history:[],
	    	value:1
	    };
  		this.handleChange = this.handleChange.bind(this);
  		this.getAvatar = this.getAvatar.bind(this);
      this.getNotifications = this.getNotifications.bind(this);
      this.handleOpenMenu = this.handleOpenMenu.bind(this);
      this.handleRequestClose = this.handleRequestClose.bind(this);
      this.setAnchor = this.setAnchor.bind(this);
      this.setTarget = this.setTarget.bind(this);
  		this.signOut = this.signOut.bind(this);

      this.handleItemClick = this.handleItemClick.bind(this);
  	}
  	handleChange(event, index, value){
      console.log(event,index,value);
  		this.setState({value});
  	}
  	getAvatar(){
  		return this.state.profile || "public/resources/images/uxceo-128.jpg";
  	}
    handleOpenMenu(e){
      e.preventDefault();
      this.setState({
        openMenu: true,
        anchorEl: e.currentTarget,
      });
    }
    handleRequestClose(){
      this.setState({
        openMenu: false,
      });
    };
    setAnchor(positionElement, position){
      const {anchorOrigin} = this.state;
      anchorOrigin[positionElement] = position;

      this.setState({
        anchorOrigin: anchorOrigin,
      });
    };
    setTarget(positionElement, position){
      const {targetOrigin} = this.state;
      targetOrigin[positionElement] = position;

      this.setState({
        targetOrigin: targetOrigin,
      });
    };

  	getNotifications(){
  		if(this.props.count>0){
	  		return (<Badge
	  		        badgeContent={this.props.count}
	  		        secondary={true}
	  		        badgeStyle={{top: 12, right: 12}}
	  		      >
	  		        <NotificationsIcon />
	  		      </Badge>);
	  	}else{
	  		return (<NotificationsIcon />);
	  	}
  	}
    signOut(e){
      e.preventDefault();
      const { history } = this.props || [];

      //Eliminar LocalStorage
      window.localStorage.clear();

      delete axios.defaults.headers.common['Authorization'];
      delete axios.defaults.headers.common['token'];
      
      history.push("/login");
    }
    componentWillReceiveProps(nextProps) {
      // console.log(nextProps);
    }
    handleToggle(){
      this.setState({open: !this.state.open});
    }
    handleItemClick(item){
      if(this.props.handleItemClick){ 
        this.props.handleItemClick(item);
      }
    }
    handleItemTop = ()  => {
      console.log("Item Top!");
    }
    componentWillMount(){

      store.subscribe(() => {
        let state = store.getState();
        let {active_group} = state;
        if(active_group){
          this.setState({active_group});
        }
      });

       get_current_active_group()
       .then(response => {
          let {data} = response;
          let {active_group} = data;
          store.dispatch(onChangeActiveGroup(active_group));
          console.log("Response Header: ",data);
       })
       .catch(error => console.log("ERROR: ",error.message));
     }
  	render(){
  		return(
  			<Toolbar className={(this.props.className)?this.props.className:"AppToolbar"}>
  				
          <ToolbarGroup firstChild={true}>
  					{
              (this.props.items)?
              <DropDownMenu className="down-menu" value={this.state.active_group} onChange={this.handleChange}>
              <MenuItem value={this.state.active_group} style={style} primaryText={this.state.active_group} 
              onClick={() => {this.handleItemTop()}}/>
              {
                this.props.items.map((val,index) => {
                  return (
                    <MenuItem key={index} value={index} style={style} primaryText={val.text} onClick={() => {this.handleItemClick(val)}}/>
                  );
                })
              }
            </DropDownMenu>:<List className="card-header head-logo">
             <ListItem
               disabled={true}
               leftAvatar={
                 <Avatar src="public/resources/images/icon.png" />
               }
             >
             <div className="card-title"><h2>Donec Lab</h2></div>
             </ListItem>
          </List>
            }
  				</ToolbarGroup>
  				
          <ToolbarGroup>
  			  {
  			  	this.getNotifications()  			  		
  			  }
          <ToolbarTitle text="Options" />    
          <Avatar className="profile-picture" src={this.getAvatar()} onClick={this.handleOpenMenu}/>
            
          <Popover
            open={this.state.openMenu}
            anchorEl={this.state.anchorEl}
            anchorOrigin={this.state.anchorOrigin}
            targetOrigin={this.state.targetOrigin}
            onRequestClose={this.handleRequestClose}
          >
            <Menu className="app-menu">
              <MenuItem primaryText="Refresh" />
              <MenuItem primaryText="Help &amp; feedback" />
              <MenuItem primaryText="Settings" />
              <MenuItem primaryText="Sign out" onClick={this.signOut}/>
            </Menu>
          </Popover>

  			  
			  </ToolbarGroup>
  			</Toolbar>
		)
	}	
}
export default withRouter(AppHeader);