const Donec = require("../../Donec");
const Schema = {
    "token": {"type":"String","required": true},
    "active":{"type":"Boolean","default":true},
    "user_id": {
        "type":"ObjectId",
        "ref":"user"
    },
    "date_login": {
        "type": "Date",
        "default": "now"
    },
    "date_logout": {
        "type": "Date"
    }
}
const Model = new Donec.Model("access",Schema,"en");
module.exports = Model;