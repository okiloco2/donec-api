import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import Avatar from 'material-ui/Avatar';
import FontIcon from 'material-ui/FontIcon';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import IconButton from 'material-ui/IconButton';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import Badge from 'material-ui/Badge';
const style = {color: "#000"};

class AppBar  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	open: true,
	    	value:1
	    };
  		this.handleChange = this.handleChange.bind(this);
  	}
  	handleChange(event, index, value){
  		this.setState({value});
  	}
   
  	render(){
  		return(
  			<Toolbar className="AppToolbar">
  				<ToolbarGroup firstChild={true}>
  					<DropDownMenu className="down-menu" value={this.state.value} onChange={this.handleChange}>
               <MenuItem value={1} style={style} primaryText="Current Project" />
  			       <MenuItem value={1} style={style} primaryText="Ver todos los proyectos" />
  			       <MenuItem value={2} style={style} primaryText="Agregar un proyecto" />
  			     </DropDownMenu>
  				</ToolbarGroup>
  				<ToolbarGroup>
  			  <ToolbarTitle text="Notificaciones" />    
  			  <Badge
  			        badgeContent={10}
  			        secondary={true}
  			        badgeStyle={{top: 12, right: 12}}
  			      >
  			        <NotificationsIcon />
  			      </Badge>
  			  <Avatar src="public/resources/images/uxceo-128.jpg" />
  			  <ToolbarSeparator />
  				</ToolbarGroup>
  			</Toolbar>
		)
	}	
}
export default AppBar;