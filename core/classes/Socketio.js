"use strict";
var events = require("events"); //Events administra eventos
const url = require("url");
const Router = require("./Router");
const JwtValidator = require("./JwtValidator");
var express = require("express");
/* var Subscriber = require("./ClientManager"); */
//var Publisher = require("./ClientManager");
const SocketMiddleware = require("./SocketMiddleware");
var router = express.Router();
var { pluralize } = require("../../helpers/helper");

//var Sandbox = new events.EventEmitter();

class SocketIo extends events.EventEmitter {
  constructor(route_path, app, db, io) {
    super(arguments);
    this.validator = new JwtValidator();

    this.app = app;
    this.db = db;
    this.io = io;
    this.routes = [];
    this.route_path = route_path;

    /* this.client = new Subscriber(app, db, io);
    this.director = this.client.director;
    this.builder = this.client.builder; */

    //this.director.subscribe("connect");
    this.init();
  }
  init() {
    const self = this;
    let { db, io, app } = this;
    //this.director.subscribe("temp-reading:*");
    io.use((socket, next) => {
      //console.log(socket.request, socket.request.res);
      /* io.use(function (socket, next) {
        self.sessionMiddleware(socket.request, socket.request.res, next);
      }); */
      var namespace = socket.nsp.name;
      let server = socket.nsp.server;

      for (var s in server) {
        if (s === "nsps")
          for (var namespace in server[s]) {
            if (namespace !== "/") console.log("namespace: ", namespace);
          }
        //console.log(`\t>_${s}`);
      }

      /* var user_id = socket.request.session.user_id;
      var token = socket.request.session.token;

      */

      console.log("\tTodas las peticiones pasan por Socket!", namespace);

      return next();
      //VALIDAR MIDDLEWARES
      /* self.validator
        .validate(token)
        .then(() => {
          //console.log("Token is Valid!");
          return next();
        })
        .catch(err => {
          next(new Error(err));
        });

      if (user_id) return next();
      next(new Error("Authentication error")); */
    });
    /* Punto de conexion de DonecSocket */
    io.on("connection", function(socket) {
      console.log("Cliente conectado!");
      /* var user_id = socket.request.session.user_id; */
      socket.emit("connected", socket.id);

      /*<>>>>>>>>>> VALIDAR AUTENTICACION */
      /* if (user_id) {
        console.log("Usuario Logueado: userID:", user_id);
      } else {
        console.log("No se ha iniciado session.");
      } */
      //console.log("connected: ", count++);

      /* io.of("/connect").clients((error, clients) => {
        console.log("Clients: ", clients);
      }); */

      //self.director.init(socket);

      self.socket = socket;

      /* Routes */
      let routes = app.get("routes") || {};
      Object.keys(routes).forEach(key => {
        //console.log(key, /* routes[key] */);

        socket.on(`${key}`, async (params, fn) => {
          //console.log("Params: ", params);
          try {
            let middleware = new SocketMiddleware(key, params, routes[key]);
            middleware.on("error", error => {
              console.log("El emit wuapea!!");
              io.emit("error", error);
            });
            middleware.on(key, response => {
              if (socket.request.session) fn(response);
            });
            await middleware.checkNameSpace(key, params);
            middleware.init();
          } catch (error) {}
        });
      });
      /* routes = routes.filter((item, index) => {
        let key = Object.keys(item)[0];
        let method_key = Object.keys(item[key])[0];
        let method = item[key][method_key];
        let path = `${key}_${method_key}`;
        let findex = path_routes.indexOf(path);
        if (findex === -1) {
          path_routes.push(path);
          return true;
        } else {
          routes[findex] = item;
          return false;
        }
      }); */
      //console.log(app.get("routes"));
      //console.log("Routes: ", routes);

      socket.on("ferret", function(name, fn) {
        fn(name + " says hello!");
      });
      /* 
      DECLARAR ENDPOINTS
      routes.forEach(route => {
        //self.register(route);
        let key = Object.keys(route)[0];
        let method_key = Object.keys(route[key])[0];
        let method = route[key][method_key];
        let path = `${method_key} ${key}`;
        socket.on(path, function(params, fn) {
          console.log("Params: ", params);
          let middleware = new SocketMiddleware({
            method_key,
            method,
            path,
            params
          });
          return middleware.on(path, response => {
            console.log("FN: ", response);
            fn(response);
          });
        }); */

      /* let match = path.match(/(\:(\w+))/gi);
			   if (match)
				 match = match.map(item => {
				   return item.replace(/:/, "");
				 });
			   console.log(match); */
      //});
      self.socket.on("onsync", function(name, fn) {
        //console.log("OnSync!!!", self.director.socket);
        if (typeof fn !== "undefined") {
          fn(name + " WooW!");
        }
      });

      //self.socket.emit("prueba",{"msg":"Hola desde el server."});
    });
    db.on("define", (name, model) => {
      var schema = model.getSchema();
      var lang = schema.lang;
      var alias = pluralize(lang || "es", name);
      var pathname = "/" + alias;
      //console.log(route_path,": ",pathname,name);
      var route = { name: name, path: pathname, alias: alias };

      self.routes.push(route);
      self.register(route);
    });

    //console.log(">>>>>>|",self.routes);
    db.on("ready", () => {
      /* Middlewares */
      let middlewares = app.get("middlewares");
      console.log("Middlewares: ", middlewares);
    });
  }
  register(route) {
    var self = this;
    if (self.socket) {
      //console.log("Socket: ", route);

      if (this.db[route.name]) {
        //console.log("Route: ",route);
        this.socket.on("get:" + route.name, function(params) {
          self.list(route.name, params);
        });
      }
    }
  }

  list(name, params) {
    var self = this;
    console.log("Params: ", params);

    console.log("GET: List " + name);
    this.db[name].search(params, function(err, docs) {
      //res.send(JSON.stringify({data:docs}));
      self.socket.emit("get:" + name, docs);
    });
  }
}
module.exports = SocketIo;
