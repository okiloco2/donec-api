var jwt = require("jsonwebtoken");

const validateToken = (token,lvl_access) => {
	return new Promise((resolve,reject) => {

		jwt.verify(token, process.env.SECRET_KEY, function(err,decode){
			if(decode && lvl_access == "/admin"){
				//console.log("decoded: ",decode);
				if(decode.group !== "superadmin"){
					reject("El usuario no tiene acceso como Super Admin.")
					return;
				}
			}			
			let msg = "";
			if(err){
			   	switch(err.name){	
			   		case "TokenExpiredError":
							  msg = ">>>La sesión ha expirado." + token;
			   		break;
					   default:
			   			msg = err.message;
			   		break;
			   	}	
		  		reject(msg);
			}else{
				resolve();
			}
		});
	});
}
const jwtmiddleware = (req,res,next) => {
	
	var auth = req.get("token");
	var access = req.originalUrl.includes("login");
	var lvl_access = "";
	var route_path = req.originalUrl.includes("/app");
	
	

	if(!route_path){

		route_path = req.originalUrl.includes("/admin");
		if(route_path){
			lvl_access = "/admin";
		}
	}else{
		lvl_access = "/app";
	}

	if(lvl_access == "/admin"){
		auth = req.get("authorization");
	}
	


	if(!access){
		
		validateToken(auth,lvl_access)
		.then(() => {
			next();
		}, error => {
			//console.log("VAlidar JWT.",error	)
			res.status(401)
			.json({success:false,msg:error});
		});
	}else{
		next();
	}
}

module.exports = jwtmiddleware;