const Donec = require("../../Donec");
const ModuleSchema = {
    "name": {"type":"String","unique":true,"required": true},
    "config": "String",
    "estado": {
        "type": "Boolean",
        "default": true
    }
}
const ModuleModel = new Donec.Model("module",ModuleSchema,"en");
module.exports = ModuleModel;