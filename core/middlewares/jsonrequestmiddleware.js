module.exports = (err,req,res,next)=>{
    if(err){
        if (res.headersSent) {
            return next(err);
        }
        res.status(500);
        return res.json({
            success:false,
            msg:"Error en parámetros de entrada: "+err.message
        });
        return;
    } 

}