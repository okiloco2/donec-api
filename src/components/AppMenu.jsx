import React, { Component } from 'react';
import {List, ListItem} from 'material-ui/List';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import ContentDatabase from 'material-ui/svg-icons/action/settings-backup-restore';
import FontIcon from 'material-ui/FontIcon';
import Divider from 'material-ui/Divider';
import ActionInfo from 'material-ui/svg-icons/action/info';
import {blue500, red500, greenA200} from 'material-ui/styles/colors';
import { Link } from "react-router-dom";
import { withRouter } from 'react-router';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import store from '../store.jsx';
import { onItemTap, onSaveCollection } from '../actionCreators.jsx';
import Helper from "../helper/Helper.jsx"

var FontAwesome = require('react-fontawesome');
const iconStyles = {
  marginRight: 24,
};
class AppMenu  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {open: true};
	    this.onItemTab = this.onItemTab.bind(this);
  	}
  	onItemTab(item){
  		const { match, history } = this.props;
  		if(history.location.pathname.toLowerCase()!=item.path.toLowerCase()){
        store.dispatch(onItemTap(item));
        history.replace("/");
        history.push(item.name.toLowerCase());
  		}
  	}
    componentWillMount(){
      store.subscribe(() => {
        const _state = store.getState();
        if(_state.routes){
          let data = Helper.uniqArray(_state.routes,"link");

         //console.log("Routes: ",Helper.sortArray(data,"order"));
        }
      });
    }
  	render(){
  		var me = this;
  		return(
  			<div className="menu" ref="menu">
  				<List className="card-header">
  				   <ListItem
  				   	 disabled={true}
  				     leftAvatar={
  				       <Avatar src="public/resources/images/icon.png" />
  				     }
  				   >
  				   <div className="card-title"><h2>Donec Lab</h2></div>
  				   </ListItem>
  				</List>
  				
  				<List className="project-overview">
			      <ListItem disabled={true} primaryText={<span className="project-overview-title">Project Overview</span>} leftIcon={<FontIcon className="material-icons">home</FontIcon>} rightIcon={<FontIcon className="material-icons">settings</FontIcon>}/>
			    </List>
			    <Divider />
			    <List>
			    { this.props.routes.filter(val=>{return val.menu;}).map(function(val,index){
			      	return (<ListItem key={index} primaryText={val.name} onClick={(e) => {
			      		e.preventDefault();	
		      			me.onItemTab(val);
			      	}} leftIcon={<FontIcon className="material-icons">{val.icon}</FontIcon>}/>);
				   })
				}
			    </List>
  			</div>
		)
  	}
}
export default withRouter(AppMenu);


