const onItemTap = item => {
	return {
      type:"CHANGE_PATH",
      name:item.name,
      path:item.path.toLowerCase()
    };
}

const onSaveCollection = item => {
  return {
      type:"SAVE_COLLECTION",
      collection:{
        _id:item._id,
        "state":item.state,
        "status":item.status,
        name:item.name,
        lang:item.lang,
        config:item.config
      }
    };
}

const getCollections = collections => {
  return {
      type:"GET_COLLECTIONS",
      collections:collections
    };
}

const onGetCollection = _id => {
  return {
      type:"GET_COLLECTION",
      collection:{
        _id
      }
    };
}
const onCodeError = code_error => {
  return {
      type:"CODE_ERROR",
      code_error
    };
}
const onMessage = message => {
  return {
      type:"ON_MESSAGE",
      message
    };
}

const onlinkRoutes = links => {
  return {
      type:"ON_LINK_ROUTE",
      links
    };
}
const onChangeRoute = (routes, forward = true) => {
  return {
      type:"ON_CHANGE_ROUTE",
      routes,
      forward
    };
}
const onChangeActiveGroup = (active_group) => {
	return {
      type:"ON_CHANGE_ACTIVE_GROUP",
      active_group
    };
}

export { onItemTap, onSaveCollection, onGetCollection, onCodeError,onMessage,getCollections, onlinkRoutes,onChangeRoute,onChangeActiveGroup };