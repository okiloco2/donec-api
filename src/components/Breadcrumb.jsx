import React, { Component } from 'react';
import store from '../store.jsx'
import { onChangeRoute} from  "../actionCreators.jsx";
import { withRouter } from 'react-router';
class Breadcrumb  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
          routes:[{text:"Collections",link:"/"}]
	    };
  	}
  	onLinkClick(item){
  	  const { history, location } = this.props || [];
      let {routes} = this.state;
  	  let forward = true;
  	  
      if(item.link!="#" || item.link!=undefined){
  	    
        if(this.state.view==="doc"){
          forward = false;
  	      this.setState({
  	        view:"list"
  	      });
  	    }
        /*if(item.order < (routes.length - 1)){
           forward = false;
        }*/
        /*if(!item.order){
          forward = true;
        }else{
          if(item.order < routes.length){
            forward = false;
          }
        }*/
        console.log(item.link);
        store.dispatch(onChangeRoute(item,forward));
        
  	    history.replace({
  	      pathname: item.link
  	    });
  	  }
  	}
    componentWillMount(){

      store.subscribe(() => {
         let {routes} = store.getState();
         if(typeof routes !== "undefined"){
           this.setState({
              routes
           });
         }
      });
    }
  	render(){
  		return(
	        <div className="link-collection">
	          <i className="material-icons icon-home">home</i> 
	          {
	            this.state.routes.map((val,index,arr) => {
	              return <span key={index}><a onClick={(e) => {this.onLinkClick(val)}}>{val.text}</a>{(arr.length>1 && index<(arr.length-1))?<i className="material-icons">keyboard_arrow_right</i>:(null)}</span>        
	            })
	          }
	        </div>
        );
  	}

}
export default withRouter(Breadcrumb);