const Donec = require("../../Donec");
const UserSchema = {
    "email": {"type":"String","unique":true,"required": true},
    "date": {"type":"Date",default:"now"},
    "password": "String",
    "usergroup": {
        "type": "ObjectId",
        "ref": "group",
        "required": true
    }
}
const UserModel = new Donec.Model("user",UserSchema,"en");
module.exports = UserModel;