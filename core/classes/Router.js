"use strict";
var express = require("express");
var router = express.Router();
var { pluralize } = require("../helpers/helper");
var fs = require("fs");
var path = require("path");
var Helper = require("../helpers/helper");
var Donec = require("../../Donec.js");

class Router {
  constructor(route_path, app, db, io) {
    if (!app) throw new Error("Missing required App");
    let self = this;

    this.app = app;
    this.db = db;
    this.io = io;
    this.routes = [];
    this.route_path = route_path;
  }
  readFiles(base_path, subfolder) {
    return new Promise(function(resolve, reject) {
      try {
        var paths = [];
        fs.readdirSync(base_path).reduce((paths, filename, index, arr) => {
          var total = arr.length - 1;
          var file = "";
          var file_path = "";
          var ext = path.extname(filename);

          file_path = base_path;

          /* --- Si es un archivo ---*/
          var handle_route;
          if (ext != null && ext == ".js") {
            var controllerName = Helper.capitalize(name);

            file = path.join(file_path, filename);
            var name = path.parse(file).name.toLowerCase();

            if (!subfolder) {
              //console.log("Router public define custom: ",name,filename);
              handle_route = require(file);
              paths.push("/" + name);
              //Donec.define(name,handle_route,true);
              Donec.define(name, {
                path: "/" + name,
                init: handle_route
              });
            } else {
              /*Listener para cuando se cree el Schema con el nombre especifico.*/
              db.on(name, function(schema) {
                try {
                  handle_route = require(file);
                  var pathname;
                  var schema;
                  pathname = pluralize(schema.lang || "es", name);

                  if (typeof subfolder == "undefined") subfolder = "";
                  pathname = subfolder + "/" + pathname;
                  /*
                   * Instanciar ruta: privada:
                   * La aplicación empieza a usar el nuevo middlewares route.
                   */
                  //console.log("onDefined",name);
                  console.log("pathname: ", pathname);
                  Donec.define(name, {
                    path: pathname,
                    schema: schema,
                    init: handle_route
                  });
                  // Donec.register({"name":name, "path": pathname,"schema":schema},route);
                  // Donec.start(name);
                  //app.use(pathname,route(app,global.router,db,schema));
                  //#app.use(pathname,route(app,io,global.router,db,schema));
                } catch (err) {
                  console.log(new Error(err.message));
                  reject(err);
                }
              });
            }
            routes_count++;
            //console.log("- File "+routes_count+":",file);
          } else {
            file_path = path.join(file_path, filename);
            //console.log("> Folder",filename,file_path);
            let name = filename.toLowerCase();
            readFiles(file_path, name);
          }
          return paths;
        }, paths);
        resolve();
      } catch (error) {
        console.log("ERROR: ", error.message);
        reject(error);
        return;
      }
    });
  }
  register(route) {
    //console.log("\tRoute: ",route);
    if (this.db[route.name]) {
      router
        .route(route.path)
        .get(this.list.bind(this.db[route.name]))
        .post(this.save.bind(this.db[route.name]))
        .put(this.update.bind(this.db[route.name]));

      router
        .route(route.path + "/:_id")
        .get(this.findById.bind(this.db[route.name]))
        .put(this.update.bind(this.db[route.name]))
        .delete(this.remove.bind(this.db[route.name]));
    }
  }
  get services() {
    return this.services_routes;
  }
  list(req, res, next) {
    var params = req.query || {};
    console.log("GET: List " + this.modelName);
    this.search(params, function(err, docs) {
      return res.send(JSON.stringify({ data: docs }));
    });
  }
  findById(req, res, next) {
    var params = req.params;
    console.log("GET: findById " + this.modelName);
    this.search(params, function(err, docs) {
      return res.send(JSON.stringify({ success: true, data: docs }));
    });
  }
  save(req, res, next) {
    var params = req.body;
    var modelName = this.modelName;
    this.create(params, function(err, doc) {
      console.log(
        ">> POST: save " + modelName + "(" + (err ? "Fail" : "succefully") + ")"
      );
      if (err) {
        return res.send(
          JSON.stringify({
            success: false,
            msg: err.message || err._message || err
          })
        );
        return;
      }
      if (!doc) {
        return res.send(
          JSON.stringify({
            success: false,
            msg: err.errmsg || err._message || err
          })
        );
      } else {
        return res.send(
          JSON.stringify({
            success: true,
            _id: !params._id ? doc._id : undefined,
            msg: !params._id
              ? "Registro creado con éxito."
              : "Registro actualizado con éxito."
          })
        );
      }
    });
  }
  update(req, res, next) {
    var params = req.body;
    params["_id"] = req.params._id;
    var modelName = this.modelName;
    this.create(params, function(err, doc) {
      console.log(
        "PUT: update " + modelName + "(" + (err ? "Fail" : "succefully") + ")"
      );
      if (err) {
        return res.send(
          JSON.stringify({
            success: false,
            msg: err.message || err._message || err
          })
        );
        return;
      }
      if (!doc) {
        return res.send(JSON.stringify({ success: false, msg: err }));
      } else {
        return res.send(
          JSON.stringify({
            success: true,
            _id: !params._id ? doc._id : undefined,
            msg: !params._id
              ? "Registro creado con éxito."
              : "Registro actualizado con éxito."
          })
        );
      }
    });
  }
  remove(req, res, next) {
    var params = req.params;
    var modelName = this.modelName;
    this.removeById(params._id, function(msg, doc) {
      console.log(
        "DELETE: remove " +
          modelName +
          "(" +
          (msg ? "Fail" : "succefully") +
          ")"
      );
      return res.send(
        JSON.stringify({
          success: true,
          msg: msg
        })
      );
    });
  }
  get(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).get(callback);
  }
  post(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).post(callback);
  }
  put(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).put(callback);
  }
  delete(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).delete(callback);
  }
}
module.exports = Router;
