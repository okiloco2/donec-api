var Sandbox = function(config){
	var debug = true;
	return {
		log:(debug)?console.log:function(){},
		model:config,
		URL_BASE:"",
		KEY_ACCESS:"",
		http:function(url,params,callback){
			
			if(typeof params === 'function' || typeof url === 'object'){
				callback = params;
				params = url;
			}
			var xhttp = new XMLHttpRequest(),
	        method = params.method || 'GET';
	        url = params.url || url;
		    
		    
		    var formData = null;
		    if(params.params){
		    	// formData = new FormData();
		    	if(method==='GET'){
		    		url = url+this.generateGET(params.params);
		    	}else if(method==='POST' || method==='PUT'){
			    	formData = {};
			    	for(var s in params.params){
			    		formData[s]=params.params[s];
			    	}
			    	formData = JSON.stringify(formData);
		    	}
		    }

		    xhttp.open(method, url, true);
		    if(params.headers){
		    	for(var key in params.headers){
		    		xhttp.setRequestHeader(key, params.headers[key]);
		    	}
		    }

		    xhttp.onload = function () {
		    	var responseText = xhttp.responseText;
		    	if (xhttp.readyState === 4 && xhttp.status === 200) {
		    		callback({
                		"ok":true,
                		"responseText":responseText
                	}); // Another callback here
		    	}else {
		    		callback({
                		"ok":false,
                		"responseText":responseText
                	}); // Another callback here
		    	}
		    }
		    xhttp.send(formData);
		},
		generateGET:function(options) {
		  // Create string for GET requests in a url
		  var args = '?';

		  var i = 1;
		  for (var key in options) {
			args += key + '=' + options[key];

			// Append & separator to all but last value
			if (i !== Object.keys(options).length)
			  args += '&';

			i++;
		  }

		  return args;
		}
	}
};

function builder(instance){
	var name,method;
	for(var s in instance){
		method = instance[s];
		if(typeof method === 'function'){
			instance[s] = function(s,method){
				return function(){
					try{
						return method.apply(this,arguments);
					}catch(err){
						console.log("error ",s,+'()'+err.message);
					
					}
				}
			}(name,method);
		}
	}
	return instance;
}
const ManagerDB = function(schemas){

	var models = {};

	return {
		init:function(){
			
			console.log(models);
		}
	}
} 
var Donec = function(){

	var $s = new Sandbox();
	var components = {};
	//#Crear Instancia 
	function createInstance(type,objectId,config){
		var creator = components[type][objectId].creator;
		components[type][objectId]["total"] = components[type][objectId]["total"] || 0;
		// var instance = 	creator.init(new Sandbox(this),config);
		var instance = 	creator.init($s,config);
		//Add default attributes
		instance["id"] = creator.id || objectId+'-'+(components[type][objectId]["total"]++);
		instance.destroy = function(){
			$s.log("Destroy instance. ",instance["id"]);
		}
		return builder(instance);
	}
	return {
		define:function(objectId,config){
			var type = config.type || "component";
			if(typeof config === 'object'){
				components[type] = components[type] || {};
				components[type][objectId] = {
					creator:config,
					instances:[]
				};
			}
		},
		create:function(objectId,config){
			var instance = {};
			for(var type in components){
				if(objectId in components[type]){
					components[type] = components[type] || {};
					components[type][objectId] = components[type][objectId] || [];
					instance = createInstance(type,objectId,config);
					components[type][objectId].instances.push(instance);
				}
			}
			return instance;
		},
		destroy:function(objectId){
			var module = this.getModule(objectId);
			$s.log(module);
			for(var i =0;i<module.instances.length;++i){
				if(module.instances[i]){
					module.instances[i].destroy();
					delete module.instances[i];
				}
			}
			module.instances =[];
			module.total = 0;
		},
		destroyAll:function(){
			for(var objectId in components){
				for(var moduleId in components[objectId]){
					this.destroy(moduleId);
				}
			}
		},
		getModules:function(){
			return components["module"];
		},
		getModule:function(moduleId){
			return components["module"][moduleId];
		},
		get:function(objectId){
			$s.log("findById: ",objectId)
			var instance;
			for(var type in components){
				for(var component in components[type]){
					var instances = components[type][component].instances;
					for(var i =0;i<instances.length;++i){
						if(objectId === instances[i].id){
							$s.log("Encontrado: ",objectId);
							instance = instances[i];
						}
					}
				}
			}
			return instance; 
		},
		getAll:function(){
			return components;
		},
		connect:function(config,callback){
			var me = this;
			$s.URL_BASE = config.url.replace("/api/connect","");
			$s.KEY_ACCESS = config.key_access;
			$s.http(config.url,{
				method:'POST',
				headers:{
					"Content-Type":"application/json",
					"Authorization":config.key_access
				}
			},function(response){
				var responseObject = JSON.parse(response.responseText);
				if(response.ok){
					console.log("Connect to: ",$s.URL_BASE);
					me.init(responseObject.data,callback);
					if(callback!=undefined){
						
					}
				}else{
					$s.error("Error "+responseObject.msg);					
				}				
			});
		}, 
		init:function(schemas,callback){
			var me = this;
			me.db = new ManagerDB(schemas);
			var models = {};
			for(var i=0;i<schemas.length;++i){
				var schema = schemas[i];
				models[schema.name] = models[schema.name] || {};
				models[schema.name]["schema"] = JSON.parse(schema.config);
				models[schema.name]["lang"] = schema.lang;
				models[schema.name]["alias"] = schema.alias;
				models[schema.name]["slug"] = schema.slug;

				me.define(schema.name,{
					type:"model",
					name:schema.name,
					schema:models[schema.name]["schema"],
					slug:schema.slug,
					alias:schema.alias,
					lang:schema.lang,
					init:function($s,data){
					  return {
					  	"name":this.name,
					  	"save":this.save,
					  	"find":this.find,
					  	"get":this.get,
					  	"set":this.set,
					  	"schema":this.schema,
					  	"slug":schema.slug,
					  	"alias":schema.alias,
					  	"model":this.getFieldValues(data)
					  }
					},
					save:function(callback){
						$s.http($s.URL_BASE+""+this.slug,{
							method:'POST',
							headers:{
								"Content-Type":"application/json",
								"token":$s.KEY_ACCESS
							},
							params:this.model
						},function(response){
							var responseObject = JSON.parse(response.responseText);
							if(callback!==undefined){
								callback(responseObject.success,responseObject.msg);
							}
							if(response.ok){
								$s.log(responseObject.msg);
							}else{
								$s.error("Error "+responseObject.msg);					
							}			
						});
					},
					update:function(callback){
						var url = $s.URL_BASE+""+this.slug;
						if(!this.model._id){
							$s.log("missing _id parameter.");
							return;
						}
						url = url+"/"+this.model._id;

						$s.http(url,{
							method:'PUT',
							headers:{
								"Content-Type":"application/json",
								"token":$s.KEY_ACCESS
							},
							params:this.model
						},function(response){
							var responseObject = JSON.parse(response.responseText);
							if(callback!==undefined){
								callback(responseObject.success,responseObject.msg);
							}
							if(response.ok){
								$s.log(responseObject.msg);
							}else{
								$s.error("Error "+responseObject.msg);					
							}			
						});
					},
					find:function(params,callback){
						$s.http($s.URL_BASE+""+this.slug,{
							method:'GET',
							headers:{
								"Content-Type":"application/json",
								"token":$s.KEY_ACCESS
							},
							params:params
						},function(response){
							var responseObject = JSON.parse(response.responseText);
							if(response.ok){
								if(callback!==undefined){
									callback(responseObject.success,responseObject.data);
								}
							}else{
							  $s.error("Error "+responseObject.msg);			
							}
						});
					},
					isValid:function(field,data){
						if(field){
							if(typeof field === 'object'){

							}else{
								// $s.log("field: ",field);
								var schema = this.schema[field];

								if("required" in schema && schema["required"]==="true"){
									if("type" in schema && schema["type"] === 'ObjectId'){
										return (typeof data === 'object');
									}
									return (data!==null && data !==undefined);
								}
							}
						}
					},
					validate:function(field,data,callback){
						var me =this, err = null,
						schema = (typeof field === 'object')?field:this.schema[field];						
						if(!err && "required" in schema && schema["required"]==="true"){
							err = !(data!==null && data!==undefined)?(field+" is required."):err;
							if(!err && "type" in this.schema[field] && this.schema[field]["type"] === 'ObjectId'){
								err = !(typeof data === 'object')?field+" is no object ":err;
							}
						}
						if(callback!==undefined){callback(err);return;}
						if(err){
							throw new Error("validation "+me.name+": "+err);
						}
					},
					getFieldValues:function(data){
						var fields = {};

						for(var s in this.schema){
							// $s.log(s,":",this.schema[s]);
							if(typeof this.schema[s] === 'object'){

								if(!("ref" in this.schema[s])){
									// $s.log("No es llave foranea.")
								}else{
									// $s.log("Es llave foranea.")
								}
								this.validate(s,data[s]);
							}
							if(s in data){
								fields[s] = data[s];//asignar valor
							}
						}
						return fields;
					},
					get:function(name){
						return this.model[name];
					},
					set:function(key,value){
						if(typeof key === 'object'){
							$s.log("old value ",this.model,", new value ",key);
							for(var s in this.schema){
								if(s in key){
									this.model[s] = key[s];
								}
							}
							return this.model;
						}
						$s.log("old value ",this.model[key],", new value ",value);
						this.model[key] = value;
						return this.model;
					}
				});

				// me.create(schema.name,JSON.parse(schema.config));
			}

			
			//me.dispatchEvent(new Event('onReady'));
			if(callback!==undefined){
				callback(models);
			}

		}
	}
}();

exports.Donec = Donec;