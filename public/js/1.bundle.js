webpackJsonp([1],{

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _index = __webpack_require__(130);

var routes = [{ path: '/', exact: true, name: 'Panel', component: _index.Panel, icon: "home", menu: false }, { path: '/dashboard', exact: true, name: 'Dashboard', component: _index.Dashboard, icon: "dashboard", menu: true }, { path: '/database', exact: true, name: 'Database', component: _index.Database, icon: "inbox", menu: true }, { path: '/collection/:alias', exact: true, name: 'Collection', component: _index.Collection, icon: "inbox", menu: false }];
exports.default = routes;

/***/ })

});