var jwt = require("jsonwebtoken");
module.exports = function(req,res,next){
	var access = req.originalUrl.includes("get_key_access");
	var auth = req.get("authorization");
	if(!access){
		jwt.verify(auth,process.env.SECRET_KEY,function(err,decode){
			if(err){
			  // console.log(err.message);	
			   //res.set("WWW-Authenticate", "Basic realm=\"Authorization Required\"");
			   return res.status(401)
			   .send(JSON.stringify({success:false,msg:err.message}));
			}else{
				next();
			}
		});
	}else{
  		next();
	}
}