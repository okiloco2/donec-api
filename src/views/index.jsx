import Dashboard from './Dashboard/Dashboard.jsx';
import Database from './Database/Database.jsx';
import Collection from './Database/Collection.jsx';
import Projects from './Projects/index.jsx';
import Page404 from './Pages/404/Page404.jsx';
import Login from './Pages/Login/Login.jsx';
import Ant from './Ant/Ant.jsx';

export {
  Dashboard,
  Database,
  Collection,
  Projects,
  Login,
  Page404,
  Ant
}