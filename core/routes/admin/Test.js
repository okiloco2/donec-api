module.exports = function(sandbox){
	const app = sandbox.app;
	const db = sandbox.db;

	return {
		init:function(schema){
			app.get("/admin/test",this.test);
		},
		test:function(req,res){
			res.json({
				success:true,
				msg:"Hola!"
			});			
		}
	}
}

