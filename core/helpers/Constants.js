const config = require("../../package.json");
let {repository} = config;

module.exports = {
URL_BASE:'http://localhost:8443',
FILENAME_README:"README.md",
TEXT_INDEX:`#INDEX 
	
This directory contains your index.html

More information about the usage of this directory in [the documentation] ${repository}`,
TEXT_README:`#ROUTES 
	
This directory contains your Application Routers

More information about the usage of this directory in [the documentation] ${repository}blob/master/README.md#rutas-personalizadas`,
TEXT_README_SERVICES:`#SERVICES
	
This directory contains your Application Services

More information about the usage of this directory in [the documentation] ${repository}`,
TEXT_README_PLUGINS:`#PLUGINS 
	
This directory contains your Application Plugins

More information about the usage of this directory in [the documentation] ${repository}`,

TEXT_MODELS:`#MODELS 
	
This directory contains your Application Models

More information about the usage of this directory in [the documentation] ${repository}blob/master/README.md#models`,

TEXT_MIDDLEWARES:`#MIDDLEWARES 
	
This directory contains your Application Middlewares

More information about the usage of this directory in [the documentation] ${repository}`,

HTML_CONTENT:`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Droid+Sans" />
    <style>
        body{
            margin: 0px;
        }
        h1 { font-family: "Droid Sans"; font-size: 24px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 26.4px; } h3 { font-family: "Droid Sans"; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 15.4px; } p { font-family: "Droid Sans"; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; } blockquote { font-family: "Droid Sans"; font-size: 21px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 30px; } pre { font-family: "Droid Sans"; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 18.5714px; }
        .container{
            background-color:#fedb9d30;
            height: 100%;
            width: 100%;
            position: absolute;
        }
        .DonecLogo{
            display: flex;
            flex-flow: column;
            align-items: center;
            justify-content: center;
            text-align: center;
            padding: 30px 8px 8px 8px;
        }
        .links{
            text-align: center;
            padding: 5px;
        }
        .button{
            text-decoration: none;
            color: #000;
            line-height: 14px;
            font-weight: 600;
            padding: 5px; 
            border: 2px solid #000;
            background-color: #fff;
            border-radius: 8px;
            font-family: "Droid Sans";
            margin-left: 10px;
            margin-bottom: .5rem;
            padding: 3px 8px;
            padding-top: 5px;
        }
        .button-possessive{
            -webkit-box-shadow: 2px 2px 0 #222;
            box-shadow: 2px 2px 0 #222;
        }
        .button-possessive:hover{
            -webkit-box-shadow: 1px 1px 0 #222;
            box-shadow: 1px 1px 0 #222;
        }
        .button-green{
            background-color: #83ffcd!important;
        }
        .button-grey{
            background-color: #fff!important;
        }
        .button-grey:hover{
            background-color:#f5f5f5!important;
        }
        .inner {
            width: 400px;
            height: 400px;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-top: -200px;
            margin-left: -200px;
        }
        .animated {
            -webkit-animation-duration: 2.5s;
            animation-duration: 2.5s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
            -webkit-animation-timing-function: linear;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            -webkit-animation-iteration-count: infinite;
        } 

        @-webkit-keyframes bounce {
            0%, 20%, 40%, 60%, 80%, 100% {-webkit-transform: translateY(0);}
            50% {-webkit-transform: translateY(-5px);}
        } 

        @keyframes bounce { 
            0%, 20%, 40%, 60%, 80%, 100% {transform: translateY(0);}
            50% {transform: translateY(-5px);}
        } 

        .bounce { 
            -webkit-animation-name: bounce;
            animation-name: bounce;
        }
    </style>
    <title>.:: Donec API</title>
</head>
<body>
    <div class="container">
        <div class="inner">

            <div class="DonecLogo">
                <img class="animated bounce" src="public/DonecLogo.svg" width="126"  height="126"/>
                <h1>Welcome to Donec API.</h1>
            </div>
            <div class="links">
                <a href="${repository}blob/master/README.md#donec-api" class="button button-green button-possessive">View Source</a> 
                <a href="${repository}blob/master/README.md#about" class="button button-grey">About us</a> 
            </div>
        </div>
    </div>
</body>
</html>`
}