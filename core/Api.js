var schedule = require("node-schedule");
var express = require("express");
var router = express.Router();
var jwt = require("jsonwebtoken");
var md5 = require("md5");
module.exports = function(Donec, app, db, io) {
  router.route("/app").get(function(req, res) {
    return res.json({
      msg: "Hola mundo"
    });
  });

  router.route("/connect").post(function(req, res) {
    db.schema.listar({}, function(err, docs) {
      console.log(req.body);

      res.status(200).send(
        JSON.stringify({
          success: true,
          msg: "Conectado.",
          data: docs
        })
      );
    });
  });

  router.route("/get_key_access").post(function(req, res) {
    var params = req.body;
    if (params.password != undefined) {
      params.password = md5(params.password);
    }
    db.user
      .findOne({ email: params.email, password: params.password })
      .then(function(user) {
        if (user) {
          var token = jwt.sign(params, process.env.SECRET_KEY, {
            expiresIn: 0
          });
          return res.json({
            success: true,
            msg: "Usuario autorizado.",
            token: token
          });
        } else {
          return res.json({
            success: false,
            msg: "Usuario no autorizado."
          });
        }
      });
  });

  //Router Validación de token jwt
  return router;
};
