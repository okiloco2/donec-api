/**
* @author: Fabian Vargas Fontalvo
* email: f_varga@hotmail.com
*/
'use strict'
module.exports.createService = function(instance){
	for(var key in instance){
		var method = instance[key];
		if(typeof method == 'function'){
			instance[key] = function(){
				return function(){
					try{
						return method.apply(this, arguments);
					}catch(err){
						console.log("ERROR SERVICES: ",err);
					}
				}
			}(key,method);
		}
	}
	return instance;
}