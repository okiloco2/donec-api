const Donec = require("../../Donec");
const Schema = {
  "name": "String",
  "opt": "Mixed",
  "lastname": "String"
}
const Model = new Donec.Model("donec", Schema, "en");
module.exports = Model;