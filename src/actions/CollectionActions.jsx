const onEdit = collection => {
	return {
      type:"ON_EDIT_COLLECTION",
      collection:collection
    };
}
const onSelected = collection => {
	return {
      type:"ON_SELECT_COLLECTION",
      collection:collection
    };
}

export { onEdit, onSelected }