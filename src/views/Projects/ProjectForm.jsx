import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import PropTypes from "prop-types";
import Constants from '../../helper/Constants.jsx';
import Helper from '../../helper/Helper.jsx';
import store from '../../store.jsx'
import { onSaveCollection,onChangeRoute } from '../../actionCreators.jsx';
import {
  Step,
  Stepper,
  StepButton,
  StepLabel
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import { withRouter } from 'react-router';
import {
  getSchemas,
  add_new_project,
  install_database
} from '../../services/index.jsx';

const customContentStyle = {
  width: '400px',
  maxWidth: 'none',
};
const styles = {
  customWidth: {
    width: 150,
  },
};
const MAX_STEP = 2;

class ProjectForm  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	view:"new",
	    	stepIndex: 0,
	    	"error":"",
	    	"errors":{},
	    	"email":"",
	        "dbname": "",
	        "host": "localhost",
	        "port": "27017",
	        "username": "",
	        "password": ""
        };
  	}

  	componentWillMount() {
  	}
  	handleChange = (event) => {
  		let {name,value} = event.target;
	    this.setState({
	      [name]: value
	    });
  	}
  	handleNext = () => {
  		const {stepIndex,finished} = this.state;

  		if(!finished){
	  	    if(this.validate()){
		  	    this.setState({
		  	      stepIndex: stepIndex + 1,
		  	      finished: stepIndex == MAX_STEP,
		  	    });
	  	    }
  		}else{
  			this.setState({
  			  stepIndex:0,
  			  finished: false,
  			});
  		}
  	};

  	handlePrev = () => {
	    const {stepIndex} = this.state;
	    if (stepIndex > 0) {
	      this.setState({stepIndex: stepIndex - 1});
	    }
  	};
  	componentDidMount(){
  	   this.db_name.focus();
	}
  	getStepContent(stepIndex) {

  	    switch (stepIndex) {
  	      case 0:
  	        return <Row>
			 	<TextField
			 	autoComplete="off"
			 	required
			 	ref={(input) => { this.db_name = input; }} 
			 	onChange={this.handleChange}
	 	       	className="form-field"	
		       	errorText={this.state.errors.dbname}
		       	floatingLabelText="Nombre Base de datos"
		       	name="dbname"
		       	onChange={this.handleChange} 
		       	value={this.state.dbname}/>

  			</Row>
  	      case 1:
  	        return <Row>
		       <TextField
		       autoComplete="off"
		       onChange={this.handleChange}
	 	       className="form-field"	
		       errorText={this.state.errors.host}
		       floatingLabelText="Servidor"
		       name="host"
		       onChange={this.handleChange} 
		       value={this.state.host}/>
		       <TextField
		       autoComplete="off"
		       onChange={this.handleChange}
	 	       className="form-field"	
		       errorText={this.state.errors.port}
		       floatingLabelText="Puerto"
		       name="port"
		       onChange={this.handleChange} 
		       value={this.state.port}/>
  	        </Row>;
  	      case 2:
  	        return <Row>
		       <TextField
		       autoComplete="off"
		       onChange={this.handleChange}
	 	       className="form-field"	
		       errorText={this.state.errors.username}
		       floatingLabelText="Usuario"
		       name="username"
		       onChange={this.handleChange} 
		       value={this.state.username}/>

		       <TextField
		       autoComplete="off"
		       onChange={this.handleChange}
	 	       className="form-field"	
		       errorText={this.state.errors.password}
		       floatingLabelText="Contraseña"
		       name="password"
        	   type="password"
		       onChange={this.handleChange} 
		       value={this.state.password}/>
  	        </Row>;
  	      default:
  	        return <span className="error">{this.state.error}</span>;
  	    }
  	  }

  	handleSubmit = (e) => {
  		e.preventDefault();
  		const me = this;
  		const {finished,stepIndex,dbname,host,port,username,password } = this.state;
  		if(finished){
  			let payloads = {dbname,host,port,username,password};
  			
  			add_new_project(payloads)
  			.then( response => {

  				let {data} = response;
  				if(data.success){
  					me.setState({
  						view:"install"
  					});
  					me.formReset(payloads);
  				}else{
  					me.setState({
  						"error":data.msg
  					});
  				}
  			})
  			.catch(error => this.setState({"error":error.message}));
  		}
  	}

  	formClearErrors = () => {
  		let {errors} = this.state;
  		Object.keys(errors).forEach((key) => {
  		  errors[key] = "";
  		})
  		this.setState({errors,error:""});
  	}
  	formReset = (props) => {
  		this.formClearErrors();
  		Object.keys(props).forEach((key) => {
    	  	this.setState({[key]:""});
  		});
  	}
  	handleInstall = (e) => {
  		e.preventDefault();
  		let {email,password,errors} = this.state;
  		if(this.validateInstall()){
  			let payloads = {email,password};
  			//Instalar base de datos.
  			install_database(payloads)
  			.then(response => {
  				let {data} = response;
  				if(data.success){
			  		this.formReset(payloads);
		  			const { history } = this.props || [];
		  			history.push({
		  			  pathname: '/'
		  			});
  				}else{
  					this.setState({
  						error:data.msg
  					});
  				}
  			})
  			.catch(error => this.setState({"error":error.message}));
  		}
  	}

  	validateInstall = () => {
  		let isValid = true;
  		let {email,password, errors} = this.state;
  		
  		if(email==""){
  			errors["email"] = "Debe especificar un correo.";
  			isValid = false;
  		}
  		if(password==""){
  			errors["password"] = "Debe especificar una contraseña.";
  			isValid = false;
  		}
  		if(email.length > 0){
  			isValid = Helper.validateEmail(email);
  			console.log(isValid);
  			if(!isValid){
  				errors["email"] = "Debe especificar un correo válido.";
  			}
  		}
  		this.setState({errors});
  		return isValid;
  	}
  	validate = () => {
  		let isValid = true;
  		const {stepIndex,errors,dbname,host, port, username, password} = this.state;
  	    switch(stepIndex){
  	    	case 0:
    		    var white_spaces = /\s/g;
    		    var result = dbname.match(white_spaces);
    		    errors["dbname"] = "";
    		    if(!dbname){
    		    	errors["dbname"] = "Debe especificar un nombre para la base de datos.";
    		    }
    		    if(result){
    		    	errors["dbname"] = "El nombre de la base de datos no debe contener espacios en blanco.";
    		    }
		    	this.setState({
		    		errors
		    	});
		    	if(errors["dbname"]!=""){
		    		isValid = false;
		    	}
  	    	break;
  	    	case 1:
  	    		errors["host"] = "";
  	    		errors["port"] = "";
  	    		if(!host){ errors["host"] = "El campo Servidor es requerido.";}
  	    		if(!port){ errors["port"] = "El campo Puerto es requerido.";}
  	    		this.setState({
  	    			errors
  	    		});
	  	    	if(errors["host"]!="" || errors["port"]!=""){
	  	    		isValid = false;
	  	    	}
  	    	break;
  	    	default:
  	    }
  		return isValid;
  	}  
  	render(){

  		const {finished, stepIndex, view} = this.state;
	    const contentStyle = {margin: '0 16px'};

  		return(
  			<Container>	

		  			<div style={{width: '100%', maxWidth: 700, margin: 'auto'}}>
		  				{
		  					(view === "new")?
  							<form onSubmit={this.handleSubmit}>
			  				
						        <Stepper linear={false} activeStep={stepIndex}>
						          <Step>
						            <StepLabel>
						              Nombre de base de datos
						            </StepLabel>
						          </Step>
						          <Step>
						            <StepLabel>
						              Servidor de base de datos
						            </StepLabel>
						          </Step>
						          <Step>
						            <StepLabel>
						              Usuario de base de datos
						            </StepLabel>
						          </Step>
						        </Stepper>
						        <div style={contentStyle}>
							          <span>{this.getStepContent(stepIndex)}</span>
							          <div style={{marginTop: 12}}>
							            {
							            	(!finished)?
								            <FlatButton
								              className="back-button"	
								              label="Atrás"
								              disabled={stepIndex === 0}
								              onClick={this.handlePrev}
								              style={{marginRight: 12}}
								            />:null
							            }
							            <RaisedButton
							              type="submit"	
				                          label={stepIndex < MAX_STEP ? 'Siguiente': stepIndex == MAX_STEP ? 'Finalizar':'Reintentar' }
				                          primary={true}
				                          onClick={this.handleNext}
				                        />
							          </div>
						        </div>
					        </form>:
					        <form onSubmit={this.handleInstall}>
					        	<h3 style={{textAlign:"center"}}>Instalar base de datos</h3>
		        	  	        <Row>
		        			       <TextField
		        			       autoComplete="off"
		        			       onChange={this.handleChange}
		        		 	       className="form-field"	
		        			       errorText={this.state.errors.email}
		        			       floatingLabelText="Correo"
		        			       name="email"
		        			       onChange={this.handleChange} 
		        			       value={this.state.email}/>

		        			       <TextField
		        			       autoComplete="off"
		        			       onChange={this.handleChange}
		        		 	       className="form-field"	
		        			       errorText={this.state.errors.password}
		        			       floatingLabelText="Contraseña"
		        			       name="password"
		        	        	   type="password"
		        			       onChange={this.handleChange} 
		        			       value={this.state.password}/>
		        	  	        </Row>
		        	  	        <span className="error">{this.state.error}</span>
		        	  	        <div style={{marginTop: 12}}>
			        	  	        <RaisedButton
						              type="submit"	
			                          label="Instalar"
			                          primary={true}
			                        />
					          	</div>
					        </form>
				    	}
			      	</div>
  			</Container>
		);
  	}

}
export default withRouter(ProjectForm);