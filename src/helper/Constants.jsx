export default {
	defaultHashTag:"/database",
	URL_LOGIN:'admin/login',
	URL_SCHEMAS:"admin/schemas",
	URL_SCHEMA:"schemas",
	URL_GET_POOLS:"admin/get_db_pools",
	URL_CHANGE_ACTIVE_GROUP:"admin/change_active_group",
	URL_GET_CURRENT_ACTIVE_GROUP:"admin/get_current_active_group",
	URL_ADD_PRJECT:"admin/add_project",
	URL_INSTALL:"install"
}