import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import { Container, Row, Col } from 'reactstrap';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {cyan500,blue500,lightBlue500,grey50} from 'material-ui/styles/colors';
import AppPanel from '../../components/AppPanel.jsx';
import { withRouter } from 'react-router';
import store from '../../store.jsx';
import routes from '../../routes.jsx';
import Constants from '../../helper/Constants.jsx';
import {  BrowserRouter as Router, Redirect, HashRouter, Route, Switch } from 'react-router-dom';
const muiTheme = getMuiTheme({
  palette: {
    textColor: grey50,
    primaryText:grey50,
    primary1Color:blue500
  },
  appBar: {
    height: 50,
  },
});

class Home  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
        open: true,
        user:null,
        token:null,
        modules:null,
        routes:[]
      };
      this.getModules = this.getModules.bind(this);
  	}
    componentDidMount() {
      const { history } = this.props || [];
      console.log(this.state);
    }
    componentWillMount(){
      const {match, location} = this.props;
      var me = this, alias = match.params.alias;

      console.log("Home: ",match,location);
    }
    getModules(){
      if(store.getState()!=undefined){
        let user = store.getState().user;
        let modules = [];
        if(user!=undefined){
          modules = user.usergroup.modules;
          /*modules = routes.filter((route)=>{
              var module = modules.filter((item)=>{
                return (item.module.name == route.name);
              });
              return (module.length>0);
          });*/
          return routes;
        }
      }else{
        return [];
      }
    }
    render(){
      return(
        <MuiThemeProvider muiTheme={muiTheme}>
         <AppPanel count={0} routes={this.getModules()}/>
        </MuiThemeProvider>
		)
	}	
}

export default withRouter(Home);


