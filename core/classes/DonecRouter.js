const Router = require("./Router");
var express = require("express");
var router = express.Router();
const Constants = require("../../helpers/Constants");
var { pluralize } = require("../../helpers/helper");
const moment = require("moment");

var dbstatus_middleware = require("../middlewares/dbstatus");
var jwtmiddleware = require("../middlewares/jwtmiddleware");

class DonecRouter {
  constructor(route_path, app, db, io) {
    if (!app) throw new Error("Missing required App");
    let self = this;

    this.app = app;
    this.db = db;
    this.io = io;
    this.routes = [];
    this.route_path = route_path;

    //Aplicar middlewares
    //app.use(route_path,jwtmiddleware);
    //app.use(route_path,dbstatus_middleware);

    if (db) {
      db.on("define", function(name, model) {
        var schema = model.getSchema();
        var lang = schema.lang;
        var pathname = pluralize(lang || "es", name);
        pathname = "/" + pathname;

        //Restrict URL
        var full_path = route_path + "" + pathname,
          name;
        var route = { name: name, path: pathname, full_path };
        if (self.routes.indexOf(pathname) === -1) {
          //console.log("Register route: ",pathname,(self.routes.indexOf(pathname)));
          self.routes.push(pathname);
        }
        self.register(route);
      }).on("ready", function() {
        //console.log("Routes Registers: ",route_path,self.routes,router);
        self.app.use(self.route_path, router);
      });
    }
  }

  register(route) {
    let self = this;
    let { path } = route;
    if (this.db[route.name]) {
      //AQUI REGISTRAR LAS RUTAS GENERICAS

      let routes = [
        {
          ["/app" + route.path]: {
            get: this.list.bind(this.db[route.name])
          }
        },
        {
          ["/app" + route.path]: {
            post: this.save.bind(this.db[route.name])
          }
        },
        {
          ["/app" + route.path + "/:_id"]: {
            get: this.findById.bind(this.db[route.name])
          }
        },
        {
          ["/app" + route.path + "/:_id"]: {
            put: this.update.bind(this.db[route.name])
          }
        },
        {
          ["/app" + route.path + "/:_id"]: {
            delete: this.remove.bind(this.db[route.name])
          }
        }
      ];
      //>_Agregar Rutas para Socket Io
      let prev_routes = self.app.get("routes") || {};
      var arr_methos = ["get", "post", "put", "delete"];
      let item_route = {};
      routes.map(item => {
        let key = Object.keys(item)[0];
        let method_key = Object.keys(item[key])[0];
        let method = item[key][method_key];
        if (typeof prev_routes[`${method_key} ${key}`] === "undefined")
          item_route[`${method_key} ${key}`] = method.bind(
            self.db[route.name]
          );
        //console.log("item: ", key, method_key, typeof method);
      });
      //console.log("Se va a registrar la ruta: ", route.path, item_route);
      self.app.set("routes", { ...prev_routes, ...item_route });

      /* prev_routes.filter((item, index) => {
        let key = Object.keys(item)[0];
        let method_key = Object.keys(item[key])[0];
        let method = item[key][method_key];

        let url = "/app" + route.path;
        if (key === url && arr_methos.indexOf(method_key) !== "-1") {
          console.log("Ya existe: ", url, method_key, item, index);
          //prev_routes[index] = item;
        }
        //console.log("_>", key, method_key, moment().format("HH:mm:ss"));
      }); */
      //self.app.set("routes", [...prev_routes, ...routes]);

      router
        .route(route.path)
        .get(this.list.bind(this.db[route.name]))
        .post(this.save.bind(this.db[route.name]));
      //.put(this.update.bind(this.db[route.name]));

      router
        .route(route.path + "/:_id")
        .get(this.findById.bind(this.db[route.name]))
        .put(this.update.bind(this.db[route.name]))
        .delete(this.remove.bind(this.db[route.name]));
    } else {
      console.log("EL MODELO NO ESTA CREADO!", path);
    }
  }
  restrict_access(req, res, next) {
    return res.json({
      success: false,
      msg: ">>|" + Constants.TEXT_RESTRICT_ACCESS
    });
    return;
  }
  get services() {
    return this.services_routes;
  }
  list(req, res, next) {
    var params = req.query || {};
    console.log("GET: List " + this.modelName);
    this.search(params, function(err, docs) {
      return res.send(JSON.stringify({ success: docs.length > 0, data: docs }));
    });
  }
  findById(req, res, next) {
    var params = req.params;
    console.log("GET: findById " + this.modelName);
    this.search(params, function(err, docs) {
      return res.send(JSON.stringify({ success: true, data: docs }));
    });
  }
  save(req, res, next) {
    var params = req.body;
    var modelName = this.modelName;

    this.create(params, function(err, doc) {
      console.log(
        "POST: save " + modelName + "(" + (err ? "Fail" : "succefully") + ")"
      );
      if (err) {
        return res.send(
          JSON.stringify({
            success: false,
            msg: err.message || err._message || err
          })
        );
        return;
      }
      if (!doc) {
        return res.send(
          JSON.stringify({
            success: false,
            msg: err.errmsg || err._message || err
          })
        );
      } else {
        return res.send(
          JSON.stringify({
            success: true,
            _id: !params._id ? doc._id : undefined,
            msg: !params._id
              ? "Registro creado con éxito."
              : "Registro actualizado con éxito."
          })
        );
      }
    });
  }
  update(req, res, next) {
    var params = req.body;
    params["_id"] = req.params._id;
    var modelName = this.modelName;
    this.create(params, function(err, doc) {
      console.log(
        "PUT: update " + modelName + "(" + (err ? "Fail" : "succefully") + ")"
      );
      if (err) {
        return res.send(
          JSON.stringify({
            success: false,
            msg: err.message || err._message || err
          })
        );
        return;
      }
      if (!doc) {
        return res.send(JSON.stringify({ success: false, msg: err }));
      } else {
        return res.send(
          JSON.stringify({
            success: true,
            _id: !params._id ? doc._id : undefined,
            msg: !params._id
              ? "Registro creado con éxito."
              : "Registro actualizado con éxito."
          })
        );
      }
    });
  }
  remove(req, res, next) {
    var params = req.params;
    var modelName = this.modelName;
    this.removeById(params._id, function(msg, doc) {
      console.log(
        "DELETE: remove " +
          modelName +
          "(" +
          (msg ? "Fail" : "succefully") +
          ")"
      );
      return res.send(
        JSON.stringify({
          success: true,
          msg: msg
        })
      );
    });
  }
  get(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).get(callback);
  }
  post(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).post(callback);
  }
  put(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).put(callback);
  }
  delete(path, callback) {
    if (!callback) throw new Error("Missing required callback.");
    return router.route(path).delete(callback);
  }
  restrict(original_url) {
    let restrict_access = global.restrict_access;

    const _regex =
      "((GET)|(POST)|(PUT)|(DELETE))\\s(/){0,1}(" + original_url + ")";
    const str_regex =
      "((GET)|(POST)|(PUT)|(DELETE))\\s(/){0,1}([(\\w+(/){0,1})]+)";

    let restrinct = {};
    let _match = restrict_access.match(_regex);
    if (_match) {
      _match.forEach(item => {
        if (item) {
          if (item.match(str_regex)) {
            let method = item.substring(0, item.indexOf(" "));
            //console.log("Match: ",original_url, item," Method: "+method);
            restrinct[original_url] = method.toLowerCase();
          }
        }
      });
    }
    return restrinct;
  }
}
module.exports = DonecRouter;
