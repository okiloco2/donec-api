module.exports = function(sandbox) {
  const app = sandbox.app;
  const db = sandbox.db;

  return {
    init: function(schema) {
      var self = this;
      sandbox.get("/users", this.list_users);
      sandbox.delete("/users/:_id", function(req, res) {
        return res.json({
          success: false,
          msg: "No se puede eliminar usuarios"
        });
      });
    },
    list_users: function(req, res) {
      var params = req.query;
      console.log("*GET - User");

      return db.user
        .listar_users(params)
        .then(data => {
          res.send(
            JSON.stringify({
              success: true,
              data
            })
          );
        })
        .catch(err =>
          res.send(
            JSON.stringify({
              success: false,
              data: []
            })
          )
        );
    }
  };
};
