var jwt = require("jsonwebtoken");
module.exports = function(req,res,next){
	var access = req.originalUrl.includes("login");
	var auth = req.get("authorization");
	if(!access){
		jwt.verify(auth,process.env.SECRET_KEY,function(err,decode){
			let msg = "";
			if(err){
			   switch(err.name){	
			   		case "TokenExpiredError":
			   			msg ="La sesión ha expirado.";
			   		break;
			   		default:
			   			msg = err.message;
			   		break;
			   }	
			   //res.set("WWW-Authenticate", "Basic realm=\"Authorization Required\"");
			   return res.status(401)
			   .send(JSON.stringify({success:false,msg}));
			}else{
				next();
			}
		});
	}else{
  		next();
	}
}