/**
 * @SessionManager
 * La responsabilidad de esta clase, es definir que tipo de
 * almacenamiento utilizará la session de express
 * Las opciones disponibles son express, redis.
 */
"use strict";

var randtoken = require("rand-token");
var session = require("express-session");
//var RedisStore = require("connect-redis")(session);
class SessionManager {
  constructor() {
    this.secret = process.env.SECRET_KEY || randtoken.uid(256); //Random secret key
    this.sessionMiddleware;
  }

  start() {
    //Usar Sesión de Express
    if (typeof global.session !== "undefined") {
      var config = global.session.config || {};
      if (typeof global.session.type !== "undefined") {
        console.log("Tipo de Session: ", global.session.type);
        switch (global.session.type) {
          case "redis":
            var options = config.options || {}; //configuración de Redis
            this.sessionMiddleware = session({
              secret: this.secret,
              resave: false,
              saveUninitialized: false,
             /*  store: new RedisStore({
                ...options
              }), */
              ...config
            });
            break;
          case "express":
            this.sessionMiddleware = session({
              secret: this.secret,
              resave: false,
              saveUninitialized: false,
              ...config
            });
            break;
        }
      }
    } else {
      console.log("No se definió sesssion.");
    }
    return this.sessionMiddleware;
  }
}

module.exports = SessionManager;
