module.exports = {
  scope: "core",
  session_express: true,
  session: {
    type: "express",
    config: {
      secret: "11819ac6bf8a3272e4531c572e6ad145",
      time_out: "2h"
    }
  },
  allow_user_registration: false,
  token: {
    key_secret: "c20d8281f15f26ab809c4736ac1eda7b",
    time_out: "2h"
  },
  allowOrigins: ["*"],
  root: "/",
  basepath: "/server",
  database: {
    active_group: process.env.ACTIVE_GROUP || "donec",
    donec: {
      dbname: "donec",
      host: "ds011228.mlab.com",
      port: "11228",
      username: "admin",
      password: "okiykoki2009"
    }
  },
  plugins: ["Jwtoken256"],
  default_group: "guest",
  restrict_access: [
    "POST /app/groups",
    "POST /app/modules",
    "DELETE /app/users/:_id",
    "DELETE /app/groups/:_id",
    "DELETE /app/schemas/:_id",
    "GET /app/accesses"
  ],
  middlewares: ["dbstatus", "jwtmiddleware"]
};
