import React, { Component } from 'react';
import { render } from 'react-dom';
import App from './App.jsx';
import './css/App.css';

import store from './store.jsx';
import { Provider } from 'react-redux'

render(
	<Provider store={store}>
		<App/>
	</Provider>,
	document.getElementById("app")
);