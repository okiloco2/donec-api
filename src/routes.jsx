import {
	Dashboard,
	Panel,
	Database,
	Collection
} from './views/index.jsx';

const routes = [
	{ path: '/', exact: true, name: 'Panel', component: Panel,icon:"home", menu:false },
	{ path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard,icon:"dashboard", menu:true },
	{ path: '/database', exact: true, name: 'Database', component: Database,icon:"inbox", menu:true },
	{ path: '/database/:_name', name: 'Database', component: Database, menu:false },
	{ path: '/collection/:_name/:alias', exact: true, name: 'Collection', component: Collection,icon:"inbox", menu:false },
	{ path: '/collection/:_name/:alias/:_id', exact: true, name: 'Collection', component: Collection,icon:"inbox", menu:false },
];
export default routes;
