import Constants from '../helper/Constants.jsx';
import axios from 'axios';

if(typeof localStorage.getItem("token") !== "undefined"){
	//axios.defaults.headers.common['Authorization'] = localStorage.getItem("token");
	//axios.defaults.headers.common['token'] = localStorage.token;
}

/**
*@Authentication
*/
const Authentication = (params) => {
	return new Promise((resolve,reject) => {
		axios.post(Constants.URL_LOGIN,params)
		.then(response => {
			let {data} = response;

			if(data.succcess){
				axios.defaults.headers.common['Authorization'] = data.token;
				axios.defaults.headers.common['token'] = data.token;
			}
			resolve(data);
		})
		.catch(reject);
	});
}
/**
*@getSchemas
*/
const getSchemas = (params) => {
	return axios.get(Constants.URL_SCHEMAS,{params});
}
/**
*@removeSchema
*/
const removeSchema = (_id) => {
	return axios.delete(Constants.URL_SCHEMA+"/"+_id);
}
/**
*@getDbPools
*/
const getDbPools = () => {
	return axios.get(Constants.URL_GET_POOLS);
}
/**
*@changeActiveGroup
*/
const changeActiveGroup = (active_group) => {
	return axios.get(Constants.URL_CHANGE_ACTIVE_GROUP,{
		params: {
			active_group
		}
	});
}
/**
*@get_current_active_group
*/
const get_current_active_group = (active_group) => {
	return axios.get(Constants.URL_GET_CURRENT_ACTIVE_GROUP);
}
/**
*@add_new_project
*/
const add_new_project = (params) => {
	return axios.post(Constants.URL_ADD_PRJECT,params);
}
/**
*@install_database
*/
const install_database = (params) => {
	return axios.post(Constants.URL_INSTALL,params);
}
export {
	Authentication,
	getSchemas,
	getDbPools,
	removeSchema,
	changeActiveGroup,
	get_current_active_group,
	add_new_project,
	install_database
}