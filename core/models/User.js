const Donec = require("../../Donec");
const UserSchema = {
  email: { type: "String", unique: true, required: true },
  password: "String",
  usergroup: {
    type: "ObjectId",
    ref: "group",
    required: true
  }
};
const UserModel = new Donec.Model("user", UserSchema, "en");
UserModel.define("listar_users", function(params) {
  const self = this;
  return new Promise((resolve, reject) => {
    self
      .find(params)
      .select("-password")
      .populate({
        path: "usergroup",
        model: "group",
        populate: {
          path: "modules.module",
          model: "module"
        }
      })
      .exec(function(err, data) {
        if (err) {
          reject(err);
          return;
        }
        resolve(data);
      });
  });
})
  .virtual("test")
  .get(function() {
    return "email-" + this.email;
  });

module.exports = UserModel;
