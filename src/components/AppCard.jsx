import React, { Component } from 'react';
import AppCode from "./AppCode.jsx";
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';

import { onSaveCollection, onCodeError } from '../actionCreators.jsx';
import store from '../store.jsx'

class AppCard  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      lang:"",
      editable:false,
      actions:[]
    };
    /*store.subscribe(()=>{
      this.setState({
        editable:
      });
    });*/
    this.getChildren = this.getChildren.bind(this);
    this.handleExpand = this.handleExpand.bind(this);
  }
  handleExpandChange = (expanded) => {
    this.setState({expanded});
  };

  handleToggle = (event, toggle) => {
    this.setState({expanded: toggle});
  };

  handleExpand = () => {
    this.setState({expanded: !this.state.expanded});
  };

  handleReduce = () => {
    this.setState({expanded: false});
  };
  getChildren(){
    return (<CardMedia expandable={true}>{this.props.children}</CardMedia>);
  }
  componentDidMount() {
    try{
      this.setState({
        "expanded":this.props.expanded,
        "actions":this.props.actions || [],
        "lang":this.props.lang,
        "editable":this.props.editable || this.state.editable
      });
    }catch(err){
      console.log("*AppCard ERROR: ",err.message);         
    }
  }
  getActions(){
    return (<div className="actions">
      {this.state.actions.map((val,index) => {
        return <span className="card-action" key={index}>{val}</span>
      })}
    {(this.props.state=="0")?<span className="card-action"><IconButton tooltip={this.props.status} tooltipPosition="bottom-left" onClick={this.handleExpand}>
      <FontIcon className="material-icons icon-tool warn-icon">error_outline</FontIcon>
    </IconButton></span>:null} 
    </div>);
  }
  render(){
    return(
      <Card
      className={this.props.className || "code-item"} 
      expanded={this.state.expanded} 
      onExpandChange={this.handleExpandChange}>
        <CardHeader
              className="code-head"
              title={this.props.title}
              subtitle={
                (!this.state.editable || !this.state.lang)?this.props.subtitle:
                <SelectField

                menuItemStyle={{color:"#000"}}
                className="form-field field-white"
                onChange={(event, index, value) => {
                  this.setState({"lang":value});
                  store.dispatch(onSaveCollection({
                    "_id":store.getState().collection._id,  
                    "name":store.getState().collection.name,
                    "lang":value,
                    "config":store.getState().collection.config
                  }));

                }}

                floatingLabelText="Lenguaje"
                name="lang"
                value={this.state.lang}>
                <MenuItem value={"es"} primaryText="Español" />
                <MenuItem value={"en"} primaryText="Ingles" />
              </SelectField>
              }
              actAsExpander={false}
              showExpandableButton={true}>

              {this.getActions()} 
              </CardHeader>
              {
                this.getChildren()
              }
      </Card>
    );
  }

}

export default AppCard;



