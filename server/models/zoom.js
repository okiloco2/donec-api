const Donec = require("../../Donec");
const Schema = {
  "name": "String",
  "lastname": {
    "type": "String",
    "required":"true"
  },
  "category": {
    "type": "ObjectId",
    "ref": "rol"
  }
}
const Model = new Donec.Model("zoom", Schema, "en");
module.exports = Model;