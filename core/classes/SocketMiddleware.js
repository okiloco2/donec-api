const events = require("events");
const methods = ["get", "post", "put", "delete"];
const JwtValidator = require("./JwtValidator");

class SocketMiddleware extends events.EventEmitter {
  constructor(namespace, params, callback) {
    super(arguments);
    //let { namespace, method, params, method_key } = config;
    this.namespace = namespace || "";
    this.status = 200;
    this.method = callback;
    this.params = params;
    this.socket = null;
    this.io = null;
    this.validator = new JwtValidator();

    this.error = null;

    //this.method_key = method_key;
    //this.init();
    // io.use((socket, next) => {
    //var namespace = socket.nsp.name;
  }
  async checkNameSpace() {
    const self = this;
    let { namespace, params } = this;
    let method_key = namespace.substr(0, namespace.indexOf(" "));
    let { token } = params;
    if (!methods.includes(method_key)) {
      params = {
        msg: `method ${method_key} is not defined.`,
        success: false
      };
    }
    /* await this.validator
      .validate(token)
      .then(() => {
        //console.log("Token is Valid!");
        return console.log("Token is Valid!");
      })
      .catch(err => {
        self.error = {
          msg: `${err}`,
          status: 401,
          success: false
        };
        if (self.error) self.emit("error", this.error);
        console.log("El token?", self.error);
      }); */

    console.log("CheckNameSpace: ", namespace);
  }
  setRequest() {
    let { namespace, params, io } = this;
    let method_key = namespace.substr(0, namespace.indexOf(" "));
    this.req["method"] = method_key.toUpperCase();
    this.req["originalUrl"] = namespace.substr(
      namespace.indexOf(" "),
      namespace.length
    );

    switch (method_key) {
      case "get":
        this.req["query"] = params;
        this.req["body"] = params;
        break;
      case "post":
        this.req["body"] = params;
        break;
      case "put":
        this.req["params"] = params;
        this.req["body"] = params;
        break;
      case "delete":
        this.req["params"] = params;
        this.req["body"] = params;
        break;
      default:
        this.req["query"] = params;
        this.req["params"] = params;
        this.req["body"] = params;
        break;
    }
  }
  async init() {
    let self = this;
    let { namespace } = this;

    this.res = {
      json: async function(obj) {
        console.log("se ejecutó json!🤔", typeof obj);

        if (typeof obj === "string") {
          obj = JSON.parse(obj);
          obj["status"] = self.status;
        }
        self.emit(self.namespace, obj);
      },
      send: async function(obj) {
        console.log("se ejecutó send!🤔", typeof obj);
        if (typeof obj === "string") {
          obj = JSON.parse(obj);
          obj["status"] = self.status;
        }
        self.emit(self.namespace, obj);
      },
      emit: async obj => {
        if (typeof obj === "string") {
          obj = JSON.parse(obj);
          obj["status"] = self.status;
        }
        self.emit(self.namespace, obj);
      },
      status: function(status) {
        this.status = status;
        return self.res;
      }
    };
    this.req = {
      body: {},
      query: {},
      params: {}
    };
    if (!this.error) {
      this.setRequest();
      console.log("No hay ERRORES!", this.error);
      this.method(this.req, this.res, this.next);
    } else {
      console.log("SI hay ERRORES!", this.error);
      this.emit(namespace, this.error);
    }
  }
  next(params) {
    console.log("next");
  }
}
module.exports = SocketMiddleware;
