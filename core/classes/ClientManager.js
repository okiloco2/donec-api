/**
 * @ClientManager
 * La responsabilidad de esta clase es administrar que controlador de cliente
 * Se cominicará mediante Socket, si se utilizará redis como puente o solo Socket.
 */

//var redis = require("redis");

class ClientManager {
  constructor(app, db, io) {
    var self = this;
    this.sever = app.get("sever");
    this.sessionMiddleware = app.get("session_middleware");

    this.type = global.session ? global.session.type : "express";

    this.builder = this.builder(this.type, io);
    this.director = this.director(this.builder);
    this.director.subscribe("ready");

    //Utilizar la session de express
    io.use(function(socket, next) {
      if (typeof self.sessionMiddleware == "function")
        console.log("|-----------------------------------------------|");
      self.sessionMiddleware(socket.request, socket.request.res, next);
    });
  }

  builder(type, io) {
    var subscriber = io;
    switch (type) {
      case "redis":
        //subscriber = redis.createClient();
        break;
    }
    return subscriber;
  }

  director(builder) {
    var self = this;

    return {
      init: function(socket) {
        console.log("\t\t---> Init Socket!!");
        self.socket = socket;
      },
      register: function(route) {
        console.log(">> Builder Register: ", route);
        this.subscribe(route);
      },
      set_socket: function(_socket) {
        socket = _socket;
      },
      subscribe(route, callback) {
        let { name, path } = route;
        if (!callback) callback = () => {};
        //console.log("Subscribe: ",name,path);
        if (this.type == "redis") {
          return builder.subscribe(name);
        }
        return builder.on(name, callback);
      }
    };
  }
}

module.exports = ClientManager;
