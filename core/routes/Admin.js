var schedule = require('node-schedule');
var express = require("express");
var router = express.Router();
var jwt = require("jsonwebtoken");
var moment = require("moment");
var md5 = require("md5");
var path = require('path');
var Helper = require("../../helpers/helper");
const URL_CONFIG_DATABASE = path.join(global.SERVER_ROOT,'database','config.json');
const URL_TEMPLATE_MODELS = path.join(global.SERVER_ROOT,'database','models.json');

module.exports = function(sandbox){
	const app = sandbox.app;
	const db = sandbox.db;

	return {
		init:function(db,io){

			sandbox.get("/get_collection_by_alias",this.get_collection_by_alias);
			/* sandbox.post("/login",this.login); */
			//Listar pools de base de datos
			sandbox.get("/get_db_pools",this.get_db_pools);
			//Cambiar de base de datos
			sandbox.get("/change_active_group",this.change_active_group);
			sandbox.get("/get_current_active_group",this.get_current_active_group);
			sandbox.get("/get_routes",this.get_routes);
			sandbox.post("/add_project",this.add_project);
			sandbox.get("/refresh",this.refresh);
			sandbox.get("/logout",this.logout);

			app.get("/logout",this.logout);

			
		},
		refresh:function(req,res){
			var params = req.query;
			db.switchBD(params.active_group)
			.then(function(){
				return res.send(JSON.stringify({msg:"refres",success:true}));
			});
		},
		listarUsers:function(req,res){
			db.user.listar(req.query,function(docs){
				return res.send(JSON.stringify({data:docs,success:true}));
			});
		},
		add_project:function(req, res){
			let {dbname,host,port,username,password} = req.body;
			
			Helper.readFile(URL_CONFIG_DATABASE)
			.then(function(config){

				if(!(dbname in config)){

					Helper.readFile(URL_TEMPLATE_MODELS)
					.then( model => {
						let {collections} = model;

						config[dbname] = {
							dbname:dbname.toLowerCase(),
							host,
							port,
							collections
						}
						if(username!=""){
							config[dbname]["username"] = username;
						}
						if(password!=""){
							config[dbname]["password"] = password;
						}

						Helper.writeFile(URL_CONFIG_DATABASE,config)
						.then(function(){

							//Cambiar a base de datos creada
							db.switchBD(dbname)
							.then(function(){
								return res.json({
									success:true,
									msg:"Proyecto creado con éxito."
								})
							},err => {
								return res.send(JSON.stringify({msg:err.message,success:false}));
							});

						},function(err){
							return res.json({
								success:false,
								msg:err.message
							});
						});

					}, error => {
						return res.json({
							success:false,
							msg:error.message
						});
					});
				}else{
					return res.json({
						success:false,
						msg:"Ya existe un proyecto con el nombre "+dbname
					});
				}
			});	
		},
		get_routes:function(req,res){
			return res.json({msg:"hola"});
		},
		get_current_active_group:function(req,res){
			return res.json({"active_group":db.active_group.dbname});
		},
		change_active_group:function(req,res){
			var params = req.query;
			db.switchBD(params.active_group)
			.then(function(){
				return res.send(JSON.stringify({msg:"Base de datos cambiada con éxito",success:true}));
			},err => {
				return res.send(JSON.stringify({msg:err.message,success:false}));
			});
		},
		get_db_pools:function(req,res){
			Helper.readFile(URL_CONFIG_DATABASE).
			then(function(config){

				let pools = [];
				for(var key in config){
					if(key!="active_group"){
						pools.push(config[key]);
					}
				}
				res.status(200)
				.send(JSON.stringify({
					"success":true,
					"data":pools	
				}));

			});
		},
		get_collection_by_alias:function(req,res){
			let alias = req.query.alias;
			db.schema.listar({},(err,docs)=>{

				res.status(200)
				.send(JSON.stringify(docs.filter((val)=>{return val.alias===alias})[0]));
			});
		},
		login:function(req,res){
			var params = req.body;
			params["password"] = md5(params.password);


			console.log("HP LOGIN!!!");
			let user_id = req.session.user_id;
			/*if(user_id)
				return res.send(JSON.stringify({
					success:false,
					msg:"Ya existe una sesión activa"
				}));*/
				
				db.user.findOne({
					"email":params.email,
					"password":params.password
				})
				.select("-password")
				.populate({
					path:"usergroup",
					model:"group",
					select:"_id name modules",
					match:{"name":"superadmin"},
					populate:{
						path:"modules.module",
						model:"module"
					}
				})
				.exec(function(err,user){

					if(err){
						return res.send(JSON.stringify({
							"msg":err.message,
							"success":false
						}));
					}
					if(!user){
						return res.send(JSON.stringify({
							success:false,
							msg:"No hay ninguna cuenta con el correo electrónico que has introducido."
						}));						
					}
					if(user.usergroup){
						params["group"] =  user.usergroup.name;
						const Jwt = sandbox.loadPlugin("Jwtoken256");
						const payloads = {
							"email":params.email,
							"password":params.password,
							"group":params.group
						}
						let token = Jwt.sign(payloads);

						db.access.findOne({"user_id":user.id},(err,doc) => {
							
							if(!doc){

								
								/*var token = jwt.sign(payloads,process.env.SECRET_KEY,{
									expiresIn:process.env.SESSION_TIMEOUT
								});*/
								req.session.user_id = user._id;
								req.session.group = user.usergroup.name;
								req.session.token = token;

								db.access.create({"token":token,"user_id":user._id, "date_login":new Date(),"date_logout":null,"active":true},(err) => {
									if(!err){
										//console.log("Token guardado!");
										return res.send(JSON.stringify({
											"user":user,
											"token":token,
											"msg":"Ya existe una sesión activa",
											"success":true
										}));
									}else{
										return res.send(JSON.stringify({
											"msg":err.message,
											"success":false
										}));
									}
								});
							}else{
								db.access.findOne({"user_id":user.id,"active":true},(err,doc) => {

									if(err){
										return res.send(JSON.stringify({
											"msg":err.message,
											"success":false
										}));
									}
									req.session.user_id = user._id;
									req.session.group = user.usergroup.name;
									req.session.token = token;

									db.access.update({"user_id":user.id},{"token":token,"date_login":new Date(),"date_logout":null,"active":true},(err) => {
										if(!err){
											//console.log("Token actualizado!");

											return res.send(JSON.stringify({
												"user":user,
												"token":token,
												"success":true
											}));
										}else{
											return res.send(JSON.stringify({
												"msg":err.message,
												"success":false
											}));
										}
									});		
								});
							}
						});
					}else{
						return res.send(JSON.stringify({
							success:false,
							msg:"El usuario no tiene acceso como Super Admin."
						}));
					}
				});
		},
		logout:function(req,res){

			var token = req.headers['authorization'] || req.headers['token'];
			if(token){	
				let {user_id} = req.session;
				if(user_id)
				db.access.findOne({user_id,token,"active":true},(err,doc) => {

					if(err){
						return res.send(JSON.stringify({
							"msg":err.message,
							"success":false
						}));
					}
					if(doc){
						db.access.create({"_id":doc._id,"token":token,"user_id":user_id,"active":false,"date_logout":new Date()},(err) => {
							//console.log("Token Desactivado!");
							req.session.destroy();
							res.status(401)
							.json({"msg":"Sesión terminada","success":true});
						});
					}else{
						res.status(401)
						.json({"msg":"No hay sesión activa para el usuario.","success":true});
					}
				});
			}
			
		}
	}
}

