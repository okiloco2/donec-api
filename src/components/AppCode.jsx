import React, { Component } from 'react';

var CodeMirror = require('react-codemirror');
import "../../node_modules/codemirror/lib/codemirror.css";
import "../../node_modules/codemirror/theme/material.css";
import "../../node_modules/codemirror/mode/javascript/javascript.js";
/*Lint CodeMirror*/
import "../../node_modules/codemirror/addon/lint/lint.js";
import "../../node_modules/codemirror/addon/lint/json-lint.js";
import "../../node_modules/codemirror/addon/lint/lint.css";

import "../../node_modules/codemirror/keymap/sublime.js";
import Subheader from 'material-ui/Subheader';
import { onSaveCollection, onMessage, onCodeError } from '../actionCreators.jsx';
import { onEdit, onSelected } from  "../actions/CollectionActions.jsx";

import store from '../store.jsx'

class AppCode  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	config:"",
	    	err:null
	    }
	    let state = store.getState();
	    store.subscribe(() => {
	      /*this.setState({
	        message:{
	          "open":true,
	          "msg":"_state.error"
	        }
	      });*/
	    });
	    this.displayError = this.displayError.bind(this);
	}
	getJsonConfig(config){
		try{
			return vkbeautify.json(config);
		}catch(e){
			return "";
		}    	
	}
	readOnly(){
		console.log("readOnly",this.props.readOnly)
		return (this.props.readOnly || true);
	}
	displayError(){
		return (typeof this.state.err === 'object' && this.state.err!=null)?<span color="red">ERROR: {this.state.err.message}</span>:(null);
	}
	render(){
		return(
			<div className="app-code">
				<CodeMirror
				value={this.getJsonConfig(this.props.config)}
				options={{
				  readOnly:this.props.readOnly,
				  mode: {
				    name: 'application/javascript',
				    jsonld: true,
				    lineWrapping: true
				  },
				  dragDrop:true,
				  keyMap:'sublime',
				  theme: 'material',
				  matchBrackets: true,
				  autoCloseBrackets: true,
				  lineNumbers: true,
				  gutters: ["CodeMirror-lint-markers"],
				  lint: true
				}}
				onChange={(val,editor) => {
					
				  try{
				    val = vkbeautify.json(val);
				    
				    if(this.props.editor!=undefined){
				    	store.dispatch(onSelected(JSON.parse(val)));
				    }else{
					    store.dispatch(onSaveCollection({
					      "_id":store.getState().collection._id,	
					      "name":store.getState().collection.name,
					      "lang":store.getState().collection.lang,
					      "state":store.getState().collection.state,
					      "status":store.getState().collection.status,
					      "config":val
					    }));
				    }
				    store.dispatch(onCodeError(""));
				    var message = {
				    	msg:"",
				    	open:false
				    }
				    store.dispatch(onMessage(message));
				    this.setState({
				      "config":val,
				      "err":null
				    });
				  }catch(error){
				  	var message = {
				  		msg:error.message,
				  		open:true
				  	}
				    store.dispatch(onMessage(message));
				    store.dispatch(onCodeError(error.message));
				    this.setState({
				      "err":error
				    });
				  };
				}}/>
			</div>
			
		);
	}
}
export default AppCode;

