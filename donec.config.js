module.exports = {
  scope: "app",
  dev: false,
  //protocol:"socket",
  root: "/",
  basepath: "/server",
  session_express: true,
  allowOrigins: ["http://localhost:3001"],
  session: {
    type: "express",
    config: {
      secret: "11819ac6bf8a3272e4531c572e6ad145"
    }
  },
  plugins: ["Hola", "Hola2"],
  token: {
    key_secret: "c20d8281f15f26ab809c4736ac1eda7b",
    time_out: "2h"
  },
  middlewares: ["mymiddleware"],
  restrict_access: ["GET /app/users"],
  allow_user_registration: false
};
