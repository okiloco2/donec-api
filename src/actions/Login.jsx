const onLogin = auth => {
	return {
      type:"ON_LOGIN",
      user:auth.user,
      token:auth.token
    };
}
export { onLogin };