/**
 * @author: Fabian Vargas Fontalvo
 * email: f_varga@hotmail.com
 * Compatible con Nodejs: 7.10.0 >
 */

var events = require("events"); //Events administra eventos
const mongoose = require("mongoose");
var md5 = require("md5");
mongoose.Promise = global.Promise;
var dateFormat = require("dateformat");
var moment = require("moment");
var { pluralize } = require("../helpers/helper");
moment.locale("es");

var ObjectId = mongoose.Types.ObjectId;
const Mixed = mongoose.Types.Mixed;

var fs = require("fs");
var path = require("path");

// var Schema = mongoose.Schema;
var config = require("../database/config.json");
var str2json = require("../helpers/str2json");
var Helper = require("../helpers/helper");
var Constants = require("../helpers/Constants");
var Setting = require("./helpers/Constants");

var jsonfile = require("jsonfile");
exports.createManagerDB = function(options, callback) {
  return new ManagerDB(options, callback);
};

const CORE_MODELS = ["module", "access", "user", "group"];
/*
 * @ManagerDB: options
 * Tiene la responsabilidad de
 * Conectarse con la base de datos,
 * Registra los Esquemas en la Collection schemas
 * Cargar y crear instancias de Modelo con los esquemas registrados en schemas
 * Administra los Eventos y funciones básicas de un Modelo.
 */
function ManagerDB(options) {
  var self = this;
  this.url = path.join(global.SERVER_PATH, "database", "config.json");
  self.models = {};
  self.schemas = {};
  this.config = {};
  self.autoRefesh = true;
  //"mongodb://localhost:27017/canguro-app"
  this.setConfig = function(config) {
    try {
      /**
       * HACER QUE EL ACTIVE GROUP PUEDA SER DINAMICO
       */
      var poolDb = global.pool_database[process.env.ACTIVE_GROUP];
      //console.log("\tPool Db:",poolDb);
      if (poolDb) {
        if (poolDb.collections != undefined) {
          self.collections = poolDb.collections;
        }

        self.active_group = poolDb;
        self.dbname = self.active_group.dbname;

        self.host = self.active_group.host;
        self.port = self.active_group.port;

        self.username = self.active_group.username;
        self.password = self.active_group.password;

        //console.log("VALIDATE: ",self.username,self.username!=undefined)
        if (typeof self.username !== "undefined") {
          self.linkconex =
            "mongodb://" +
            self.username +
            ":" +
            self.password +
            "@" +
            self.host +
            ":" +
            self.port +
            "/" +
            self.dbname;
        } else {
          self.linkconex =
            "mongodb://" + self.host + ":" + self.port + "/" + self.dbname;
        }
        self.conn = mongoose.connection;
        self.config = {
          host: self.host,
          port: self.port,
          username: self.username,
          password: self.password,
          linkconex: self.linkconex
        };
      }
    } catch (err) {
      console.log("[ERROR]", err);
    }
  };
  //para emitir eventos ilimitados
  this.setMaxListeners(0);
}

/**
* @Model: name,schema
* Crea una instancia de Model de mongoose
* Devuelve el Model
+ name: String Nombre del Schema
+ schema: Object instancia de mongoose.Schema
*/
function Model(name, schema) {
  if (typeof mongoose.connection.models[name] !== "undefined") {
    /*delete self.models[name];
		delete self.schemas[name];
		delete mongoose.connection.models[name];
		delete self[name];*/
  }
  var model = mongoose.model(name, schema);

  model["schema"] = schema;
  model["getSchema"] = function() {
    return schema;
  };
  return model;
}
/**
* @Schema: name,config,virtuals
* Crea una instancia de Schema de mongoose
* Devuelve el Schema
+ name: String Nombre del Schema
+ config: Object Campos del Schema
+ virtuals: function (Sin uso por ahora)
*/
function Schema(name, options, virtuals) {
  var schema;
  function Password(key, options) {
    mongoose.SchemaType.call(this, key, options, "Password");
  }
  Password.prototype = Object.create(mongoose.SchemaType.prototype);
  Password.prototype.cast = function(val) {
    var _val = md5(val);
    return _val;
  };
  mongoose.Schema.Types.Password = Password;

  //NOTA: Importante establecer las propiedades toObject y toJSON para que funcione.
  schema = new mongoose.Schema(options, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  });
  return schema;
}

/*Permite emitir Eventos personalizados*/
ManagerDB.prototype = new events.EventEmitter();

ManagerDB.prototype.getActiveGroup = function() {
  return this.active_group;
};
ManagerDB.prototype.updateActiveGroup = poolDb => {
  const me = this;
  let url = path.join(global.SERVER_ROOT, "database", "config.json");
  return new Promise((resolve, reject) => {
    Helper.readFile(url).then(
      function(config) {
        let current_active_group = config.active_group;
        //Modificar active_group
        config[current_active_group] = poolDb;

        Helper.writeFile(url, config).then(
          function() {
            resolve(config);
          },
          function(err) {
            reject(err);
          }
        );
      },
      function(err) {
        //console.log(">>ERROR: ",err,url);
        reject(err);
      }
    );
  });
};

ManagerDB.prototype.getConfig = function() {
  return this.config;
};
ManagerDB.prototype.switchBD = function(active_group) {
  var self = this;

  return new Promise((resolve, reject) => {
    if (typeof active_group === "undefined") {
      reject({ message: "No se pasó un active group." });
      return;
    }
    if (active_group == process.env.ACTIVE_GROUP) {
      resolve();
      return;
    }

    Helper.readFile(self.url).then(
      function(config) {
        let linkconex = self.linkconex;
        config["active_group"] = active_group;
        self.setConfig(config);

        Helper.writeFile(self.url, config).then(
          function() {
            try {
              //console.log("Disconnected from " + self.linkconex + " through app termination");
              // todo workaround for HMR. It remove old model before added new ones
              Object.keys(mongoose.connection.models).forEach(key => {
                //console.log("delete ",key);
                delete mongoose.connection.models[key];
              });
              //Registrar los schemas de la base de datos
              for (var s in self.models[s]) {
                delete self.models[s];
              }
              for (var s in self.schemas[s]) {
                delete self.schemas[s];
              }
              if (typeof self.active_group.collections !== "undefined") {
                self.active_group.collections.forEach(item => {
                  let name = item.name;
                  delete self[name];
                });
              }
              //Cambiar active group
              process.env.ACTIVE_GROUP = active_group;

              resolve(config);
              mongoose.connection.close(function() {
                console.log("Unconnected from: ", linkconex);
                console.log("..................................");
                self.connect().then(
                  function() {
                    resolve();
                  },
                  function(error) {
                    reject(error);
                  }
                );
              });
            } catch (error) {
              reject(error);
              return;
            }
          },
          function(err) {
            //console.log("No se pudo modificar el archivo de configuración.");
          }
        );
      },
      function(err) {
        //console.log("Error al cargar archivo de base de datos.");
        reject(err);
      }
    );
  });
};

ManagerDB.prototype.getCollections = function() {
  return this.collections;
};
/*
 * @ObjectId:_id
 * Util para utilizar en los documentos como primary key o
 * referencia de oto modelo.
 * this.ObjectId(some_id);
 */
ManagerDB.prototype.ObjectId = function(_id) {
  return ObjectId(_id);
};

/**
 * @parseObject: options,callback
 * Convierte un objeto relación con clave valor y tipo
 * para campos de esquemas.
 * Los campos estan disponibles en el objeto {this.fieds}
 */
ManagerDB.prototype.parseObject = function(obj) {
  var self = this;
  for (var s in obj) {
    var field = obj[s];
    if (typeof field == "object") {
      if (Array.isArray(field)) {
        if (field.length === 1 && field[0] in mongoose.Schema.Types) {
          //Se permiten campos compuestos dispuestos en Schema.Types
          obj[s][0] = mongoose.Schema.Types[field[0]];
        } else {
          field.forEach(function(item, index) {
            obj[s][index] = self.parseObject(item);
          });
        }
      } else {
        if ("type" in field) {
          switch (field.type) {
            case "Date":
              if ("default" in field) {
                if (field.default == "now") {
                  field.default = Date.now;
                }
              }
              break;
          }
          if (typeof mongoose.Schema.Types[field.type] !== "undefined") {
            if (typeof mongoose.Schema.Types[field.type] !== "undefined") {
              field.type = mongoose.Schema.Types[field.type];
            }
          }
        } else {
          if (typeof mongoose.Schema.Types[field.type] !== "undefined") {
            field.type = mongoose.Schema.Types[field.type];
          } else {
            for (var k in field) {
              var fieldChild = field[k];
              obj[s][k] = self.parseObject(fieldChild);
              ////console.log("\t--->",k,fieldChild);
            }
          }
        }
        obj[s] = self.parseObject(field);
      }
    } else {
      obj[s] = field;
    }
  }
  return obj;
};

ManagerDB.prototype.parseFieldsMap = function(obj) {
  var self = this;
  for (var s in options) {
    var field = options[s];
    if (typeof field == "object") {
      if (Array.isArray(field)) {
        field[s]["type"] = "Array";
        field.forEach(function(item, index) {
          field[s][index] = self.parseFieldsMap(item);
        });
      } else {
        if ("ref" in field) {
          fields[s]["type"] = "Object";
        }
      }
    } else {
      field[s]["type"] = field;
    }
  }
  return obj;
};
ManagerDB.prototype.setFieldsMap = function(options, callback) {
  var self = this;
  var fields = {};
  return new Promise(function(resolve, reject) {
    if (!fields) fields = {};
    for (var s in options) {
      var field = options[s];
      if (typeof field == "string") {
        // //console.log(s+" es un string simple.");
        fields[s] = { type: "String" };
      } else if (typeof field == "object") {
        if (Array.isArray(field)) {
          // //console.log(s+" es array.")
          fields[s] = { type: "Array" };
          field.forEach(function(item, index) {
            //var fieldChild = self.setFieldsMap(item);
            var fieldChild = item;

            for (var key in item) {
              var subitem = item[key];

              if (subitem.hasOwnProperty("ref")) {
                fields[s]["ref"] = subitem.ref;
                // //console.log("\tsubitem:",subitem)
              }
            }
            if (fieldChild.hasOwnProperty("ref")) {
              fields[s]["ref"] = fieldChild.ref;
              // //console.log("\titem:",fieldChild)
            }
          });
        } else {
          if (field.hasOwnProperty("ref")) {
            // //console.log(s+" es object secundario",s)
            fields[s] = { type: "Object", ref: field["ref"] };
          } else {
            fields[s] = {
              type: field.type != undefined ? field.type : "Object"
            };
            // //console.log(s+" es object principal",s)
          }
        }
      }
    }

    resolve(fields);
  });
};
/**
 * @createSchema: name,options,callback
 * Crea esquemas, inicializa funciones Estaticas
 * para ser usadas en el modelo para CRUD
 * Esta función define los metodos search, create y delete
 * Una vez instanciado el schema se emite un evento con el nombre de éste.
 * Ejemplo:
 * on("NOMBRE_SCHEMA",callback);
 */
ManagerDB.prototype.createSchema = function(name, options, lang, callback) {
  var self = this;
  var state = "1";

  return new Promise(function(resolve, reject) {
    var fields = {};
    self["name"] = name;

    if (typeof options == "string") {
      options = options.replace("\n", "");
      options = options.replace(/\\/g, "");
      try {
        options = JSON.parse(options);
      } catch (e) {
        throw "Error al transformar JSON en: " +
          name +
          "\n" +
          e +
          "\n" +
          options;
      }
    }
    var raw_config = options;

    /*options.timestamps ={
	        createdAt: 'Date',
	        updatedAt: 'Date'
	    };*/

    /*Objeto convertido con parametros de Schema validos para mongoose*/
    if (name != "schema") {
      options = self.parseObject(options);
    }

    /**
     * Emitir evento previo a la creación de Schema.
     */

    self.emit("pre-" + name, options);

    var schema;
    try {
      schema = Schema(name, options, self.virtuals);

      if (typeof mongoose.connection.models[name] !== "undefined") {
        //console.log("EL MODELO YA ESTA CREADO:",name);
      }

      //console.log("Options: ",options);
      // schema = self.getSchema(name) || Schema(name,options,self.virtuals);
      /*Mapping de campos del eschema*/
      self.setFieldsMap(options).then(function(fields) {
        schema.statics.fields = fields;
      });
      self.schemas[name] = schema;
      self.schemas[name]["name"] = name;
      schema["lang"] = lang;
      // schema["name"] = name;

      schema.statics.getFields = function() {
        //Obtener fields de la colleccion
        let fields = this.fields;
        if (!Helper.isEmpty(fields)) {
          //console.log("Felds: ",fields);
        }
        return fields;
      };
      schema.statics.setFields = function(fields) {
        this.fields = fields;
      };

      schema.statics.set = function(name, value) {
        this[name] = value;
      };
      schema.statics.get = function(name) {
        return this[name];
      };
      //Retornar los campos del Schema
      schema.statics.getFieldsMap = function(params) {
        var fields = this.getFields();
        var out_fields = !params ? [] : {};
        /*if(Helper.isEmpty(fields)){
					fields = params;
				}*/

        if (typeof params !== "undefined") {
          for (var key in fields) {
            if (typeof params[key] !== "undefined") {
              out_fields[key] = params[key];
            }
          }
        } else {
          for (var key in fields) {
            out_fields.push(key);
          }
        }
        // //console.log(out_fields);
        return out_fields;
      };
      /**
       * @fieldsMap: params,model
       * Se encarga de mapear los parametros de entrada
       * relacionandolos con los campos del esquema.
       * Devuelve el modelo con los datos pasados.
       */
      schema.statics.fieldsMap = function(params, model) {
        var fields = this.getFields();
        var output = {};

        return new Promise(function(resolve, reject) {
          //console.log("PARAMS:",params,Helper.isEmpty(params))
          if (!Helper.isEmpty(params)) {
            for (var key in params) {
              var field = fields[key];
              var val = params[key];
              if (name == "test") {
                //console.log("key: ",key,"value: ",val);
              }
              if (typeof field !== "undefined") {
                if (field.type == "Array" || field.type == "Object") {
                  output[key] = field.type == "Array" ? [] : {};
                  if (typeof field.ref != "undefined") {
                    // output[field.ref] = (field.type == 'Array')?[]:{};

                    if (typeof val == "string") {
                      if (field.type == "Array") {
                        var item = {};
                        item[field.ref] = val;
                        output[key].push(item);
                      } else {
                        output[key] = val;
                      }
                    } else if (typeof val == "object") {
                      if (Array.isArray(val)) {
                        val.forEach(function(record, index) {
                          var item = {};
                          item[field.ref] = record;
                          if (field.type == "Array") {
                            output[key].push(item);
                          } else {
                            output[key] = item;
                          }
                        });
                      } else {
                        output[key] = val;
                      }
                    }
                  } else {
                    if (typeof val == "string") {
                      output[key].push(val);
                    } else if (typeof val == "object") {
                      if (Array.isArray(val)) {
                        val.forEach(function(record) {
                          output[key].push(record);
                        });
                      } else {
                        for (var s in val) {
                          var record = val[s];
                          output[key][s] = record;
                        }
                      }
                    }
                  }
                } else {
                  if (field.type == "Date") {
                    if (field.default != undefined) {
                      if (field.default == "now") {
                        output[key] = moment().format("YYYY-MM-DD HH:mm:ss");
                      }
                    }
                  } else {
                    output[key] = val;
                  }
                }
              } else {
                output[key] = val;
                //console.log("no definido.",key,val);
              }
            }
            //Objeto resultante
            if (typeof model !== "undefined") {
              for (var s in fields) {
                if (s != "id" || s != "_id") {
                  model[s] = output[s];
                }
                //output[s] = model[s];
                //console.log("\t"+s,model[s])
              }
            }
            //console.log(output);
            //console.log("---------------");
            resolve(output);
          } else {
            reject("**Se requieren parametros de entrada.");
          }
        });
      };
      schema.statics.create = function(params, callback) {
        var me = this;

        if (!Helper.isEmpty(params)) {
          params.id = params.id || params._id;

          //Cuando se va a crear un nuevo Schema el parámetro config debe ser tipo String.
          if (name == "schema") {
            if (typeof params.name === "undefined") {
              if (callback != undefined)
                callback({
                  success: false,
                  msg: "Debe especificar el nombre del Schema."
                });
              return;
            }
            if (typeof params.config === "undefined") {
              if (callback != undefined)
                callback({
                  success: false,
                  msg: "Debe especificar configuración para Schema."
                });
              return;
            }
            if (typeof params.lang === "undefined") {
              params["lang"] = "es";
            }
            if (typeof params["config"] === "object") {
              try {
                params["config"] = JSON.stringify(params["config"]);
              } catch (err) {
                if (callback != undefined) callback(err);
                return;
              }
            }
          }

          if (params.id) {
            this.findById(params.id, function(err, doc) {
              //doc - Representa una instancia del mondelo mongoose.
              if (!doc) {
                if (callback != undefined) {
                  callback("No existe registro.");
                  return;
                }
              }

              //params = me.getFieldsMap(params);

              me.fieldsMap(params, doc).then(
                function(obj_map) {
                  //console.log("Mapping: ",obj_map);
                  doc.update(obj_map, doc, function(err, obj_map) {
                    if (err) throw err;
                    if (callback != undefined) callback(err, doc);
                  });
                },
                function(err) {
                  if (callback != undefined) callback(err);
                }
              );
            });
          } else {
            var model = this;
            //params = this.getFieldsMap(params);

            if (Helper.isEmpty(params)) {
              if (callback != undefined) {
                callback({
                  message: "Primero debe definir campos para la collección."
                });
                return;
              }
            }

            //El crear de Schemas es diferente
            this.fieldsMap(params).then(
              function(obj_map) {
                var instance = new model(obj_map); //Instancia del Modelo
                instance.validate(function(err) {
                  if (err) {
                    console.log("ERROR: ", err.message);
                    if (err.name == "ValidationError") {
                      var { validation_err } = {};
                      if (name in err) {
                        //console.log("ERROR NAME!",err[name]);
                      }
                      for (var s in err.errors) {
                        //console.log("--> VALIDATION ERROR: ",err.errors[s].message);
                        validation_err = err.errors[s];
                        break;
                      }
                      switch (validation_err.kind) {
                        case "required":
                          validation_err["message"] =
                            "The field " + validation_err.path + " is required";
                          break;
                      }
                      if (callback != undefined) {
                        callback(validation_err);
                        return;
                      }
                    } else {
                      // A general error (db, crypto, etc…)
                      if (callback != undefined) {
                        callback(err);
                        return;
                      }
                    }
                  }

                  //Guardar instancia de Modelo
                  instance.save(function(err, doc) {
                    if (err) {
                      if (callback != undefined) {
                        if (callback != undefined) callback(err, doc);
                      }
                      // throw err;
                      return;
                    }
                    if (name == "schema") {
                      self.autoRefesh = true;

                      self.refresh(function() {
                        if (callback != undefined) {
                          callback(err, doc);
                        }
                      });
                    } else {
                      if (callback != undefined) {
                        if (callback != undefined) callback(err, doc);
                      }
                    }
                  });
                });
              },
              function(err) {
                //console.log(err);
                if (callback != undefined) {
                  callback(err);
                }
              }
            );
          }
        } else {
          if (callback != undefined) {
            callback("No se encontraron parametros de entrada.");
          }
        }
      };
      schema.statics.query = function(params, callback) {
        var query = null;
        if (params._id) {
          query = this.findById(params._id, function(err, doc) {
            if (callback != undefined) callback(err, query);
            //console.log("query "+name+":",params);
            return query;
          });
        } else {
          params = this.getFieldsMap(params);
          query = this.find(params, function(err, docs) {
            if (callback != undefined) callback(err, query);
            //console.log("query "+name+":",params);
            return query;
          });
        }
      };
      schema.statics.search = function(params, callback) {
        if (params._id) {
          this.findById(params._id, function(err, doc) {
            if (!doc) {
              doc = [];
              if (callback != undefined) {
                return callback(err, doc);
              }
            }
            if (callback != undefined) callback(err, doc);
            return doc;
          });
        } else {
          params = this.getFieldsMap(params);
          this.find(params, function(err, docs) {
            if (callback != undefined) callback(err, docs);
            return docs;
          });
        }
      };
      schema.statics.findOneById = function(id, callback) {
        if (!id) {
          if (callback != undefined) callback();
        } else {
          this.findById(id, function(err, doc) {
            if (callback != undefined) callback(err, doc);
          });
        }
      };
      schema.statics.removeById = function(id, callback) {
        if (id) {
          this.findByIdAndRemove(id, function(err, doc) {
            if (!doc) {
              if (callback != undefined) callback("No existe registro.");
            } else {
              if (callback != undefined) callback(null, doc);
            }
          });
        } else {
          /*if(!Helper.isEmpty(params)){
						this.remove(params,function(err,doc){
							if(!doc){
								if(callback!=undefined) callback("No existe registro."); 
							}else{
								if(callback!=undefined) callback("Registro eliminado.",doc);
							}
						});
					}else{
					}*/
          if (callback != undefined)
            callback("No se encontraron parametros de entrada.");
        }
      };

      /**
			* Emitir evento de Schema creado.
			+ #on
			*/
      self.emit(`prev-${name}`, schema);
      self.emit(name, schema);
      self.changeState(name, state);
      //register Schema
      resolve(schema);
    } catch (err) {
      state = "0";
      self.changeState(name, state, err, function(err) {
        reject(err.message);
      });
    }
  });
};
/**
 * @changeState: name,state,callback
 * Maquina de estados de Schemas
 */
ManagerDB.prototype.changeState = function(name, state, msg, callback) {
  msg = typeof msg === "function" ? "" : msg;
  callback = typeof msg === "function" ? msg : callback;

  if (name != "schema") {
    var schema = this.getModel("schema");
    schema.findOne({ name: name }, function(err, doc) {
      if (doc) {
        doc.state = state;
        if (msg == "") {
          switch (state) {
            case "0":
              msg = "Error de sintaxis en el Schema " + name;
              break;
            case "1":
              msg = "Esquema activo " + name;
              break;
            case "-1":
              msg = "Esquema inactivo " + name;
              break;
          }
        }
        doc.status = msg;
        doc.save(function(err) {
          if (callback != undefined) {
            callback(msg);
          }
        });
      } else {
        if (callback != undefined) {
          callback(msg);
        }
      }
    });
  } else {
    if (callback != undefined) {
      callback(msg);
    }
  }
};
/**
 * @createModel: name,schema,callback
 * Crea modelos, los modelos manejan la relación con la base
 * de datos, proporcianan todos los eventos y metodos accesibles
 * en la API de mongoose.
 */
ManagerDB.prototype.createModel = function(name, schema, callback) {
  var self = this;

  try {
    //var model = this.getModel(name) || new Model(name,schema);
    //Eliminar Modelo Existente
    if (typeof schema === "undefined")
      throw new Error("No se definió un schema para el modelo.");
    /* if(typeof mongoose.connection.models[name] !== "undefined"){
			delete self.models[name];
			delete self.schemas[name];
			delete mongoose.connection.models[name];
			delete self[name];
		} */
    var model = new Model(name, schema);

    this.models[name] = model;
    this.models[name]["name"] = name;

    this[name] = model;
    if (callback != undefined) {
      callback(model);
    }
    /**
     * Emitir eventos para el manejador de la base de datos
     * y para el Schema, ambos reciben como parametro el modelo instanciado.
     */
    if (name == "schema") {
      self.emit("define", name, model);
      schema.emit("define", model);
    }
    return model;
  } catch (error) {
    return console.log("Error en " + name, error);
  }
};
/*
* @create: {name,options},callback
+ name:String Nombre del Modelo
+ options:object Parametros del Modelo
+ callback:function Funcion de Devolución
* Crea una instancia de un modelo y guarda sus valores.
* [Devuelve] la instancia creada y el modelo.
* Útil par crear un Schema inexistente en la base de datos.
**/
ManagerDB.prototype.create = function({ name, options }, callback) {
  var self = this;
  var schema = this.getSchema(name); //No retorna el Schema
  /*  if(typeof schema === "undefined"){
		console.log("Schem is not defined ",name);
		return;
	 } */
  var model = this.createModel(name, schema);

  //Crear nueva instancia de modelo
  var instance = new model(options);
  instance.save(function() {
    if (callback != undefined) {
      callback(instance, model);
    }
  });
  return instance;
};
/**
 * @load callback
 * Carga el archivo de configuración de la base de datos
 * database/config.json
 */
ManagerDB.prototype.load = function(callback) {
  var self = this;
  return new Promise(function(resolve, reject) {
    Helper.readFile(self.url).then(
      function(config) {
        self.setConfig(config);
        resolve(config);
      },
      function(err) {
        //console.log("Error al cargar archivo de base de datos.");
        reject(err);
      }
    );
  });
};
/**
 * @register name,config,lang,callback
 * Inserta documentos en la Colleción Schemas.
 * Si ya existe el registro lo actualiza.
 */
ManagerDB.prototype.register = function(name, config, lang, callback) {
  var self = this;
  //Registra esquema en la collection schema
  try {
    if (name != "schema") {
      var model = this.getModel("schema"); //obtener modelo Schema
      if (typeof model === "undefined")
        return console.log("Modelo ya no existe en la base de datos.", name);

      var params = {
        name: name,
        config: config
      };

      model.findOne({ name: name }, function(err, doc) {
        if (typeof config == "object") {
          config = JSON.stringify(config);
          config = config.replace("\n", "");
          config = config.replace(/\\/g, "");
          //console.log("Es objeto.",name);
        }
        if (!doc) {
          //Crear Schema en base de datos
          self.create(
            {
              name: "schema",
              lang: lang,
              options: { name: name, lang: lang, config: config }
            },
            function() {
              //Actualizar los esquemas de la base de datos

              //self.refresh(function(){
              //console.log("Se registró el esquema: ",name+".")
              if (callback != undefined) {
                callback(!doc);
              }
              //});
            }
          );
        } else {
          //Establecer el lenguaje del Schema
          //console.log("model: ",name," ya está en base de datos.");
          if (callback != undefined) {
            callback(!doc);
          }
        }
      });
    } else {
      if (callback != undefined) {
        callback(false);
      }
    }
  } catch (error) {
    console.log(new Error(error));
  }
};

ManagerDB.prototype.Model = function() {
  const self = this;
  var models = {};
  function getModel(modelId) {
    return models[modelId];
  }
  function create() {
    //return new Promise((resolve,reject) => {
    let { name, config, lang } = self;
    self.createSchema(name, config, lang).then(
      schema => {
        let model_instance = self.createModel(schema.name, schema);
        //resolve(model_instance);
      },
      err => reject(err)
    );
    //});
  }
  return {
    define: function(modelId, schema, lang) {
      self.models[modelId] = {
        schema: schema,
        lang: lang,
        instance: null
      };
    },
    create: function(modelId, callback) {
      let { schema, lang } = getModel(modelId);

      return new Promise((resolve, reject) => {
        self.createSchema(modelId, schema, lang).then(
          function(schema) {
            //Instancia modelo Schema
            var model = self.createModel(schema.name, schema);
            if (typeof callback !== "undefined") {
              callback(model);
            }
            resolve(model);
          },
          err => reject(err)
        );
      });
    }
  };
};

/*
 * @define: callback
 * Crea nuevas instancias de Modelos
 * con su respectivo Schema registrado en collection Schemas
 */
ManagerDB.prototype.define = function(params) {
  params = params || {};
  var self = this;
  return new Promise(function(resolve, reject) {
    var modelSchema = {
      name: { type: "String", unique: true },
      lang: { type: "String", default: "es", required: true },
      state: { type: "String", default: "1" },
      status: { type: "String", default: "" },
      config: { type: "Mixed", required: true }
    };
    //Crear el Esquema principal de la base de datos, que contiene todos los esquemas.

    self.createSchema("schema", modelSchema, "es").then(
      function(schema) {
        //Instancia modelo Schema
        var modelSchema = self.createModel(schema.name, schema);

        //Cargar Schemas de la base de Datos y generar Modelos
        //console.log("... Cargando Schemas ...");
        // global.Msg("Atención","Cargando Schemas...");
        modelSchema.find(params, function(e, docs) {
          if (docs) {
            if (docs.length > 0) {
              getModels().then(models => {
                //console.log("SCHEMAS: ",JSON.stringify(models,null,2));

                self.defineSchemas(models).then(
                  function() {
                    // global.Msg("Atención","END defineSchemas: "+docs.length);
                    self.autoRefesh = true;
                    resolve();
                  },
                  function(err) {
                    reject(err);
                  }
                );
              });
              // global.Msg("Atención","defineSchemas: "+docs.length);
            } else {
              // global.Msg("Atención","defineCoreSchemas");
              self.defineCoreSchemas().then(
                function() {
                  self.autoRefesh = true;
                  resolve();
                },
                function(err) {
                  //console.log("ERROR: ",err.message);
                }
              );
            }
          } else {
            console.log("No hay documentos.");
            reject("No hay documentos.");
          }
        });
      },
      function(err) {
        // global.Msg("Error","Error al crear esquema");
        //console.log("Error al crear esquema. ",err);
        reject(err);
      }
    );
  });
};

function getModels() {
  const _path = path.join(global.APP_PATH, "models");
  const core_models_path = path.join(global.CORE_PATH, "models");
  let schemas = {};
  return new Promise((resolve, reject) => {
    //Modelos de la Aplicación
    readModels(_path).then(_schemas => {
      schemas = _schemas;

      //Modelos del Core
      readModels(core_models_path).then(_schemas => {
        Object.values(_schemas).forEach(item => {
          var { name } = item;
          if (!(name in schemas)) {
            schemas[name] = item;
          }
        });
        resolve(Object.values(schemas));
      });
    });
  });
}
function readModels(_path) {
  let models = {};
  var schemas = {};
  const core_models_path = path.join(global.CORE_PATH, "models");

  //console.log("Read path: ",_path);

  return new Promise((resolve, reject) => {
    fs.readdirSync(_path).reduce((schemas, filename, index, arr) => {
      var ext = path.extname(filename);
      const file = path.join(_path, filename);
      var file_name = path.parse(file).name.toLowerCase();

      if (ext) {
        if (ext === ".js") {
          //Cargar models del Core
          if (_path === core_models_path) {
            models[file_name] = require("./models/" + filename);
          } else {
            //Cargar models de Application
            var app_path_name = path.basename(global.APP_PATH);
            let app_model_path = "./" + app_path_name + "/models";
            let model_path = app_model_path + "/" + filename;
            models[file_name] = require.main.require(model_path);
          }
          let model = models[file_name];
          let name = model.name || file_name;
          let alias = pluralize(model.lang || "es", name);
          model["alias"] = alias;

          schemas[name] = model;
          if (index === arr.length - 1) {
            resolve(schemas);
          }
        }
      }
      return schemas;
    }, schemas);
  });
}
ManagerDB.prototype.defineCoreSchemas = function(callback) {
  var self = this;
  self.autoRefesh = false;
  //var schemas = self.getCollections();
  var model = self.getModel("schema");
  var models = {};
  console.log(">> Define core Schemas: ");

  const core_models_path = path.join(global.CORE_PATH, "models");
  const app_models_path = path.join(global.APP_PATH, "models");

  let schemas = {};
  function readFiles(_path) {
    fs.readdirSync(_path).reduce((schemas, filename, index, arr) => {
      var ext = path.extname(filename);
      const file = path.join(_path, filename);
      var file_name = path.parse(file).name.toLowerCase();
      if (ext) {
        if (ext === ".js") {
          //Cargar models del Core
          if (_path === core_models_path) {
            models[file_name] = require("./models/" + filename);
          } else {
            //Cargar models de Application
            var app_path_name = path.basename(global.APP_PATH);
            let app_model_path = "./" + app_path_name + "/models";
            let model_path = app_model_path + "/" + filename;
            models[file_name] = require.main.require(model_path);
          }
          let model = models[file_name];

          //console.log("MODEL: ",model);
          schemas[model.name || file_name] = model;
        }
      }
      return schemas;
    }, schemas);
    return Object.values(schemas);
  }

  return new Promise(function(resolve, reject) {
    //Load Core models
    var _schemas = readFiles(core_models_path);

    //Load app models
    Helper.createDirectoryAndFile(
      app_models_path,
      Setting.FILENAME_README,
      Setting.TEXT_MODELS
    ).then(() => {
      //Load Core models
      _schemas = readFiles(app_models_path);
      self.defineSchemas(_schemas).then(
        function() {
          //console.log("Se crearon los esquemas del core.");
          resolve();
        },
        err => reject(err)
      );
    });
  });
};
ManagerDB.prototype.redefineSchema = function(schema, callback) {
  var self = this;
  return new Promise(function(resolve, reject) {
    /*Object.keys(mongoose.connection.models).forEach(key => {
		  delete mongoose.connection.models[key];
		});*/
    let { name } = schema;
    if (typeof mongoose.connection.models[name] !== "undefined") {
      console.log("Ya existe hay que eliminar!", self.models);
      delete self.models[name];
      delete self.schemas[name];
      delete mongoose.connection.models[name];
      delete self[name];
    }

    self.createSchema(schema.name, schema.config, schema.lang).then(
      function(sch) {
        //Crea nueva instancia de modelo
        self.createModel(schema.name, sch, function(model) {
          /**
           * Emitir eventos para el manejador de la base de datos
           * y para el Schema, ambos reciben como parametro el modelo instanciado.
           */
          //if(schema.name!="schema"){
          self.emit("define", schema.name, model);
          sch.emit("define", model);
          //}
          self.changeState(schema.name, "1");
          resolve(model);
        });
      },
      function(err) {
        self.changeState(schema.name, "0", err);
        reject(err);
      }
    );
  });
};

ManagerDB.prototype.defineSchemas = function(schemas) {
  var self = this;
  var count = 1;
  return new Promise(function(resolve, reject) {
    schemas.forEach(function(doc, index, arr) {
      let name = doc.name.toLowerCase();
      //Registrar los esquemas en collection Schemas
      self.register(name, doc.config, doc.lang, function() {
        //Crear Esquema con la configuración registrada.
        self.createSchema(name, doc.config, doc.lang).then(
          function(sch) {
            //Eliminar Modelo Existente
            /*if(typeof mongoose.connection.models[name] !== "undefined"){
						delete self.models[name];
						delete self.schemas[name];
						delete mongoose.connection.models[name];
						delete self[name];
					}*/
            //Crea nueva instancia de modelo
            self.createModel(name, sch, function(m) {
              /**
               * Emitir eventos para el manejador de la base de datos
               * y para el Schema, ambos reciben como parametro el modelo instanciado.
               */
              if (name != "schema") {
                self.emit("define", name, m);
                sch.emit("define", m);
              }
              //global.Msg("index: ", index+"="+(arr.length-1));
              if (index == arr.length - 1) {
                resolve();
              }
              count++;
            });
          },
          function(err) {
            //console.log("Error::: ",err);
            reject(err);
          }
        );
      });
    });
  });
};
ManagerDB.prototype.getStatus = function() {
  return mongoose.connection.readyState;
};
ManagerDB.prototype.refresh = function(refresh, callback) {
  var self = this;
  var state = mongoose.connection.readyState;
  if (typeof refresh == "function") {
    callback = refresh;
    refresh = self.autoRefesh;
  }
  if (state === 0) {
    if (callback != undefined) {
      callback({
        message: Constants.TEXT_STATUS_DISCONNECTED
      });
    }
    return;
  }

  refresh = refresh || self.autoRefesh;
  if (refresh) {
    // todo workaround for HMR. It remove old model before added new ones
    Object.keys(mongoose.connection.models).forEach(key => {
      //console.log("delete ",key);
      delete mongoose.connection.models[key];
    });
    //Registrar los schemas de la base de datos
    for (var s in self.models[s]) {
      delete self.models[s];
    }
    for (var s in self.schemas[s]) {
      delete self.schemas[s];
    }

    //AQUI

    getModels().then(models => {
      models.forEach(item => {
        let name = item.name;
        delete self[name];
      });

      self.define().then(
        function() {
          //console.log("ManagerDB Ready.")
          //self.emit("ready");
          //console.log("refresh.");
          if (callback != undefined) {
            callback();
          }
        },
        function(err) {
          if (callback != undefined) {
            callback();
          }
        }
      );
    });
  } else {
    if (callback != undefined) {
      callback();
    }
  }
};
ManagerDB.prototype.restart = function(callback) {
  var self = this;

  //Registrar los schemas de la base de datos
  for (var s in self.models[s]) {
    delete self.models[s];
  }
  for (var s in self.schemas[s]) {
    delete self.schemas[s];
  }

  // todo workaround for HMR. It remove old model before added new ones
  Object.keys(mongoose.connection.models).forEach(key => {
    delete mongoose.connection.models[key];
  });

  mongoose.disconnect(function() {
    mongoose.connect(self.linkconex, { useMongoClient: true }, function() {
      self.define().then(
        function() {
          //console.log("ManagerDB Ready.")
          //self.emit("ready");
          //console.log("restart.");
          if (callback != undefined) {
            callback();
          }
        },
        function(err) {
          if (callback != undefined) {
            callback();
          }
        }
      );
    });
  });
};
/**
* @connect: callback
* Conecta con la base de datos MongoDB
* utilizando la propiedad linkconex de ManagerDB.
* Una vez conectado, procede a crear el esquema base
* "schema", luego crea una instancia del modelo schema.
* consulta los registros, para crear los esquemas
* y modelos de cada registro.
* La composición base de un esquema es:
+ name:String
+ config: String
* name: Representa el nombre del Schema
* config: Almacena un String en formato Json, con la
* configuración del schema.
*/
let intentos = 0;
let MAX_INTENTOS = 5;
ManagerDB.prototype.connect = function(callback) {
  var self = this;
  return new Promise(function(resolve, reject) {
    /* Helper.readFile(self.url)
		.then(function(config){ */

    let config = global.pool_database[global.active_group];
    //console.log("____>>>>>",config);

    //#Asignar configuracion a base de datos
    self.setConfig(config);

    var state = mongoose.connection.readyState;

    console.log("manager DB Conect to: ", self.linkconex);
    mongoose.connect(self.linkconex, { useMongoClient: true }).then(
      function() {
        /**
         * Definir los schemas de la base de datos
         */
        self.define().then(
          function() {
            console.log("ManagerDB Ready.");
            console.log("..................................");
            console.log("Connected to " + self.linkconex);
            self.emit("ready");
            resolve("Conectado a la base de datos.");
          },
          function() {
            self.emit("ready"); //Listo pero con errores
            resolve(
              "Conectado a la base de datos. Algunos esquemas no se pudieron definir."
            );
          }
        );
      },
      error => {
       /*  if (intentos < MAX_INTENTOS) {
          intentos++;
          setTimeout(() => {
            self.connect(callback);
          }, 1000);
        } else { */
          reject(error);
        /* } */
      }
    );

    mongoose.connection.on("connected", function() {
      // console.log("Donec is connected to: ",self.linkconex);
    });

    mongoose.connection.on("error", function(error) {
      //console.log("Error connect to ", error);
    });

    mongoose.connection.on("disconnected", function() {
      //console.log("Disconnected from " + self.linkconex);
    });

    process.on("SIGINT", function() {
      mongoose.connection.close(function() {
        //console.log("Disconnected from " + self.linkconex + " through app termination");
        process.exit(0);
      });
    });

    /* },reject);	 */
  });
};
/**
 *@disconnect: callback
 * Desconecta de la base de datos
 */
ManagerDB.prototype.disconnect = function(callback) {
  mongoose.disconnect(callback);
};
/**
 * @getSchema: name
 * Devuelve un Schema registrado en la colección schemas de ManagerDB.
 */

ManagerDB.prototype.getSchema = function(name) {
  return this.schemas[name];
};
/**
 * @getSchemasMaps
 * Devuelve un objeto que contiene todos los Schemas registrados en el objeto schemas de ManagerDB.
 */

ManagerDB.prototype.getSchemasMaps = function() {
  return this.schemas;
};
/**
 * @getSchemas
 * Devuelve un array con todos los Schemas registrados en el objeto schemas de ManagerDB.
 */

ManagerDB.prototype.getSchemas = function() {
  var schemas = [];
  for (var key in this.schemas) {
    schemas.push(this.schemas[key]);
  }
  return schemas;
};
/**
 * @getModel: name
 * Devuelve un Modelo registrado en la colección models de ManagerDB.
 */

ManagerDB.prototype.getModel = function(name) {
  return this[name] || mongoose.connection.models[name] || {};
};
ManagerDB.prototype.getCoreModels = function() {
  return CORE_MODELS;
};
ManagerDB.prototype.dropCollection = function(name, callback) {
  return mongoose.connection.dropCollection(name, callback);
};
