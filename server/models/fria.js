const Donec = require("../../Donec");
const Schema = {
  "name": "String",
  "lastname": "String",
  "p": "Boolean",
  "po": "String",
  "enc": "Boolean",
  "ab": "String",
  "cea": "Boolean",
  "ves": "String",
  "do": "Boolean",
  "a": "String"
}
const Model = new Donec.Model("fria", Schema, "es");
module.exports = Model;