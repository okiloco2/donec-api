'use strict';
const fs = require ('fs'); 
const jwt = require ('jsonwebtoken');
var randtoken = require('rand-token');
const path = require("path");
var Donec = require("../../Donec.js");

Donec.Plugin("Jwtoken256",(sandbox) => {
    return {
        init:function(){
            //console.log("Init Plugin JWToken!");
        },
        sign:function(payload){
            var token = jwt.sign(payload, process.env.SECRET_KEY, { expiresIn: process.env.SESSION_TIMEOUT || '2d'}) ;
            return token;
        },
        verify:function(token,callback){
            return jwt.verify(token, process.env.SECRET_KEY, callback);
        },
        decode: (token) => {
            return jwt.decode(token, {complete: true});
        },
        sign_options:function(params) {
           signOptions = {params};
        },
        verify_options:function(params) {
            verifyOptions = {params};
        }
    }
});