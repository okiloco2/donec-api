import { createStore } from 'redux';
import Helper from "./helper/Helper.jsx";

const reducer = (state,action)=>{


	
	/*
	let _state = state;
	if(typeof(state) !== "undefined" && state!=null){
	  localStorage.setItem("state",JSON.stringify(state));
	  _state = state;
	}else{
	  _state = JSON.parse(localStorage.getItem("state")); 
	}
	console.log(state,action);
	*/
	switch(action.type){
		case "CHANGE_PATH":
			return{
				...state,
				name:action.name,
				path:action.path
			}
		break;
		case "ON_LOGIN":
			return {
		      ...state,
		      user:action.user,
		      token:action.token
		    };
		break;
		case "SAVE_COLLECTION":
			return {
		      ...state,
		      collection:action.collection
		    };
		break;
		case "GET_COLLECTION":
			return {
		      ...state,
		      collection:action.collection
		    };
		break;
		case "GET_COLLECTIONS":
			return {
		      ...state,
		      collections:action.collections
		    };
		break;
		case "CODE_ERROR":
			return {
		      ...state,
		      code_error:action.code_error
		    };
		break;
		case "ON_MESSAGE":
			return {
		      ...state,
		      message:action.message
		    };
		break;
		case "ON_EDIT_COLLECTION":
			return{
				...state,
				collection:action.collection
			}
		break;
		case "ON_SELECT_COLLECTION":
			return{
				...state,
				collection:action.collection
			}
		break;
		case "ON_LINK_ROUTE":

			return{
				...state,
				links:(state.links!=undefined)?state.links.concat(action.links).filter((item) => {
					return item.link!=action.links.link;
				}):[]
			}
		break;

		case "ON_CHANGE_ROUTE":
			
			let {routes, forward} = action;
			state["routes"] = (typeof(state.routes)!== "undefined")?state.routes:[];
			if(!routes){
				return{
					...state,
					routes:[]
				}
			}
			if(routes.view){
				let links = action.routes;
				links["order"] = state.routes.length;
				return{
					"view":routes.view,
					...state,
					routes:Helper.uniqArray(state.routes.concat(action.routes))
				}
			}
			if(forward){
				let links = Helper.uniqArray(state.routes.concat(action.routes),"link");
				links.forEach((item, index) => {
					if(item.link){
					  let match = item.link.match(/(\/)\w+/g);	
					  item["order"] = (match)?match.length:0;
					  links[index] = item;
					}
				});
				state.routes = Helper.sortArray(links,"order");
				return{
					...state,
					routes:state.routes
				}
			}else{
				state.routes = state.routes.filter(item => {
					return (item.order < routes.order);
				});
			}
		break;

		case "ON_CHANGE_ACTIVE_GROUP":
			return{
				...state,
				"active_group":action.active_group
			}
		break;

	}

	return state;
}
export default createStore(reducer,{name:""});