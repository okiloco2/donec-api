const mongoose = require("mongoose");
const Constants = require("../helpers/Constants");
module.exports = function(req,res,next){
	
	var state = mongoose.connection.readyState;

	var lvl_access = req.originalUrl.includes("/app");
	if(lvl_access == "/app"){
		
		if(!process.env.ALLOW_USER_REGISTRATION){
			res.status(200)
			.json({
				success:false,
				msg:Constants.TEXT_ALLOW_USER_REGISTRATION
			});
			return;
		}
	}
	if(state === 0){
		res.status(200)
		.send(JSON.stringify({
			success:false,
			msg:Constants.TEXT_STATUS_DISCONNECTED
		}));
	}else{
		next();
	}
}