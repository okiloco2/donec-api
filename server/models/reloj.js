const Donec = require("../../Donec");
const Schema = {
  "name": "String",
  "hora": "Number",
  "segundos": "Number"
}
const Model = new Donec.Model("reloj", Schema, "es");
module.exports = Model;