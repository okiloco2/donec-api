const Donec = require("../../Donec");
const Schema = {
  "name": "String",
  "modelo": "String",
  "marca": {
    "type": "String",
    "required": true
  },
  "proveedor": {
    "type": "ObjectId",
    "ref": "group"
  }
}
const Model = new Donec.Model("moto", Schema, "es");
module.exports = Model;