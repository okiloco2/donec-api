import React, { Component } from 'react';
import Helper from "../../helper/Helper.jsx";
import { withRouter } from 'react-router';
import {AppHeader} from '../../components/index.jsx';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {cyan500,blue500,lightBlue500,grey50} from 'material-ui/styles/colors';
import Avatar from 'material-ui/Avatar';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import { Button, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';


import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ContentFilter from 'material-ui/svg-icons/content/filter-list';
import FileFileDownload from 'material-ui/svg-icons/file/file-download';

import {
    getDbPools,
    changeActiveGroup
} from "../../services/index.jsx";
import store from "../../store.jsx";
import ProjectForm from "./ProjectForm.jsx";
import {
  Breadcrumb
} from "../../components/index.jsx";

const muiTheme = getMuiTheme({
  palette: {
    textColor: grey50,
    primaryText:grey50,
    primary1Color:blue500
  },
  appBar: {
    height: 50
  },
});



class Projects  extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
            view:"projects",
            projects:[],
            active_group:""
	    }
	}
    componentWillMount(){

        let {location:{state}} = this.props;

        if(state){
            console.log("_State: ",state);
        }

        store.subscribe(() => {
          let state = store.getState();
          let {active_group} = state;
          if(active_group){
            this.setState({active_group});
          }
        });

        getDbPools()
        .then(response => {
            let {data} = response;
            let projects = data.data;
            this.setState({
                projects
            });
            console.log("Projects: ",data);
        })
        .catch(err => console.log("ERROR: ",err.message));

    }

    handleNewProject = () => {
        console.log("New Proeject!");
        this.setState({
            view:"add-project"
        });

    }
    onItemTab = (active_group) => {

        console.log("chagen active_group: ",active_group);
        const { history, location } = this.props || [];
        if(this.state.active_group !== active_group){
            changeActiveGroup(active_group)
            .then(response => {
                let {data} = response;
                if(data.success){
                    history.push({
                        pathname: '/',
                        state:{
                            active_group
                        }
                    });
                }else{

                }
            })
            .catch(err => console.log("ERROR: ",err.message));
        }else{
            history.push({
                pathname: '/',
                state:{
                    active_group
                }
            });
        }
    }

    handleItemClick = (value,dbname) => {
        console.log(value);
        switch(value){
            case "1":
                this.onItemTab(dbname);
            break;
            default:
        }

    }
	render(){
        let {projects, view} = this.state;
        const ItemProject = (props) => {
            let {dbname,host,port} = props;
             return(
                 <div className="card-container"> 
                     <Card className={"my-card"+((this.state.active_group === dbname)?" current-project":"")}>
                         <CardHeader
                              title={
                                <div>
                                    <span>{host}</span>
                                    <div className="more-button">
                                       <IconMenu
                                         targetOrigin={{ vertical: 'top', horizontal: 'left'}}
                                         anchorOrigin={{ vertical: 'bottom', horizontal: 'left'}}
                                         iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                                         onChange={(e,value) => this.handleItemClick(value,dbname)}
                                         value={this.state.valueSingle}
                                       >
                                         <MenuItem value="1" primaryText="Seleccionar" />
                                         <MenuItem value="2" primaryText="Configuracion" />
                                         <MenuItem value="3" primaryText="Eliminar" />
                                       </IconMenu>
                                    </div>
                                </div>
                              }
                              subtitle={port}
                         />
                         <CardText className="my-card-text">
                            <div>
                                <i className="material-icons icon-home">remove_red_eye</i> 
                                <span><a onClick={() => this.onItemTab(dbname)}>{dbname}</a></span>
                            </div>
                         </CardText>
                     </Card>
                 </div>
             );
        }
		return(
			<MuiThemeProvider muiTheme={muiTheme}>

			  <div>	
				  <div className="projects">	
					  <AppHeader className="app-toolbar ligth-header"/>
                      
					  <Container className="projects-welcome flex-row">

				          <Row>
				            <Col xs="6" sm="4">
                                <div>
    				            	{
                                        (view === "projects")?
                                       <div> 
                                           <h2>Te damos la bienvenida a Donec</h2>
                                           <p>Herramienta ideal para agilizar el desarrollo de apps Web/Móvil/Desktop.</p>
                                       </div>:
                                       <div> 
                                           <h2>Administra Bases de datos.</h2>
                                           <p>Configura y administra distintas bases de datos de forma rápida y segura.</p>
                                       </div>
                                    }
				            	</div>				            	
				            </Col>
				            <Col sm="6" sm="4">
				            </Col>
				            <Col sm="4"></Col>
				          </Row>
			        </Container>
        		    <Container className="card-projects flex-row align-items-center">
        	          <Row>
        	            <Col xs="6" sm="4"></Col>
        	            <Col xs="12">
                                <Toolbar className="app-toolbar">
                                       <ToolbarTitle text={
                                            <div className="link-collection">
                                           {
                                                (view === "add-project")?
                                                <div>
                                                    <i className="material-icons icon-home">arrow_back_ios</i> 
                                                    <span><a onClick={() => this.setState({view:"projects"})}>Proyectos</a></span>
                                                </div>
                                                :<div>
                                                    <i className="material-icons icon-home">dns</i> 
                                                    <span>Bases de datos</span>
                                                </div>
                                            }
                                            </div>
                                       } />  
                                       <ToolbarGroup firstChild={true}>
                                       </ToolbarGroup>
                                       <ToolbarGroup className="light-tool">
                                        { 
                                          (view === "projects")?  
                                          <IconButton tooltip="Agregar Proyecto" onClick={this.handleNewProject}>
                                            <FontIcon className="material-icons add-icon">add_circle</FontIcon>
                                          </IconButton>:null
                                        }
                                       </ToolbarGroup>
                                </Toolbar>
                                {
                                    (this.state.view === "projects")?
                                    <div className="projects-container">
                                        
                                        <div className="card-container"> 
                        	            	<Card className="my-card add-project" onClick={this.handleNewProject}>
                        	            		
                        	            		<BottomNavigation selectedIndex={this.state.selectedIndex} className="add-button">
                	            		          <BottomNavigationItem
                	            		            label="Agregar Proyecto"
                	            		            icon={<FontIcon className="material-icons">add</FontIcon>}
                	            		            
                	            		          />
                            		            </BottomNavigation>
                        	            	</Card>
                                        </div>
                                        {
                                            projects.map((item, index) => {
                                                return (<ItemProject key={index.toString()} {...item}/>);
                                            })
                                        }
                                    </div>:
                                    <div className="project-new">
                                        <ProjectForm />
                                    </div>
                                }
                                
        	            </Col>
        	            <Col sm="4"></Col>
        	          </Row>
                </Container>

				  </div>
			  </div>	

			</MuiThemeProvider>
		);
	}
};
export default withRouter(Projects);