/**
* @routes: app,db
* Carga los middlewares de la aplicación 
* Crea Rutas dinamicas para acceder a las funciones CRUD
* de modelos de manera Simple.
* Si desea agregar nueva funcionalidad, debe sobre escribir las rutas
* o crear rutas personalizadas en el directorio /routes para rutas publicas
* o en /routes/app para rotas privadas de la aplicación.
* Los esquemas estan disponibles en el evento on("NOMBRE_SCHEMA",callback);
* ...................................................................
* NOTA: Si el nombre del archivo no corresponde con
* un nombre de schema válido, el valor del atributo de entrada "schema"
* será indefinido. Queda a disposición del desarrollador utilizar el objeto db
* para escuchar los eventos y especificar con que esquema trabajará.
* Eventos Disponibles:
+ db.on("NOMBRE_MODELO",(schema))
+ schema.on("define",(model))
+ db.on("define",(name,model))
* ver la definición de las rutas publicas y privadas en este script.
* keyword: (Instanciar ruta:)
* 
* NOTA:
* Las rutas públicas se cargan directamente, las rutas privadas deben 
* cargarse cuando se emita el evento define del modelo.
*/
var express = require("express");
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var router = express.Router();
//var admin_router = express.Router();
////var AdminRouter = require("./AdminRouter");

var dateFormat = require('dateformat');
var {pluralize} = require("../helpers/helper");
var md5 = require("md5");
var fs = require("fs");
var path = require("path");
var Helper = require("../helpers/helper");
const Constants = require("./helpers/Constants");


var jwt = require("jsonwebtoken");
var Donec = require("../Donec.js");
//// var Admin = require("./AdminRouter.js");


function fileExists(path) {

	try  {
	  return fs.statSync(path).isFile();
	}
	catch (e) {
  
	  if (e.code == 'ENOENT') { // no such file or directory. File really does not exist
		console.log("File does not exist.");
		return false;
	  }
  
	  console.log("Exception fs.statSync (" + path + "): " + e);
	  throw e; // something else went wrong, we don't have rights, ...
	}
}

module.exports = function(context,app,db,io){
	
	const public_routes = path.join(global.SERVER_PATH,'routes');
	const app_routes = path.join(global.APP_PATH,'routes');
	const app_services = path.join(global.APP_PATH,'services');
	const core_routes = path.join(global.CORE_PATH,'routes');

	console.log("Core Routes: ",core_routes);
	
	var routes_count = 0;
	
	
	
	//app.use("/admin",admin_router);
	function readFiles(base_path,subfolder){
		var fileContents;
		return new Promise(function(resolve,reject){
			var paths = [];
			try{
				fileContents = fs.readdirSync(base_path)
				.reduce((paths,filename,index,arr) => {
					var total = (arr.length-1);
					var file = "";
					var file_path = "";
					var ext = path.extname(filename);
					
					file_path = base_path;
					
					/* --- Si es un archivo ---*/
					var handle_route;
					if(ext!=null && ext!==""){
						if(ext=='.js'){
							var controllerName=Helper.capitalize(name);
							file = path.join(file_path,filename);
							var name = path.parse(file).name.toLowerCase();

							if(!subfolder){
								//console.log("Router public define custom: ",name,filename);
								handle_route = require(file);
								paths.push("/"+name);
								//Donec.define(name,handle_route,true);
								
								Donec.define(name,{
									"path":"/"+name,
									"init":handle_route
								});
							}else{
								/*Listener para cuando se cree el Schema con el nombre especifico.*/
								db.on(name,function(schema){
									try{
										handle_route = require(file);
										//console.log("?",handle_route);
										var pathname;
										var schema;
										pathname = pluralize(schema.lang || "es",name);

										if(typeof subfolder == "undefined") subfolder = "";
										pathname = subfolder+"/"+pathname;
										/*
										* Instanciar ruta: privada: 
										* La aplicación empieza a usar el nuevo middlewares route.
										*/
										//console.log("onDefined",name);
										Donec.define(name,{
											"path":pathname,
											"parent":subfolder,
											"schema":schema,
											"init":handle_route
										});
										// Donec.register({"name":name, "path": pathname,"schema":schema},route);
										// Donec.start(name);
										//app.use(pathname,route(app,global.router,db,schema));
										//#app.use(pathname,route(app,io,global.router,db,schema));
									}catch(err){
										console.log(new Error(err.message));
										reject(err);
									}
								});
							}
							routes_count++;
						}
						//console.log("- File "+routes_count+":",file,ext);
					}else{
						//console.log("> Folder",filename,file_path);
						file_path = path.join(file_path,filename);
						
						let name = filename.toLowerCase();
						readFiles(file_path,name); 
					}
					return paths;
				},paths);
				resolve();
			}catch(error){
				reject(error);
			};			
		});
	}

	//console.log("Cargando rutas...");
	
	//Cargar Rutas del Core

	//Cargar Rutas del usuario
	if(!fs.existsSync(global.APP_PATH)) {//Si no existe crea el directorio de la aplicación
		fs.mkdirSync(global.APP_PATH);
	}
	

	Helper.createDirectoryAndFile(app_routes,Constants.FILENAME_README,Constants.TEXT_README)
	.then(()=>{
		readFiles(app_routes)
		.then(function(){
			Helper.createDirectoryAndFile(app_services,Constants.FILENAME_README,Constants.TEXT_README_SERVICES)
			.then(()=>{
				
				readFiles(core_routes)
				.then(function(){
					//Cargar Rutas de la aplicación
					readFiles(public_routes)
					.then(function(){
						
					},function(err){
						console.log("Error to load App Routes: ",err.message);
					});
				});
			},function(err){
				console.log("Error to load App Routes: ",err.message);
			});
			//console.log("Rutas de la aplicación cargadas con éxito!");
		},err => {
			console.log("ERROR: ",err.message);
		});
		//console.log("Rutas públicas cargadas con éxito!");
	},function(err){
		console.log("Error to load Core Routes: ",err.message);
	});

	
	return router;
};