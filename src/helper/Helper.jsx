var Helper = {
	model:{},
	getHashParam: function(str,paramIndex) {
	  var sections = str.split("/");
	  if((typeof(paramIndex) == "undefined")){
	    paramIndex = (sections.length - 1);
	    return sections[paramIndex];
	  }
	  return sections.filter(function(value,index){
	    return (index - 1)===paramIndex;
	  }).join("");
	},
	validateEmail: function(value)  {
    	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(value);
  	},
  	parseFormData: function(params)  {
		const formData = new FormData();
		for(var s in params){
        	formData.append(s,params[s]);
        }
        return formData;
  	},
  	localStorage:function(key,values){
  		if(localStorage.getItem(key)){
  			try{
  				//console.log(key+" ya existe.");
  				if(values){
  					localStorage.setItem(key,JSON.stringify(values));
  				}
  				if(this.state){
  					this.setState({
  						[key]:JSON.parse(localStorage.getItem(key))
  					});
  				}
  				return JSON.parse(localStorage.getItem(key));
  			}catch(err){
  				return;
  			}
  		}else{
  			//console.log(key+" guardado.");
			localStorage.setItem(key,JSON.stringify(values));
			if(this.state){
				this.setState({
					[key]:JSON.parse(localStorage.getItem(key))
				});
			}
  			return JSON.parse(localStorage.getItem(key));
  		}
  	},
    sortArray:function(data,field,order){
        order = (typeof order !== "undefined")?order:1;
        if(data.lenght===0) return data;
        if(typeof data[0] === "object"){
          if(typeof field === "undefined") return data;
          return data.sort((a, b) => {
              if(a[field]<b[field]) return (order * -1);
              if(a[field]>b[field]) return (order * 1);
              return 0;
          });
        }else if(typeof data[0] === "string"){
          order = (typeof field === "undefined")?order:field;
          return data.sort(function(a, b){
            return (order>0)?a-b:b-a;
          });
        }
    },
    groupBy:function(xs, key) {
      return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    },
    uniqArray:function(data,field,order_field,sort){
      var result =[];
      var obj_data = [];
      if(data.length==0){return result;}
      
      if(field){
        if(typeof(data[0]) === 'object'){
          data.forEach(item => {
            if(result.indexOf(item[field])<0){
              result.push(item[field]);
              obj_data.push(item);
              }
          });

          //console.log("order Array:",this.sortArray(obj_data,order_field));
          return obj_data;
        }
      }
      data.forEach(item => {
        if(result.indexOf(item) < 0) {
            result.push(item);
          }
      });
      return result;
    },
    isEmpty:function(obj){
      var hasOwnProperty = Object.prototype.hasOwnProperty;
      var str = null;

          // null and undefined are "empty"
          if (obj == null) return true;
          
          // If it isn't an object at this point
          // it is empty, but it can't be anything *but* empty
          // Is it empty?  Depends on your application.
          if (typeof obj !== "object") return true;

          // Assume if it has a length property with a non-zero value
          // that that property is correct.
          if (obj.length > 0)    return false;
          if (obj.length === 0)  return true;

          // Otherwise, does it have any properties of its own?
          // Note that this doesn't handle
          // toString and valueOf enumeration bugs in IE < 9
          for (var key in obj) {
              if (hasOwnProperty.call(obj, key)) return false;
          }
          return true;
    }
    
}
export default Helper;
