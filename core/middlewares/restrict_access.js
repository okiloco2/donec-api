const Constants = require("../../helpers/Constants");
module.exports = (req,res,next) => {

	let restrict_access = global.restrict_access;
	let original_url = req.method+" "+req.originalUrl;
	
	/**Validar Errores en parámetros de entrada.
	if(err){
		if (res.headersSent) {
			return next(err);
		}
		res.status(500);
		res.json({
			success:false,
			msg:"Error en parámetros de entrada: "+err.message
		});
		return;
	} */

	/**
	 * Verificar rutas excluidas para restringir acceso.
	 */

	const regex = "("+req.method+")\\s(\/){0,1}([(\\w+(\/){0,1})]+)(:[\\w]+)";
	const regx_restrict = new RegExp(regex,"g");

	const _regex = "("+req.method+")\\s(\/){0,1}([(\\w+(\/){0,1})]+)[\\w+]";
	
	
	const regx_urls = new RegExp(_regex,"g");

	if(original_url.match(regx_urls)){
		//Urls restringidas que coinciden.
		let _urls = restrict_access.match(regx_restrict);
		if(_urls){
			var str = ""; 
			var str_result = original_url.slice(original_url.lastIndexOf("/"), original_url.length);
			str_result = original_url.replace(str_result,"/*");
			str = _urls.reduce((str,value) => {
				var _str = value.slice(value.lastIndexOf(":"), value.length);
				_str = value.replace(_str,"*");
				if(_str === str_result){
					console.log("Restrict: ",original_url);
					str = str_result;
				}
				return str;
			},str);
			if(str!=""){
				return res.json({
					success:false,
					msg:Constants.TEXT_RESTRICT_ACCESS
				});
			}
		}
	}
	
	if(restrict_access.includes(original_url) && original_url!== "GET /"){
		console.log("Restrict: ",original_url)
		return res.json({
			success:false,
			msg:Constants.TEXT_RESTRICT_ACCESS
		});
	}else{
		next();
	}
}